var submenu_animation_duration = 300;
var donateoverlay_animation_duration = 300;
var submenu_pos_open = 0;
var submenu_pos_close = 0;
var scrolled = false;
var $searchIsotpe;

$(document).ready(function()
{	
	ignitePackery();
	addClickListeners();
	setTimeout(function(){
		articleHeight();
		subsiteHeight();
	}, 100);
	
});


/*************************************************************************************************************************
	SCROLL TO SECTION
**************************************************************************************************************************/

function scrollToElement(ele) {
   // $(window).scrollTop(ele.offset().top).scrollLeft(ele.offset().left);
    $('html,body').animate({scrollTop: ele.offset().top+0}, 800);
}



/*************************************************************************************************************************
	WINDOW SCROLL FOR SAFARI FOOTER FIX
**************************************************************************************************************************/
	
$( window ).scroll(function() {
	
		$('#footer').css({'display': 'block'});
		
		
	
});

/*************************************************************************************************************************
	SUBSITE HEIGHT RESIZE
**************************************************************************************************************************/
	
function subsiteHeight()
{
	$('.article_content_subsite').each(function(){
		var biggestHeight = 0;
		$this_article = $(this);
		$this_article.find('.module').each(function(){
			$this = $(this);
			var thisHeight = $this.height()+$this.position().top;
			
			if(thisHeight > biggestHeight)
			{
				biggestHeight = thisHeight;
			}
		});
		$this_article.css({height: biggestHeight+10+'px'});
		
	});	
	
}

/*************************************************************************************************************************
	NEW LINE TO BR
**************************************************************************************************************************/

function nl2br (str, is_xhtml) {   
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';    
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1'+ breakTag +'$2');
}


/*************************************************************************************************************************
	ARTICLE HEIGHT RESIZE
**************************************************************************************************************************/
	
function articleHeight()
{
	var biggestHeight = 0;
	$('.module').each(function(){
		$this = $(this);
		var thisHeight = $this.height()+$this.position().top;
		
		if(thisHeight > biggestHeight)
		{
			biggestHeight = thisHeight;
		}
	});
	
	$('#article_content').css({height: biggestHeight+50+'px'});
}


/*************************************************************************************************************************
	ISOTOPE SEARCH ARTICLE TEASERS
**************************************************************************************************************************/
function igniteIsotopeTeaser(tag)
{
	if(tag != false)
	{
		
		$searchIsotpe = $('#isotopeHolder').isotope(
		{
			itemSelector: '.teaser_item',
			filter: '.'+tag,
			gutter: 5
		});
	}
	else
	{
		$searchIsotpe = $('#isotopeHolder').isotope(
		{
			itemSelector: '.teaser_item',
			gutter: 5
		});
	}
	
}



/*************************************************************************************************************************
	ISOTOPE SUBSITE ARTICLE TEASERS
**************************************************************************************************************************/
function igniteIsotopeTeaserSubsite(num)
{
	
	for(var i = 1; i<=num; i++)
	{
		$('#isotopeHolder'+i+'').isotope(
		{
			itemSelector: '.teaser_item',
			gutter: 5
		});
	}
	
	
	
	
}


/*************************************************************************************************************************
	PACKERY
**************************************************************************************************************************/
function ignitePackery()
{
	$('#articles').packery(
	{
		itemSelector: '.article',
		gutter: 10
	});
}




/****************************************************************************************************************************
	MENU
**************************************************************************************************************************/
function toggleMenuListeners(toggle)
{
	if(toggle)
	{
		$('.menu_item').click(function()
		{
			toggleSubmenu($(this).attr('target'));
		});
		
		$('.menu_item_search').click(function()
		{
			
		});
	}
	else
	{
		$('.menu_item').unbind('click');
		$('.menu_item_search').unbind('click');
	}
}


function toggleSubmenu(target)
{
	toggleMenuListeners(false);
	
	if($('.submenu.open').length > 0)
	{
		if($('.submenu.open').attr('target') != target)
		{
			$('.submenu.open').animate({'height': 0, 'padding-top': 0, 'padding-bottom': 0, 'max-height': 0}, submenu_animation_duration, function()
			{
				$(this).removeClass('open');
			
				$('.submenu[target="' + target + '"]').css('height', 'auto');
				$('.submenu[target="' + target + '"]').animate({'max-height': 1000, 'padding-top': 15, 'padding-bottom': 15}, submenu_animation_duration, function()
				{
					$(this).addClass('open');
					toggleMenuListeners(true);
				});
			});
		}
		else
		{
			$('.submenu.open').animate({'height': 0, 'padding-top': 0, 'padding-bottom': 0, 'max-height': 0}, submenu_animation_duration, function()
			{
				$(this).removeClass('open');
				toggleMenuListeners(true);
			});
		}
	}
	else
	{
		$('.submenu[target="' + target + '"]').css('height', 'auto');
		$('.submenu[target="' + target + '"]').animate({'max-height': 1000, 'padding-top': 15, 'padding-bottom': 15}, submenu_animation_duration, function()
		{
			$(this).addClass('open');
			toggleMenuListeners(true);
		});
	}
}


/****************************************************************************************************************************
	NEWSLETTER
**************************************************************************************************************************/
function toggleNewsletterListeners(toggle)
{
	if(toggle)
	{
		$('#footer_newsletter_register, #contact_newsletter_register').click(function()
		{
			save_newsletter_email($(this));
		});
	}
	else
	{
		$('#footer_newsletter_register').unbind('click');
		$('#contact_newsletter_register').unbind('click');
	}
}

function save_newsletter_email(register)
{
	if(register.attr('id') == 'footer_newsletter_register')
	{
		var email = $('#footer_newsletter').val(); 
		var input = $('#footer_newsletter');
		var success = $('#footer_newsletter_success');
	}
	else
	{
		var email = $('#contact_newsletter').val(); 
		var input = $('#contact_newsletter');
		var success = $('#contact_newsletter_success');
	}
	
	$.ajax(
	{
        url: rootUrl + 'St_anna/save_newsletter_email',
        data: {email: email},
        type:"POST",
        cache: false,
        success: function(data) 
        {
        	register.fadeOut(200);
        	input.fadeOut(200, function()
        	{
            	success.fadeIn(200);
        	});
        }
    });		
}



/****************************************************************************************************************************
	SEARCH
**************************************************************************************************************************/
function toggleSearchListeners(toggle)
{
	if(toggle)
	{
		$('.menu_item_search').click(function()
		{
			var elem = $('#search_overlay');
			toggleSearchListeners(false);
			if(elem.hasClass('open'))
			{
				elem.animate({'top': '-=30'}, 100, function()
				{
					elem.removeClass('open');
					toggleSearchListeners(true);
				});
			}
			else
			{
				elem.animate({'top': '+=30'}, 100, function()
				{
					elem.addClass('open');
					elem.find('input').focus();
					toggleSearchListeners(true);
				});
			}
		});
		
		$('#search_input').keyup(function(e)
		{
			if (e.keyCode == 13)
			{
				window.location.href = rootUrl + 'search?param=' + encodeURIComponent($('#search_input').val());
			}
		});
	}
	else
	{
		$('.menu_item_search').unbind('click');
		$('#search_input').unbind('keyup');
	}
}


/****************************************************************************************************************************
MOBILE MENU
**************************************************************************************************************************/
function MobileMenu()
{
$('#mobile_menu_button').click(function(){
	
	if(!$(this).hasClass('open'))
	{
		
		$(this).addClass('open');
		$('.mobile_submenu').stop().animate({top:-1500},500)
		$('#mobileMenu').stop().animate({top:150},500);
	}
	else
	{
		
		$(this).removeClass('open');
		$('#mobileMenu').stop().animate({top:-1500},1000);
	}
	unbindMobile();
	MobileMenu();
	
});


$('.mobile_menu_item').click(function(){
	var target = $(this).attr('target');
	
	$('#mobile_menu_button').removeClass('open');
	$('#mobileMenu').stop().animate({top:-1500},500, function(){
		$('.mobile_submenu[target="'+target+'"]').stop().animate({top:150},500);
	});

	
	
});
}

function unbindMobile()
{
$('#mobile_menu_button').unbind('click');
}



function addClickListeners()
{
	$('.search_tag_item').click(function(){
		var filter = $(this).attr('tag');
		$searchIsotpe.isotope({ filter: '.'+filter });
	});
}


/****************************************************************************************************************************
SLICK SLIDER
**************************************************************************************************************************/

function initSlick(left,center,right)
{
	
	var slick1 = $('#mid_holder').slick({
		
		vertical: true,
		verticalSwiping: true,
		draggable: false,
		swipe: false,
		infinite:true,	    
	    slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: left*1000,
		arrows: false,
	});
	
	
	/*if(left != 0)
	{
		var slick1 = $('#slider_left').slick({
			vertical: true,
			verticalSwiping: true,
			infinite:true,	    
		    slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: left*1000,
			arrows: false		
		});
	}
	else
	{
		var slick1 = $('#slider_left').slick({
			vertical: true,
			verticalSwiping: true,
			infinite:true,	    
		    slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false		
		});
	}
	
	if(center != 0)
	{
		var slick1 = $('#slider_center').slick({
			vertical: true,
			verticalSwiping: true,
			infinite:true,	    
		    slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: center*1000,
			arrows: false		
		});
	}
	else
	{
		var slick1 = $('#slider_center').slick({
			vertical: true,
			verticalSwiping: true,
			infinite:true,	    
		    slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false		
		});
	}
	
	
	
	if(right != 0)
	{
		var slick1 = $('#slider_right').slick({
			vertical: true,
			verticalSwiping: true,
			infinite:true,	    
		    slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: right*1000,
			arrows: false		
		});
	}
	else
	{
		var slick1 = $('#slider_right').slick({
			vertical: true,
			verticalSwiping: true,
			infinite:true,	    
		    slidesToShow: 1,
			slidesToScroll: 1,
			arrows: false		
		});
	}*/
	
	
}

