function smallscreen()
{
	submenu_pos = 110;
	
	var ratio = wWidth / 800;
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	// FUNCTIONALITY
	////////////////////////////////////////////////////////////////////////////////////////////////
	center_news_articles();
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	// CSS
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	$('.containerwidth').css(
	{
		'width': wWidth-30,
		'margin-left': 0,
		'margin-right': 0,
	});
	
	$('#menu_container').css(
	{
		'padding-left': '15px',
		'padding-right': '15px'
	});
	
	$('.containerborder').css(
	{
		'border-left': 'solid 15px #ffffff',
		'border-right': 'solid 15px #ffffff',
	});
	
	$('#menu_upper_container').css(
	{
		'width': wWidth-30,
		'margin-left': 125,
		'margin-right': 15,
	});
	
	$('#menu_logo').css(
	{
		'left': (wWidth/2) - 420 - 220,
	});	
	
	$('#footer_container').css(
	{
		'margin-left': 0,
		'margin-right': 0,
		'width': wWidth,
	});
	
	$('.footer_column').css(
	{
		'width': wWidth-20,
		'margin-right': 20,
	});
	
	$('#footer_social, #footer_donations, #footer_menu').css(
	{
		'width': wWidth-15,
		'padding-left': 15
	});

	$('#headline_container, .headline_video, .headline_img').css(
	{
		'width': wWidth - 30,
		'height': parseInt((wWidth - 30) * 0.422),
	});
	
	$('.headline').css(
	{
		'width': wWidth - 60,
		'height': parseInt((wWidth - 60) * 0.398)
	});
	
	$('.upper_menu_right').css(
	{
		'float': 'none',
		'margin-left': 0,
		'margin-right': 10
	});
	
	$('.upper_menu_social').css(
	{
		'height': 30
	});
	
	$('.upper_menu_left').css(
	{
		'float': 'none',
		'height': 30
	});
	
	$('#menu_upper_container_social').css(
	{
		'float': 'none',
		'margin-top': 10
	});
	
	$('.upper_menu_newsletter').css(
	{
		'display': 'inline-block',
		'line-height': '18px',
		'padding': '4px 35px',
	});
	
	$('#menu_upper').css(
	{
		'height': 100
	});
	
	$('#menu_shadow_bottom').css(
	{
		'top': 100
	});
	
	$('#menu').css(
	{
		'top': 100
	});	
	
	$('#container').css(
	{
		'margin-top': 150
	});
	

	$('.headline_text').css(
	{
		'font-size': 30 * ratio
	});
	
	$('#donate_overlay').css(
	{
		'top': 370,
		'display': 'none'
	});
	
	$('#menu_container li').css(
	{
		'margin-right': 25
	});
	
	$('.submenu').css(
	{
		'top': 150
	});

	$('#article').css(
	{
		'width': wWidth,
		'margin-left': 0,
		'margin-right': 0,
		'margin-top': 150
	});	

	$('#article_content').css(
	{
		'width': wWidth - 30
	});
	
	$('#article_sidebar').css(
	{
		'width': 0,
		'display': 'none'
	});	
	
	$('.article_preview').css(
	{
		'width': wWidth - 30 - 20,
	});	
	
	$('.article_preview_text').css(
	{
		'width': wWidth - 30 - 20,
		'margin-top': 7,
	});
	
	$('#search_overlay').css(
	{
		'right': 15,
		'top': 100
	});

}

function center_news_articles()
{
	var cWidth = wWidth - 30;
	var aWidth = 250;
	var gutter = 10;
	
	if(cWidth >= 770)
		perline = 3;
	else if(cWidth >= 510)
		perline = 2;
	else
		perline = 1;
	
	var margin = (cWidth - (aWidth * perline)) / 2;
	
	$('#articles').css(
	{
		'width': (perline * aWidth) + ((perline-1) * gutter),
		'padding-left': margin,
		'padding-right': margin
	});
}


