
function mobiledevice()
{
	
	var ratio = wWidth / 800;
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	// FUNCTIONALITY
	////////////////////////////////////////////////////////////////////////////////////////////////
	center_news_articles();
	fix_content_textsize();
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	// CSS
	////////////////////////////////////////////////////////////////////////////////////////////////
	
	$('.containerwidth').css(
	{
		'width': wWidth-30,
		'margin-left': 0,
		'margin-right': 0,
	});
	
	$('#menu_container').css(
	{
		'padding-left': '15px',
		'padding-right': '15px'
	});
	
	$('.containerborder').css(
	{
		'border-left': 'solid 15px #ffffff',
		'border-right': 'solid 15px #ffffff',
	});
	
	$('#menu_upper_container').css(
	{
		'width': wWidth-30,
		'margin-left': 120,
		'margin-right': 15,
	});
	
	$('#menu_logo').css(
	{
		'left': (wWidth/2) - 420 - 220,
	});	
	
	$('#donate_overlay').css(
	{
		'left': wWidth - 60
	});
	
	$('#footer_container').css(
	{
		'margin-left': 0,
		'margin-right': 0,
		'width': wWidth,
	});
	
	$('.footer_column').css(
	{
		'width': wWidth-20,
		'margin-right': 20,
	});
	
	$('#footer_social, #footer_donations, #footer_menu').css(
	{
		'width': wWidth-15,
		'padding-left': 15
	});

	$('#headline_container, .headline_video, .headline_img').css(
	{
		'width': wWidth - 30,
		'height': 0,
	});
	
	$('.headline').css(
	{
		'width': wWidth - 60,
		'height': parseInt((wWidth - 60) * 0.398)
	});
	
	$('.upper_menu_right').css(
	{
		'float': 'none',
		'margin-left': 0,
		'margin-right': 10
	});
	
	$('.upper_menu_social').css(
	{
		'height': 20
	});
	
	$('.upper_menu_left').css(
	{
		'float': 'none',
		'height': 27
	});
	
	$('#menu_upper_container_social').css(
	{
		'float': 'none',
		'margin-top': 10
	});
	
	$('.upper_menu_newsletter').css(
	{
		'display': 'inline-block',
		'line-height': '12px',
		'padding': '2px 9px',
	});
	
	$('#menu_upper').css(
	{
		'height': 100
	});
	
	$('#menu_shadow_bottom').css(
	{
		'top': 100
	});
	
	$('#menu').css(
	{
		'top': 100
	});	
	
	$('#container').css(
	{
		'margin-top': 150
	});
	

	$('.headline_text').css(
	{
		'font-size': 30 * ratio
	});
	
	
	$('#donate_overlay').css(
	{
		'top': 200,
		'display': 'none'
	});	
	
	$('#menu_container li').css(
	{
		'margin-right': 10
	});	

	$('.submenu').css(
	{
		'top': 110
	});
	
	
	$('#article').css(
	{
		'width': wWidth,
		'margin-left': 0,
		'margin-right': 0,
		'margin-top': 150
	});	

	$('#article_content').css(
	{
		'width': wWidth - 30
	});
	
	$('#article_sidebar').css(
	{
		'width': 0,
		'display': 'none'
	});	
	
	$('#article').css(
	{
		'width': wWidth,
	});	

	$('#article_content').css(
	{
		'width': wWidth,
	});
	
	$('#article_sidebar').css(
	{
		'width': 0,
		'display': 'none'
	});		
	
	$('#article_info, #article_sharing').css(
	{
		'width': wWidth - 30,
	});
	
	
	$('#article_headline').css(
	{
		'width': wWidth - 30,
		'font-size': 21,
	});	
	
	$('.module').css(
	{
		'width': wWidth - 30,
	});
	
	$('.module_image img').css(
	{
		'max-width': wWidth - 30,
	});
	
	$('.article_preview').css(
	{
		'width': wWidth - 30 - 20,
	});
}

function center_news_articles()
{
	var cWidth = wWidth - 30;
	var aWidth = 250;
	var gutter = 10;
	
	if(cWidth >= 770)
		perline = 3;
	else if(cWidth >= 510)
		perline = 2;
	else
		perline = 1;
	
	var margin = (cWidth - (aWidth * perline)) / 2;
	
	$('#articles').css(
	{
		'width': (perline * aWidth) + ((perline-1) * gutter),
		'padding-left': margin,
		'padding-right': margin
	});
}

function fix_content_textsize()
{
	
}