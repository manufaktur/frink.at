var active_bg_index = 0;
var background_interval = 0;


$(document).ready(function()
{	
	igniteBackground();
});


function onYouTubeIframeAPIReady() 
{
	var keys = Object.keys(video_embedds);
	
	for(var i = 0 ; i < keys.length ; i++)
	{
		var player = new YT.Player('yt_iframe_' + keys[i], 
		{
	        height: '770',
	        width: '325',
	        videoId: video_embedds[keys[i]],
	        events: 
	        {
	        	'onStateChange': onPlayerStateChange
	        }
	    });
	}
}

function onPlayerStateChange(event) 
{
    if (event.data == YT.PlayerState.PLAYING) 
    {
    	clearInterval(background_interval);
    }
    else if(event.data == YT.PlayerState.PAUSED) 
    {
    	startBackgroundRotation();
    }
}

function igniteBackground()
{
	$('#bg_image img:first').addClass('bg_image_active');
	active_bg_index = $('#bg_image img:first').attr('ordering');
	
	if($('.headline[ordering="' + active_bg_index + '"]').length > 0)
		$('.headline[ordering="' + active_bg_index + '"]').addClass('active').fadeIn(0);
	
	if($('.headline_img[ordering="' + active_bg_index + '"]').length > 0)
		$('.headline_img[ordering="' + active_bg_index + '"]').addClass('active').fadeIn(0);
	
	
	if(ignite_backgrounds == 1)
		startBackgroundRotation();
}


function startBackgroundRotation()
{
	background_interval = setInterval(function()
	{
		// bg image
		var active_bg_img = $('#bg_image .bg_image_active');
		var next_bg_img = ($('#bg_image .bg_image_active').next().length > 0) ? $('#bg_image .bg_image_active').next() : $('#bg_image img:first');
		active_bg_index = next_bg_img.attr('ordering');
		
		active_bg_img.fadeOut(500, function()
		{
			active_bg_img.removeClass('bg_image_active');
			next_bg_img.fadeIn(500);
			next_bg_img.addClass('bg_image_active');
		});
		
		if($('#headline_container').length > 0)
		{
			// bg_img_small
			var active_bg_small = $('.headline_img.active');
			var next_bg_small = $('.headline_img[ordering="' + active_bg_index + '"]');
			if(active_bg_small.length > 0)
			{
				active_bg_small.fadeOut(500, function()
				{
					active_bg_small.removeClass('active');
					if(next_bg_small.length > 0)
					{
						next_bg_small.addClass('active');
						next_bg_small.fadeIn(500);
					}
				});
			}
			else
			{
				if(next_bg_small.length > 0)
				{
					next_bg_small.addClass('active');
					setTimeout(function()
					{
						next_bg_small.fadeIn(500);
					}, 500);
				}	
			}
			
			// bg text
			var active_bg_text = $('.headline.active');
			var next_bg_text = $('.headline[ordering="' + active_bg_index + '"]');
			if(active_bg_text.length > 0)
			{
				active_bg_text.fadeOut(500, function()
				{
					active_bg_text.removeClass('active');
					if(next_bg_text.length > 0)
					{
						next_bg_text.addClass('active');
						next_bg_text.fadeIn(500);
					}
				});
			}
			else
			{
				if(next_bg_text.length > 0)
				{
					next_bg_text.addClass('active');
					setTimeout(function()
					{
						next_bg_text.fadeIn(500);
					}, 500);
				}	
			}
			
			// bg_video
			var active_bg_video = $('.headline_video.active');
			var next_bg_video = $('.headline_video[ordering="' + active_bg_index + '"]');
			if(active_bg_video.length > 0)
			{
				active_bg_video.fadeOut(500, function()
				{
					active_bg_video.removeClass('active');
					if(next_bg_video.length > 0)
					{
						next_bg_video.addClass('active');
						next_bg_video.fadeIn(500);
					}
				});
			}
			else
			{
				if(next_bg_video.length > 0)
				{
					next_bg_video.addClass('active');
					setTimeout(function()
					{
						next_bg_video.fadeIn(500);
					}, 500);
				}	
			}
		}
	}, bg_interval);
}

