

function new_link_module(id)
{
	var new_elem = 
	{
		id: id,	
		ordering: 0,
		db_id: null,
		selector: null,
		textarea: null,
		preview: null,
		type: 'link',
		text: '',
		state: null,
		align: 0,
		
		margin_top: 0,
		margin_bottom: 0,
		font_color: 1,
		font_size: 15,
		text_align: 0,
				
		
		// MODULE BASIC
		init: function(parent)
		{
			var html = '<div style="border:1px solid #000" class="module module_link" module_id="' + this.id + '"><div id="sorting_holder"></div></div>';
			
			parent.append(html);
			
			this.selector = '.module[module_id="' + this.id + '"]';
			
			this.state = 'preview';
			this.bindListeners();
			
			
			
		},
		
		bindListeners: function()
		{
			$(this.textarea).keyup(function(e)
			{
				active_module.text = $(this).val();
				var mod_id = $(e.target).parent().attr('module_id');
				$(modules[mod_id].selector).height($(e.target).height());
				$(e.target).height(0);
			    var scrollval = $(e.target)[0].scrollHeight;
			    $(e.target).height(scrollval);
			    $(modules[mod_id].selector).height('auto');
			    if (parseInt(e.target.style.height) > $(window).height() - 30) 
			    {
			    	$(document).scrollTop(parseInt(e.target.style.height));
			    }
			    
			    $(active_module.preview).empty();
			    $(active_module.preview).html(nl2br($(active_module.textarea).val()));
			});
			
			$(this.textarea).focus(function()
			{
				modules[$(this).parent().attr('module_id')].setActive();
			});
			
			$(this.preview).click(function()
			{
				modules[$(this).parent().attr('module_id')].setActive();
			});
		},
		
		unbindListeners: function()
		{
			$(this.textarea).unbind('keyup');
			$(this.textarea).unbind('focus');
		},
			
		
		showProperty: function(parent)
		{
			parent.fadeIn(0);
			parent.attr('module_id', this.id);
			parent.empty();
			
			html = '<table >';
			html += '<tr>  <td class="module_properties_label">  <label>Text</label>  </td>  <td class="module_properties_input">  <input type="text" id="link_text" value=""></td>  </tr>';
			html += '<tr>  <td class="module_properties_label">  <label>Target</label>  </td>  <td class="module_properties_target">  <select class="link_target">';
			for(var i = 0 ; i < targets.length ; i++)
			{
				html += '<option value="' + targets[i].id + '"';			
				html += '>' + targets[i].name + '</option>';
			}
			html += '</select>  </td>  </tr>';
			html += '<tr>  <td class="module_properties_label">  <label>Subsite</label>  </td>  <td class="module_properties_subsite">  <select class="link_subsite">';
			for(var i = 0 ; i < subsites.length ; i++)
			{
				html += '<option value="' + subsites[i].id + '"';
				html += '>' + subsites[i].name + '</option>';
			}
			html += '</select>  </td>  </tr>';
			html += '<tr>  <td class="module_properties_label">  <label>URL</label>  </td>  <td class="module_properties_input">  <input type="text" class="link_url" value=""></td>  </tr>';
			html += '<tr> <td colspan="2"> <div class="module_properties_button" id="add_link">Add link</div> </td> </tr>';
			html += '</table>';
			
			
			parent.html(html);
			this.uploadListeners();
			togglePropertyListeners(true);
		},
		
		activate: function(focus_on_input)
		{
			if(this.state == 'editor')
				$(this.textarea).focus();
			else
				this.setActive();
		},
		
		setActive: function()
		{
			if(active_module != null)
				active_module.blur(this.id);
			
			active_module = modules[this.id];
			
			
			this.showProperty(module_properties);	
			this.showOptions();
			this.changeState('editor');
		},
		
		blur: function(new_active)
		{
			if(new_active != this.id)
			{
				this.changeState('preview');
				this.hideOptions();
				if(this.text.length == 0)
				{
					$(this.selector).find('.module_remove').click();
				}
			}

		},
		
		showOptions: function()
		{
			if($(this.selector).find('.module_options').length == 0)
			{
				var html = '<div class="module_options">Links';
				html += module_remove_option();
				html += '</div>';
				$(this.selector).append(html);
				toggleOptionListeners(true);
			}
		},
		
		hideOptions: function()
		{
			$(this.selector).find('.module_options').remove();
		},
		
		getSaveData: function()
		{
			var ret = 
			{
				'text': $('#sorting_holder').html(),	
				'id': this.db_id,
				'type': this.type,
				'ordering': this.ordering,
				'parent_id': parent_id,
				'parent_type': parent_type,
			};
			
			return ret;
		},
	
		load: function(properties)
		{
			this.setText(properties.content);
		},		
		
		
		// PROPERTY SETTERS/GETTERS
		setMarginTop: function(margin_top)
		{
			this.margin_top = parseInt(margin_top);
			$(this.selector).css({'margin-top': this.margin_top});
		},
		
		setMarginBottom: function(margin_bottom)
		{
			this.margin_bottom = parseInt(margin_bottom);
			$(this.selector).css({'margin-bottom': this.margin_bottom});
		},
		
		//MODULE SPECIFIC
		setText: function(content)
		{
			$('#sorting_holder').append(content);
			var container = $('#sorting_holder');
			
			this.addSorting(container);
		},
		
		setFontColor: function(color_id)
		{
			this.font_color = color_id;
			for(var i = 0 ; i < colors.length ; i++)
			{
				if(colors[i].id == color_id)
				{
					$(this.textarea).css({'color': colors[i].hexcode});
					$(this.preview).css({'color': colors[i].hexcode});
				}
					
			}
			
			this.activate();
		},
		
		setTextAlign: function(align)
		{
			this.align = align;
			$(this.textarea).css({'text-align': text_align[this.align]});
			$(this.preview).css({'text-align': text_align[this.align]});
			$(this.textarea).keyup();
		},
		
		setFontSize: function(font_size)
		{
			this.font_size = font_size;
			$(this.textarea).css({'font-size': font_size + 'px'});
			$(this.preview).css({'font-size': font_size + 'px'});
			$(this.textarea).keyup();
		},
		
		setTemplate: function(margin_bot,margin_top, color, size)
		{
			this.setMarginBottom(margin_bot);
			this.setMarginTop(margin_top);
			this.setFontColor(color);
			this.setFontSize(size);
		},
		
		changeState: function(new_state)
		{
			if(new_state == 'preview')
			{
				$(this.selector).find('.module_preview').fadeOut(0);
				$(this.selector).find('.module_html').fadeIn(0);
				$(this.preview).fadeIn(0);
				$(this.textarea).fadeOut(0);
				$(this.selector).css({'padding-left': 15, 'padding-right': 15});
				this.hideStyling();
			}
			else
			{
				$(this.selector).find('.module_html').fadeOut(0);
				$(this.selector).find('.module_preview').fadeIn(0);
				$(this.textarea).fadeIn(0);
				$(this.preview).fadeOut(0);
				$(this.textarea).keyup();
				$(this.selector).css({'padding-left': 14, 'padding-right': 14});
				this.showStyling();
			}
			
			this.state = new_state;
		},
		
		showStyling: function()
		{
			var html = "";
			html += '<div class="module_text_styling">';
			
			html += '</div>';
			$(this.selector).append(html);
			
			
		},
		
		hideStyling: function()
		{
			$(this.selector).find('.module_text_styling').remove();
		},
		
		applyTag: function(starttag, endtag)
		{
			var ta = $(this.textarea);
			
			var sel = ta.getSelection();
			var text = ta.val();
			if(sel.start == sel.end)
			{
				var before = text.substring(0, sel.start);
				var after = text.substring(sel.start, text.length);
				
				var newtext = before + starttag + endtag + after;
			}
			else
			{
				var before = text.substring(0, sel.start);
				var after = text.substring(sel.end, text.length);
				var selection = text.substring(sel.start, sel.start + sel.length);
				var newtext = before + starttag + selection + endtag + after;
			}

			ta.val(newtext);
			$(this.textarea).keyup();
		},
		
		
		
		upload: function()
		{
			$('.module_properties_button[method="upload"]').parent().find('input').click();
		},
		
		uploadListeners: function()
		{
			$('#add_link').click(function()
			{
			
			
			var target = $('.link_target').val();
			var subsite = $('.link_subsite').val();
			var text = $('#link_text').val();
			var url = $('.link_url').val();
			
			if(subsite != 0)
			{
				url = rootUrl+"site/"+subsite;
			}
			
			var link = '<div class="draggable_link"><a style="text-decoration:none;color:#929291;"target="'+target+'" href="'+url+'"><span style="font-size:18px;">»</span> '+text+'</a></div>';
			
			var container = $('#sorting_holder');
			
			container.append(link);
			this.addSorting(container);
				
				
			});
		},
		
		addSorting: function(container)
		{
			container.sortable({
			  stop: function( event, ui ) 
			  {
			  		var elem = $('.draggable_link');
	
			  }
			});
		},
		
		
		setSidebarImage: function(fname)
		{
			this.right_side_img = fname;
			$('.prop_right_image_fname').empty();
			$('.prop_right_image_fname').val(fname);
		},
		
		remove_right_side_image: function()
		{
			this.setSidebarImage('');
			this.setSidebarText('');
		},
		
		setSidebarText: function(text)
		{
			$('.prop_right_image_text').empty();
			$('.prop_right_image_text').val(text);
			this.right_side_text = text;
		}
		
		
	}
	
	return new_elem;
}