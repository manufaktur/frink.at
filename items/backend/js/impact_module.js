

function new_impact_module(id)
{
	var new_elem = 
	{
		id: id,	
		ordering: 0,
		db_id: null,
		selector: null,
		textarea: null,
		preview: null,
		type: 'impact',
		text: '',
		text_left: '',
		text_middle: '',
		text_right: '',
		
		color_left: 0,
		align_left: 0,
		color_middle: 0,
		align_middle: 0,
		color_right: 0,
		align_right: 0,
		
		state: null,
		align: 0,
		
		left_title: '',
		middle_title: '',
		right_title: '',
		font_size: 15,
		text_align: 0,
		
		right_side_img: '',
		right_side_img_top: 0,
		right_side_text: '',
		uploadpath: 'items/general/uploads/impact_images',
		
		// MODULE BASIC
		init: function(parent, drop_top)
		{
			
			var html = '<div top_pos="'+drop_top+'" class="module module_impact" module_id="' + this.id + '" style="top:'+drop_top+'px">'+
							'<div class="module_impact_left impact_item" width=250 height=50>'+
								'<img class="impact_left_image impact_image" src="' + rootUrl + 'items/backend/img/image_upload_placeholder.png' + '" />'+
								'<div class="impact_left_title impact_title"></div>'+
								'<textarea class="impact_content_left impact_content module_text_html"></textarea>'+
							'</div>'+
							'<div class="module_impact_mid impact_item" width=250 height=50>'+
								'<img class="impact_middle_image impact_image" src="' + rootUrl + 'items/backend/img/image_upload_placeholder.png' + '" />'+
								'<div class="impact_middle_title impact_title"></div>'+
								'<textarea class="impact_content_middle impact_content "></textarea>'+
							'</div>'+
							'<div class="module_impact_right impact_item" width=250 height=50>'+
								'<img class="impact_right_image impact_image" src="' + rootUrl + 'items/backend/img/image_upload_placeholder.png' + '" />'+
								'<div class="impact_right_title impact_title"></div>'+
								'<textarea class="impact_content_right impact_content "></textarea>'+
							'</div>'+
						'</div>';
			parent.append(html);
			this.selector = '.module[module_id="' + this.id + '"]';
			this.textarea_left = '.module[module_id="' + this.id + '"] .impact_content_left';
			this.textarea_right = '.module[module_id="' + this.id + '"] .impact_content_right';
			this.textarea_middle = '.module[module_id="' + this.id + '"] .impact_content_middle';
			this.title_left = '.module[module_id="' + this.id + '"] .impact_left_title';
			this.title_mid = '.module[module_id="' + this.id + '"] .impact_middle_title';
			this.title_right = '.module[module_id="' + this.id + '"] .impact_right_title';
			this.preview = '.module[module_id="' + this.id + '"] .module_text_clear';
			this.state = 'preview';
			this.bindListeners();
		},
		
		bindListeners: function()
		{
			$(this.textarea_left).keyup(function(e)
			{
				$this = $(this);
				this.text_left = $this.val();
				
			});
			
			
			$(this.textarea_middle).keyup(function(e)
			{
				$this = $(this);
				this.text_mid = $this.val();
			
			});
			
			$(this.textarea_right).keyup(function(e)
			{
				$this = $(this);
				this.text_right = $this.val();
			
			});
			
			
			$('.module').draggable(
			{
				axis: 'y',
				grid: [10,10],
				containment: '.drag_contain',
				stop:function(event,ui) {	       
			        var pos = ui.helper.position();
			        $(this).attr('top_pos', pos.top);
			        $(this).attr('left_pos', pos.left);
			       
			    }
				
			});
			
			
			$(this.textarea_left).focus(function()
			{
				modules[$(this).parent().parent().attr('module_id')].setActive();
			});
			
			$(this.textarea_middle).focus(function()
			{
				modules[$(this).parent().parent().attr('module_id')].setActive();
			});
			
			$(this.textarea_right).focus(function()
			{
				modules[$(this).parent().parent().attr('module_id')].setActive();
			});
			
			
			$(this.preview).click(function()
			{
				modules[$(this).parent().attr('module_id')].setActive();
			});
		},
		
		unbindListeners: function()
		{
			$(this.textarea).unbind('keyup');
			$(this.textarea).unbind('focus');
		},
		
		showProperty: function(parent)
		{
			parent.fadeIn(0);
			parent.attr('module_id', this.id);
			parent.empty();
			html = '<table>';
		
			html += '<tr> <td class="module_properties_label"><label>Left Title</label></td> <td class="module_properties_input"><input type="text" method="setLeftTitle" value="' + this.left_title + '"></td> </tr>';
			
			html += '<tr> <td class="module_properties_label"><label>Middle title</label></td> <td class="module_properties_input"><input type="text" method="setMiddleTitle" value="' + this.middle_title + '"></td> </tr>';
			
			html += '<tr> <td class="module_properties_label"><label>Right title</label></td> <td class="module_properties_input"><input type="text" method="setRightTitle" value="' + this.right_title + '"></td> </tr>';	
			
			
			html += '<tr> <td colspan="2"> <div class="module_properties_button" method="uploadLeft">Upload left image</div> <input type="file" id="module_properties_upload_button_left" accept=".png,.jpg,.jpeg"/> </td> </tr>';
			html += '<tr> <td colspan="2"> <div class="module_properties_button" method="uploadMiddle">Upload middle image</div> <input type="file" id="module_properties_upload_button_middle" accept=".png,.jpg,.jpeg"/> </td> </tr>';
			html += '<tr> <td colspan="2"> <div class="module_properties_button" method="uploadRight">Upload right image</div> <input type="file" id="module_properties_upload_button_right" accept=".png,.jpg,.jpeg"/> </td> </tr>';
			
			html += '<tr> <td class="module_properties_label"><label>Left Font color</label></td> <td class="module_properties_color"><select id="colorLeft" method="setFontColorLeft">';
			for(var i = 0 ; i < colors.length ; i++)
			{
				html += '<option value=' + i + ' hexcode="' + colors[i].hexcode + '"';
				if(colors[i].id == this.font_color)
					html += 'SELECTED';
				html += '>' + colors[i].name + '</option>';
			}
			html += '</select></td></tr>'; 

			html += '<tr> <td class="module_properties_label"><label>Left Text align</label></td> <td class="module_properties_select"><select id="alignLeft" method="setTextAlignLeft">';
			for(var i = 0 ; i < text_align.length ; i++)
			{
				html += '<option value=' + i + ' ';
				if(i == this.align)
					html += 'SELECTED';
				html += '>' + text_align[i] + '</option>';
			}
			html += '</select></td></tr>';
			
			html += '<tr> <td class="module_properties_label"><label>Mid Font color</label></td> <td class="module_properties_color"><select id="colorMiddle" method="setFontColorMiddle">';
			for(var i = 0 ; i < colors.length ; i++)
			{
				html += '<option value=' + i + ' hexcode="' + colors[i].hexcode + '"';
				if(colors[i].id == this.font_color)
					html += 'SELECTED';
				html += '>' + colors[i].name + '</option>';
			}
			html += '</select></td></tr>'; 

			html += '<tr> <td class="module_properties_label"><label>Mid Text align</label></td> <td class="module_properties_select"><select id="alignMiddle" method="setTextAlignMiddle">';
			for(var i = 0 ; i < text_align.length ; i++)
			{
				html += '<option value=' + i + ' ';
				if(i == this.align)
					html += 'SELECTED';
				html += '>' + text_align[i] + '</option>';
			}
			html += '</select></td></tr>';
			
			
			html += '<tr> <td class="module_properties_label"><label>Right Font color</label></td> <td class="module_properties_color"><select id="colorRight" method="setFontColorRight">';
			for(var i = 0 ; i < colors.length ; i++)
			{
				html += '<option value=' + i + ' hexcode="' + colors[i].hexcode + '"';
				if(colors[i].id == this.font_color)
					html += 'SELECTED';
				html += '>' + colors[i].name + '</option>';
			}
			html += '</select></td></tr>'; 

			html += '<tr> <td class="module_properties_label"><label>Right Text align</label></td> <td class="module_properties_select"><select id="alignRight" method="setTextAlignRight">';
			for(var i = 0 ; i < text_align.length ; i++)
			{
				html += '<option value=' + i + ' ';
				if(i == this.align)
					html += 'SELECTED';
				html += '>' + text_align[i] + '</option>';
			}
			html += '</select></td></tr>';
			
			
			
			html += '</table>';
			
			parent.html(html);
			this.uploadListeners();
			togglePropertyListeners(true);
		},
		
		activate: function(focus_on_input)
		{
			if(this.state == 'editor')
				$(this.textarea).focus();
			else
				this.setActive();
		},
		
		setActive: function()
		{
			if(active_module != null)
				active_module.blur(this.id);
			
			active_module = modules[this.id];
			
			
			this.showProperty(module_properties);	
			this.showOptions();
			this.changeState('editor');
		},
		
		blur: function(new_active)
		{
			if(new_active != this.id)
			{
				this.changeState('preview');
				this.hideOptions();
				if(this.text.length == 0)
				{
					$(this.selector).find('.module_remove').click();
				}
			}

		},
		
		showOptions: function()
		{
			if($(this.selector).find('.module_options').length == 0)
			{
				var html = '<div class="module_options">';
		
				html += module_move_option();
				html += module_remove_option();
				
				html += '</div>';
				$(this.selector).append(html);
				toggleOptionListeners(true);
				
				
			}
		},
		
		hideOptions: function()
		{
			$(this.selector).find('.module_options').remove();
		},
		
		getSaveData: function()
		{
			
			
			var ret = 
			{
				'left_content': $(this.textarea_left).val(),
				'middle_content': $(this.textarea_middle).val(),
				'right_content': $(this.textarea_right).val(),		
				'id': this.db_id,
				'type': this.type,
				'ordering': this.ordering,
				'parent_id': parent_id,
				'parent_type': parent_type,				
				'left_title': this.left_title,
				'middle_title': this.middle_title,
				'right_title': this.right_title,
				'top_pos': $(this.selector).attr('top_pos'),
				'left_pos': $(this.selector).attr('left_pos'),
				'left_image': $('.impact_left_image').attr('fname'),
				'middle_image': $('.impact_middle_image').attr('fname'),
				'right_image': $('.impact_right_image').attr('fname'),
				'left_color': this.color_left,
				'left_align': this.align_left,
				'middle_color': this.color_middle,
				'middle_align': this.align_middle,
				'right_color': this.color_right,
				'right_align': this.align_right,
				
			};
			
			return ret;
		},
	
		load: function(properties)
		{
			this.setLeftTitle(properties.left_title);
			this.setMiddleTitle(properties.middle_title);
			this.setRightTitle(properties.right_title);
			this.setTop(properties.top_pos);
			this.setLeft(properties.left_pos);
			
			this.setLeftContent(properties.left_content);
			this.setMidContent(properties.middle_content);
			this.setRightContent(properties.right_content);
			
			
			this.setLeftImage(properties.left_image);
			this.setMiddleImage(properties.middle_image);
			this.setRightImage(properties.right_image);
			
			
			this.setFontColorLeft(properties.left_color);			
			this.setTextAlignLeft(properties.left_align);
			this.setFontColorMiddle(properties.middle_color);			
			this.setTextAlignMiddle(properties.middle_align);
			this.setFontColorRight(properties.right_color);			
			this.setTextAlignRight(properties.right_align);
		},		
		
		
		// PROPERTY SETTERS/GETTERS
		setLeftContent: function(content)
		{
			$(this.textarea_left).val(content);
		},
		
		setMidContent: function(content)
		{
			$(this.textarea_middle).val(content);
		},
		
		setRightContent: function(content)
		{
			$(this.textarea_right).val(content);
		},
		
		
		//MODULE SPECIFIC
		setLeftTitle: function(content)
		{
			$(this.title_left).text(content);
			$(this.title_left).keyup();
			
			this.left_title = content;
		},
		
		setMiddleTitle: function(content)
		{
			$(this.title_mid).text(content);
			$(this.title_mid).keyup();
			
			this.middle_title = content;
		},
		
		
		setRightTitle: function(content)
		{
			$(this.title_right).text(content);
			$(this.title_right).keyup();
			
			this.right_title = content;
		},
		
		setTop: function(topP)
		{
			
			this.top_pos = topP;
			$(this.selector).css({ top: topP+'px'});
			$(this.selector).attr('top_pos', topP);
		},
		
		setLeft: function(leftP)
		{
			
			
			$(this.selector).css({ left: leftP+'px'});
			$(this.selector).attr('left_pos', leftP);
		},
		
		
		setFontColorLeft: function(color_id)
		{
			this.color_left = color_id;
			for(var i = 0 ; i < colors.length ; i++)
			{
				if(colors[i].id == color_id)
				{
					
					$(this.textarea_left).css({'color': colors[i].hexcode});
				}
					
			}
			
			this.activate();
		},
		
		setTextAlignLeft: function(align)
		{
			this.align_left = align;
			$(this.textarea_left).css({'text-align': text_align[this.align_left]});
			$(this.textarea_left).keyup();
		},
		
		
		setFontColorMiddle: function(color_id)
		{
			this.color_middle = color_id;
			for(var i = 0 ; i < colors.length ; i++)
			{
				if(colors[i].id == color_id)
				{
					$(this.textarea_middle).css({'color': colors[i].hexcode});
				}
					
			}
			
			this.activate();
		},
		
		setTextAlignMiddle: function(align)
		{
			this.align_middle = align;
			$(this.textarea_middle).css({'text-align': text_align[this.align_middle]});
			$(this.textarea_middle).keyup();
		},
		
		setFontColorRight: function(color_id)
		{
			this.color_right = color_id;
			for(var i = 0 ; i < colors.length ; i++)
			{
				if(colors[i].id == color_id)
				{			
					$(this.textarea_right).css({'color': colors[i].hexcode});
				}
					
			}
			
			this.activate();
		},
		
		setTextAlignRight: function(align)
		{
			this.align_right = align;
			$(this.textarea_right).css({'text-align': text_align[this.align_right]});
			$(this.textarea_right).keyup();
		},
		
		setFontSize: function(font_size)
		{
			this.font_size = font_size;
			$(this.textarea).css({'font-size': font_size + 'px'});
			$(this.preview).css({'font-size': font_size + 'px'});
			$(this.textarea).keyup();
		},
		
		setTemplate: function(margin_bot,margin_top, color, size)
		{
			this.setMarginBottom(margin_bot);
			this.setMarginTop(margin_top);
			this.setFontColor(color);
			this.setFontSize(size);
		},
		
		changeState: function(new_state)
		{
			if(new_state == 'preview')
			{
				$(this.selector).find('.module_preview').fadeOut(0);
				$(this.selector).find('.module_html').fadeIn(0);
				$(this.preview).fadeIn(0);
				$(this.textarea).fadeOut(0);
				$(this.selector).css({'padding-left': 15, 'padding-right': 15});
				this.hideStyling();
			}
			else
			{
				$(this.selector).find('.module_html').fadeOut(0);
				$(this.selector).find('.module_preview').fadeIn(0);
				$(this.textarea).fadeIn(0);
				$(this.preview).fadeOut(0);
				$(this.textarea).keyup();
				$(this.selector).css({'padding-left': 14, 'padding-right': 14});
				this.showStyling();
			}
			
			this.state = new_state;
		},
		
		showStyling: function()
		{
			var html = "";
			html += '<div class="module_text_styling">';
			html += '<div class="styling_option styling_bold"></div>';
			html += '<div class="styling_option styling_italic"></div>';
			html += '<div class="styling_option styling_underline"></div>';
			html += '<div class="styling_option styling_sup"></div>';
			html += '<div class="styling_option styling_sub"></div>';
			html += '<div class="styling_option styling_link"></div>';
			html += '</div>';
			$(this.selector).append(html);
			
			$(this.selector).find('.styling_bold').click(function()
			{
				active_module.applyTag('<b>', '</b>');
			});
			
			$(this.selector).find('.styling_italic').click(function()
			{
				active_module.applyTag('<i>', '</i>');
			});

			$(this.selector).find('.styling_underline').click(function()
			{
				active_module.applyTag('<u>', '</u>');
			});
			
			$(this.selector).find('.styling_sub').click(function()
			{
				active_module.applyTag('<sub>', '</sub>');
			});
	
			$(this.selector).find('.styling_sup').click(function()
			{
				active_module.applyTag('<sup>', '</sup>');
			});
			
			$(this.selector).find('.styling_link').click(function()
			{
				active_module.showLinkWindow();
			});
		},
		
		hideStyling: function()
		{
			$(this.selector).find('.module_text_styling').remove();
		},
		
		applyTag: function(starttag, endtag)
		{
			var ta = $(this.textarea);
			
			var sel = ta.getSelection();
			var text = ta.val();
			if(sel.start == sel.end)
			{
				var before = text.substring(0, sel.start);
				var after = text.substring(sel.start, text.length);
				
				var newtext = before + starttag + endtag + after;
			}
			else
			{
				var before = text.substring(0, sel.start);
				var after = text.substring(sel.end, text.length);
				var selection = text.substring(sel.start, sel.start + sel.length);
				var newtext = before + starttag + selection + endtag + after;
			}

			ta.val(newtext);
			$(this.textarea).keyup();
		},
		
		setLeftImage: function(fname)
		{
			if(fname != null)
			{
				var src = rootUrl+this.uploadpath+'/'+fname;
				$('.impact_left_image').attr('src', src);
				$('.impact_left_image').attr('fname', fname);
			}
		},
		
		setMiddleImage: function(fname)
		{
			if(fname != null)
			{
				var src = rootUrl+this.uploadpath+'/'+fname;
				$('.impact_middle_image').attr('src', src);
				$('.impact_middle_image').attr('fname', fname);
				
			}
		},
		
		setRightImage: function(fname)
		{
			if(fname != null)
			{
				var src = rootUrl+this.uploadpath+'/'+fname;
				$('.impact_right_image').attr('src', src);
				$('.impact_right_image').attr('fname', fname);
			}
			

		},
		
		
		
		showLinkWindow: function()
		{
			var html = "";
			html += '<div class="text_edit_popup">';
			
			html += '<div class="text_edit_popup_headline">Add link</div>';
			
			html += '<table class="text_edit_popup_content">';
			html += '<tr>  <td class="module_properties_label">  <label>Target</label>  </td>  <td class="module_properties_target">  <select class="link_target">';
			for(var i = 0 ; i < targets.length ; i++)
			{
				html += '<option value="' + targets[i].id + '"';			
				html += '>' + targets[i].name + '</option>';
			}
			html += '</select>  </td>  </tr>';
			html += '<tr>  <td class="module_properties_label">  <label>Subsite</label>  </td>  <td class="module_properties_subsite">  <select class="link_subsite">';
			for(var i = 0 ; i < subsites.length ; i++)
			{
				html += '<option value="' + subsites[i].id + '"';
				html += '>' + subsites[i].name + '</option>';
			}
			html += '</select>  </td>  </tr>';
			html += '<tr>  <td class="module_properties_label">  <label>URL</label>  </td>  <td class="module_properties_input">  <input type="text" class="link_url" value=""></td>  </tr>';
			html += '</table>';
			
			html += '<div class="text_edit_popup_buttons">';
			html += '<div class="text_edit_popup_button link_cancel">Cancel</div>';
			html += '<div class="text_edit_popup_button link_add">Add</div>';
			html += '</div>';
			
			html += '</div>';
			$("body").append(html);
			
			$('.link_add').click(function()
			{
				var starttag = '';
				var endtag = '</a>';
				switch($('.link_target').val())
				{
					case '0':
						var target = '_self';
						break;
					case '1':
						var target = '_blank';
						break;
				}
				
				if($('.link_subsite').val() == '0')
					var href = $('.link_url').val();
				else
					var href = rootUrl + "subsite/" + $('.link_subsite').val();
					
				starttag = '<a href="' + href + '" target="' + target + '">';
				
				active_module.applyTag(starttag, endtag);
				$('.text_edit_popup').remove();
			});
			
			$('.link_cancel').click(function()
			{
				$('.text_edit_popup').remove();
			});
			
		},
		
		uploadLeft: function()
		{
			
			$('#module_properties_upload_button_left').click();
		},
		
		uploadMiddle: function()
		{
			
			$('#module_properties_upload_button_middle').click();
		},
		
		uploadRight: function()
		{
			
			$('#module_properties_upload_button_right').click();
		},
		
		
		uploadListeners: function()
		{
			$('#module_properties_upload_button_left').change(function()
			{
				var element = $(this).attr('id');
				var file = document.getElementById(element).files[0];
				var uploadpath = active_module.uploadpath;
				var reader = new FileReader();
				var url;
				reader.readAsDataURL (file);
				reader.onload = function(event)
				{
					var result = event.target.result;
					$.ajax(
					{
						url: rootUrl + 'backend/content_download_fileupload',
						data: { filename: file.name, data: result , uploadpath: uploadpath},
						method: 'POST',
						success: function(data)
						{
							var ret = $.parseJSON(data);
							
							if(ret.success)
							{
								$(this.selector).attr('left_image', ret.filename);
								
								
								var new_src = rootUrl+uploadpath+'/'+ret.filename;
								
								$('.impact_left_image').attr('src', new_src);
								$('.impact_left_image').attr('fname', ret.filename);
							}
							else
							{
								alert('Error while uploading');
							}
						}
					});			
				};
			});
			
			$('#module_properties_upload_button_middle').change(function()
			{
				var element = $(this).attr('id');
				var file = document.getElementById(element).files[0];
				var uploadpath = active_module.uploadpath;
				var reader = new FileReader();
				var url;
				reader.readAsDataURL (file);
				reader.onload = function(event)
				{
					var result = event.target.result;
					$.ajax(
					{
						url: rootUrl + 'backend/content_download_fileupload',
						data: { filename: file.name, data: result , uploadpath: uploadpath},
						method: 'POST',
						success: function(data)
						{
							var ret = $.parseJSON(data);
							
							if(ret.success)
							{
								$(this.selector).attr('middle_image', ret.filename);
								
								var new_src = rootUrl+uploadpath+'/'+ret.filename;
								
								$('.impact_middle_image').attr('src', new_src);
								$('.impact_middle_image').attr('fname', ret.filename);
							}
							else
							{
								alert('Error while uploading');
							}
						}
					});			
				};
			});
			
			$('#module_properties_upload_button_right').change(function()
			{
				var element = $(this).attr('id');
				var file = document.getElementById(element).files[0];
				var uploadpath = active_module.uploadpath;
				var reader = new FileReader();
				var url;
				reader.readAsDataURL (file);
				reader.onload = function(event)
				{
					var result = event.target.result;
					$.ajax(
					{
						url: rootUrl + 'backend/content_download_fileupload',
						data: { filename: file.name, data: result , uploadpath: uploadpath},
						method: 'POST',
						success: function(data)
						{
							var ret = $.parseJSON(data);
							
							if(ret.success)
							{
								$(this.selector).attr('right_image', ret.filename);
								var new_src = rootUrl+uploadpath+'/'+ret.filename;
								
								$('.impact_right_image').attr('src', new_src);
								$('.impact_right_image').attr('fname', ret.filename);
							}
							else
							{
								alert('Error while uploading');
							}
						}
					});			
				};
			});
		},
		
		setSidebarImage: function(fname)
		{
			this.right_side_img = fname;
			$('.prop_right_image_fname').empty();
			$('.prop_right_image_fname').val(fname);
		},
		
		remove_right_side_image: function()
		{
			this.setSidebarImage('');
			this.setSidebarText('');
		},
		
		setSidebarText: function(text)
		{
			$('.prop_right_image_text').empty();
			$('.prop_right_image_text').val(text);
			this.right_side_text = text;
		}
		
		
	}
	
	return new_elem;
}