

function new_image_module(id)
{
	var new_elem = 
	{
		id: id,	
		ordering: 0,
		db_id: null,
		selector: null,
		img: null,
		type: 'image',
		text: '',
		uploadpath: 'items/general/uploads/content_img',
		fname: '',
		margin_top: 0,
		margin_bottom: 0,
		align: 2,
		width: 365,
		
		
		// MODULE BASIC
		init: function(parent,drop_top)
		{
			var html = '<div top_pos="'+drop_top+'" class="module module_image" module_id="' + this.id + '" style="top:'+drop_top+'px"><img src="' + rootUrl + 'items/backend/img/image_upload_placeholder.png' + '" /></div>';
			parent.append(html);
			this.selector = '.module[module_id="' + this.id + '"]';
			this.img = '.module[module_id="' + this.id + '"] img';
			this.bindListeners();
		},
		
		bindListeners: function()
		{
			$(this.selector).click(function()
			{
				modules[$(this).attr('module_id')].setActive();
			});
			
			
			$(this.selector).draggable(
			{
				
				grid: [380,10],
				containment: '.drag_contain',
				stop:function(event,ui) {	       
			        var pos = ui.helper.position();
			        $(this).attr('top_pos', pos.top);
			        $(this).attr('left_pos', pos.left);
			       
			    }
				
			});
			
		},
		
		unbindListeners: function()
		{
			$(this.selector).unbind('click');
		},
		
		showProperty: function(parent)
		{
			parent.fadeIn(0);
			parent.attr('module_id', this.id);
			parent.empty();
			html = '<table>';
			html += '<tr> <td class="module_properties_label"><label>Image size</label></td> <td class="module_properties_width"><select id="wSelect" method="setWidth">';
			
				html += '<option value="365"';
			/*	if(colors[i].id == this.font_color)
					html += 'SELECTED'; */
				html += '>One Column</option>';
				
				html += '<option value="760"';
			/*	if(colors[i].id == this.font_color)
					html += 'SELECTED'; */
				html += '>Two Column</option>';
			
			html += '</select></td></tr>'; 
			
			html += '<tr> <td class="module_properties_label"><label>Text</label></td> <td class="module_properties_input"><input type="text" id="imgtext" method="setImageText" value="' + this.text + '"></td> </tr>';
			
			
			html += '<tr> <td colspan="2"> <div class="module_properties_button" method="upload">Upload</div> <input type="file" id="module_properties_upload_button" accept=".png,.jpg,.jpeg"/> </td> </tr>';
			html += '</table>';
			
			parent.html(html);
			this.uploadListeners();
			$('#wSelect option[value='+this.width+']').prop('selected', true);
			
			togglePropertyListeners(true);
		},
		
		activate: function()
		{
			this.setActive();
		},
		
		blur: function(new_active)
		{
			if(new_active != this.id)
			{
				this.hideOptions();
			}
		},

		setActive: function()
		{
			if(active_module != null)
				active_module.blur(this.id);
			
			active_module = modules[this.id];
			
			this.showProperty(module_properties);
			this.showOptions();
		},
		
		showOptions: function()
		{
			var html = '<div class="module_options">';
			html += module_move_option();
			html += module_remove_option();
			html += '</div>';
			$(this.selector).append(html);
			toggleOptionListeners(true);
		},
		
		hideOptions: function()
		{
			$(this.selector).find('.module_options').remove();
		},
		
		
		// PROPERTY SETTERS/GETTERS
		setMarginTop: function(margin_top)
		{
			this.margin_top = parseInt(margin_top);
			$(this.selector).css({'margin-top': this.margin_top});
		},
		
		setMarginBottom: function(margin_bottom)
		{
			this.margin_bottom = parseInt(margin_bottom);
			$(this.selector).css({'margin-bottom': this.margin_bottom});
		},
		
		getSaveData: function()
		{
			var ret = 
			{
				'fname': this.fname,
				'margin_top': this.margin_top,
				'margin_bottom': this.margin_bottom,
				'id': this.db_id,
				'type': this.type,
				'ordering': this.ordering,
				'parent_id': parent_id,
				'parent_type': parent_type,
				'width': this.width,
				'text': this.text,
				'top_pos': $(this.selector).attr('top_pos'),
				'left_pos': $(this.selector).attr('left_pos'),
			};
			
			return ret;
		},
		
		load: function(properties)
		{		
			this.setImage(properties);
			this.setText(properties.text);
			this.setMarginBottom(properties.margin_bottom);
			this.setMarginTop(properties.margin_top);
			this.setTop(properties.top_pos);
			this.setLeft(properties.left_pos);
		},
		
		
		
		//MODULE SPECIFIC
		upload: function()
		{
			$('.module_properties_button[method="upload"]').parent().find('input').click();
		},
		
		uploadListeners: function()
		{
			$('#module_properties_upload_button').change(function()
			{
				var element = $(this).attr('id');
				var file = document.getElementById(element).files[0];
				var uploadpath = active_module.uploadpath;
				var reader = new FileReader();
				var url;
				reader.readAsDataURL (file);
				reader.onload = function(event)
				{
					var result = event.target.result;
					$.ajax(
					{
						url: rootUrl + 'backend/content_imageupload',
						data: { filename: file.name, data: result , uploadpath: uploadpath},
						method: 'POST',
						success: function(data)
						{
							var ret = $.parseJSON(data);
							
							if(ret.success)
							{
								$(active_module.img).attr('src', rootUrl + uploadpath + '/' + ret.filename);
								active_module.fname = ret.filename;
								imagesLoaded(active_module.img, function()
								{
									
									//active_module.igniteResize(true);
									//active_module.setImageWidth($(active_module.img).width());
								});
							}
							else
							{
								alert('Error while uploading');
							}
						}
					});			
				};
			});
		},
		
		
		setImageText: function()
		{
			
			var image_text = $('#imgtext').val();
			
			this.text = image_text;
			this.setText(image_text);
		},
		
		setText: function(text)
		{
			
			this.text = text;
			
		},
		
		
		setTop: function(topP)
		{
			
			this.top_pos = topP;
			$(this.selector).css({ top: topP+'px'});
			$(this.selector).attr('top_pos', topP);
		},
		
		setLeft: function(leftP)
		{
			
			
			$(this.selector).css({ left: leftP+'px'});
			$(this.selector).attr('left_pos', leftP);
		},
		
		setImage: function(properties)
		{
			$(active_module.img).attr('src', rootUrl + this.uploadpath + '/' + properties.fname);
			$(active_module.img).css({width: properties.width, 'margin-left': 'auto', 'margin-right': 'auto'});
			this.fname = properties.fname;
			this.igniteResize(false);
			this.setImageWidth(properties.width);
			$('#wSelect option[value='+this.width+']').prop('selected', true);
			
			
			//$(active_module.img).parent().css({width: properties.width, 'margin-left': 'auto', 'margin-right': 'auto'});
		},
		
		
		setWidth: function()
		{
			var selected_width = $('.module_properties_width select').val();
			
			this.width = selected_width;
			this.setImageWidth(selected_width);
		},
		
		setImageWidth: function(width)
		{
			
			this.width = width;
			$(this.img).parent().width(width);
			$(this.img).width(width);
			
		},
				
		igniteResize: function(isnew)
		{
			if(isnew)
			{
				$(active_module.img).resizable(
				{
					aspectRatio: true,
					ghost: true,
					maxWidth: 380,
					handles: 's',
					
					start: function(event, ui)
					{
						modules[ui.element.parent().attr('module_id')].activate();
					},
			      
			        stop: function( event, ui ) 
			        {
				      	$(active_module.img).parent().css({'margin-left': 'auto', 'margin-right': 'auto'});
				      	$(active_module.img).css({'margin-left': 'auto', 'margin-right': 'auto'});
				      	active_module.setImageWidth($(active_module.img).width());
				    },
			        create: function( event, ui ) 
			        {
			        	$(active_module.img).css({width:'auto', height: 'auto','margin-left': 'auto', 'margin-right': 'auto'}); 
				    	$(active_module.img).parent().css({width:'auto', height: 'auto','margin-left': 'auto', 'margin-right': 'auto'});
			        }
			    });
			}
			else
			{
				$(active_module.img).resizable(
				{
					aspectRatio: true,
					ghost: true,
					maxWidth: 380,
					handles: 's',
					
					start: function(event, ui)
					{
						modules[ui.element.parent().attr('module_id')].activate();
					},
			      
			        stop: function( event, ui ) 
			        {
			          	$(active_module.img).parent().css({'margin-left': 'auto', 'margin-right': 'auto'});
		      	      	$(active_module.img).css({'margin-left': 'auto', 'margin-right': 'auto'});
		      	      	active_module.setImageWidth($(active_module.img).width());
				    },
			        create: function( event, ui ) 
			        {
			        	$(active_module.img).css({height: 'auto','margin-left': 'auto', 'margin-right': 'auto'}); 
				    	$(active_module.img).parent().css({ height: 'auto','margin-left': 'auto', 'margin-right': 'auto'});
			        }
			    });
			}
		},
	}
	
	return new_elem;
}