

function new_gallery_module(id)
{
	var new_elem = 
	{
		id: id,	
		ordering: 0,
		db_id: null,
		selector: null,
		
		type: 'gallery',
		uploadpath: 'items/general/uploads/content_gallery_img',
		gallery_items: new Array(),
		
		items_per_row: 3,
		margin_top: 0,
		margin_bottom: 0,
		align: 2,
		
		// MODULE BASIC
		init: function(parent, drop_top)
		{
			var html  = '<div top_pos="'+drop_top+'" left_pos="0" style="top:'+drop_top+'px" class="module module_gallery" module_id="' + this.id + '"></div>';
			parent.append(html);
			this.selector = '.module[module_id="' + this.id + '"]';
			this.img = '.module[module_id="' + this.id + '"] img';
			this.bindListeners();
		},
		
		bindListeners: function()
		{
			$(this.selector).click(function()
			{
				modules[$(this).attr('module_id')].setActive();
			});
			
			$(this.selector).draggable(
			{
				
				grid: [380,10],
				containment: '.drag_contain',
				stop:function(event,ui) {	       
			        var pos = ui.helper.position();
			        $(this).attr('top_pos', pos.top);
			        $(this).attr('left_pos', pos.left);
			       
			    }
				
			});
		},
		
		bindDeleteListeners: function()
		{
			$('.gallery_image_delete').click(function(e)
			{
				var fname = $(this).attr('fname');
				
				active_module.gallery_items = $.grep(active_module.gallery_items, function(value) 
				{
					return value != fname;
				});
				
				$(this).parent().remove();
				
				e.preventDefault();
			});		
		},
		
		unbindListeners: function()
		{
			$(this.selector).unbind('click');
		},
		
		showProperty: function(parent)
		{
			parent.fadeIn(0);
			parent.attr('module_id', this.id);
			parent.empty();
			html = '<table>';
			html += '<tr> <td class="module_properties_label"><label>Margin-Top</label></td> <td class="module_properties_input"><input type="text" method="setMarginTop" value="' + this.margin_top + '"></td> </tr>';
			html += '<tr> <td class="module_properties_label"><label>Margin-Bottom</label></td> <td class="module_properties_input"><input type="text" method="setMarginBottom" value="' + this.margin_bottom + '"></td> </tr>';
			html += '<tr> <td colspan="2"> <div class="module_properties_button" method="upload">ADD IMAGE TO GALLERY</div> <input type="file" id="module_properties_upload_button" accept=".png,.jpg,.jpeg" multiple/> </td> </tr>';
			html += '<tr> <td colspan="2"> <div class="module_properties_button" method="saveImageTexts">SAVE IMAGE TEXTS</div> </td> </tr>';
			html += '</table>';
			
			parent.html(html);
			this.uploadListeners();
			togglePropertyListeners(true);
		},
		
		activate: function()
		{
			this.setActive();
		},
		
		blur: function(new_active)
		{
			if(new_active != this.id)
			{
				this.hideRemoveIcons();
				this.hideOptions();
				if(this.gallery_items.length == 0)
				{
					$(this.selector).find('.module_remove').click();
				}
			}
		},

		setActive: function()
		{
			if(active_module != null)
				active_module.blur(this.id);
			
			active_module = modules[this.id];
			this.showProperty(module_properties);
			this.showOptions();
			this.showRemoveIcons();
		},
		
		showOptions: function()
		{
			var html = '<div class="module_options">';
			html += module_move_option();
			html += module_remove_option();
			html += '</div>';
			$(this.selector).append(html);
			
			toggleOptionListeners(true);
		},
		
		hideOptions: function()
		{
			$(this.selector).find('.module_options').remove();
		},
		
		
		// PROPERTY SETTERS/GETTERS
		setMarginTop: function(margin_top)
		{
			this.margin_top = parseInt(margin_top);
			$(this.selector).css({'margin-top': this.margin_top});
		},
		
		setMarginBottom: function(margin_bottom)
		{
			this.margin_bottom = parseInt(margin_bottom);
			$(this.selector).css({'margin-bottom': this.margin_bottom});
		},
		
		getSaveData: function()
		{
			var ret = 
			{
				'margin_top': this.margin_top,
				'margin_bottom': this.margin_bottom,
				'id': this.db_id,
				'type': this.type,
				'ordering': this.ordering,
				'parent_id': parent_id,
				'parent_type': parent_type,
				'images': this.gallery_items,
				'top_pos': $(this.selector).attr('top_pos'),
				'left_pos': $(this.selector).attr('left_pos'),
			};
			
			return ret;
		},
		
		load: function(properties)
		{
			this.setImages(properties.images);
			this.setMarginBottom(properties.margin_bottom);
			this.setMarginTop(properties.margin_top);
			this.setTop(properties.top_pos);
			this.setLeft(properties.left_pos);
		},
		
		
		
		//MODULE SPECIFIC
		upload: function()
		{
			$('.module_properties_button[method="upload"]').parent().find('input').click();
		},
		
		uploadListeners: function()
		{
			$('#module_properties_upload_button').change(function()
			{
				var element = $(this).attr('id');
				
				
				for (var i = 0; i < $(this).get(0).files.length; ++i) {
			        var file = document.getElementById(element).files[i];
					
					var uploadpath = active_module.uploadpath;
					var reader = new FileReader();
					var url;
					reader.readAsDataURL (file);
					reader.onload = function(event)
					{
						var result = event.target.result;
						
						$.ajax(
						{
							url: rootUrl + 'backend/content_galleryimageupload',
							data: { filename: file.name, data: result , uploadpath: uploadpath},
							method: 'POST',
							success: function(data)
							{
								var ret = $.parseJSON(data);
								
								if(ret.success)
								{
								
									var elem = '<div class="gallery_item_holder" >'+
													'<div class="gallery_image_delete" fname="'+ret.filename+'"></div>'+
													'<div class="gallery_image" fname="'+ret.filename+' "style="background-image: url('+rootUrl + uploadpath + '/' + ret.filename+');" ></div>'+
													'<input type="text" fname="'+ret.filename+'" class="gallery_image_text" "style="" value="" />'+
												'</div>';
												
									$(active_module.selector).append(elem);							
									active_module.fname = ret.filename;
									//active_module.gallery_items.push(ret.filename);
									active_module.gallery_items.push({
								      fname: ret.fname,
								      text: ''
								    });
									active_module.bindDeleteListeners();
								}
								else
								{
									alert('Error while uploading');
								}
							}
						});			
					};
				}
			});
		},
		
		setImages: function(images)
		{
			
			$.each(images, function( index, item ) 
			{
			  /*	var elem = '<div class="gallery_item_holder">'+
			  					'<div class="gallery_image_delete" fname="'+item.fname+'"></div>'+
								'<img class="gallery_image" fname="'+item.fname+'" src="'+rootUrl + active_module.uploadpath + '/' +item.fname+'" />'+
							'</div>';
			*/
			    var saved_text = item.text;
			    
			    
			    if(saved_text === null)
			    {
				    saved_text = "";
			    }
			    
				var elem = '<div class="gallery_item_holder">'+
			  					'<div class="gallery_image_delete" fname="'+item.fname+'"></div>'+
			  					'<div class="gallery_image" fname="'+item.fname+' "style="background-image: url('+rootUrl + active_module.uploadpath + '/' +item.fname+');" ></div>'+
			  					'<input type="text" fname="'+item.fname+'" class="gallery_image_text" "style="" value="'+saved_text+'" />'+
							'</div>';
							
				$(active_module.selector).append(elem);							
				
				//active_module.gallery_items.push(item.fname);
				
				active_module.gallery_items.push({
			      fname: item.fname,
			      text: saved_text
			    });
			});
			
			this.bindDeleteListeners();
		},
		
		showRemoveIcons: function()
		{
			$(this.selector).find('.gallery_image_delete').fadeIn(0);
		},
		
		hideRemoveIcons: function()
		{
			$(this.selector).find('.gallery_image_delete').fadeOut(0);
		},
		setTop: function(topP)
		{
			
			this.top_pos = topP;
			$(this.selector).css({ top: topP+'px'});
			$(this.selector).attr('top_pos', topP);
		},
		
		setLeft: function(leftP)
		{
			
			
			$(this.selector).css({ left: leftP+'px'});
			$(this.selector).attr('left_pos', leftP);
		},
		
		
		saveImageTexts: function()
		{
			active_module.gallery_items = [];
			
			
			$(active_module.selector).find('.gallery_item_holder').each(function(i,item) {
		        
		        var fname = $(item).find('.gallery_image').attr('fname');
				var text = $(item).find('.gallery_image_text').val();
				
				active_module.gallery_items.push({
			      fname: fname,
			      text: text
			    });

		    });
		    
		    window.alert('Text Saved!');
		
		},
	}
	
	return new_elem;
}