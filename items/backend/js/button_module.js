

function new_button_module(id)
{
	var new_elem = 
	{
		id: id,	
		ordering: 0,
		db_id: null,
		selector: null,
		textarea: null,
		preview: null,
		type: 'button',
		text: '',
		target: '_self',
		top_pos: 0,
		url: '',
		state: null,
		align: 0,
		
		margin_top: 0,
		margin_bottom: 0,
		font_color: 1,
		font_size: 15,
		text_align: 0,
				
		
		// MODULE BASIC
		init: function(parent, drop_top)
		{
			var html = '<div top_pos="'+drop_top+'" left_pos="0" class="module module_button" module_id="' + this.id + '" style="top:'+drop_top+'px"></div>';
			
			parent.append(html);
			
			this.selector = '.module[module_id="' + this.id + '"]';
			
			this.state = 'preview';
			this.bindListeners();
			
			
			
		},
		
		bindListeners: function()
		{
			$(this.selector).click(function()
			{
			
				modules[$(this).attr('module_id')].setActive();
			});
			
			$(this.textarea).focus(function()
			{
				modules[$(this).parent().attr('module_id')].setActive();
			});
			
			$(this.preview).click(function()
			{
				modules[$(this).parent().attr('module_id')].setActive();
			});
			
			$(this.selector).draggable(
			{
				
				grid: [380,10],
				containment: '.drag_contain',
				stop:function(event,ui) {	       
			        var pos = ui.helper.position();
			        $(this).attr('top_pos', pos.top);
			        $(this).attr('left_pos', pos.left);
			       
			    }
				
			});
		},
		
		unbindListeners: function()
		{
			$(this.textarea).unbind('keyup');
			$(this.textarea).unbind('focus');
		},
			
		
		showProperty: function(parent)
		{
			parent.fadeIn(0);
			parent.attr('module_id', this.id);
			parent.empty();
			
			html = '<table >';
			html += '<tr>  <td class="module_properties_label">  <label>Text</label>  </td>  <td class="module_properties_input">  <textarea id="link_text" method="setText" value="" style="width:144px;resize:none;"></textarea></td>  </tr>';
			html += '<tr>  <td class="module_properties_label">  <label>Target</label>  </td>  <td class="module_properties_select">  <select method="setTarget" class="link_target">';
			for(var i = 0 ; i < targets.length ; i++)
			{
				html += '<option value="' + targets[i].value + '"';			
				html += '>' + targets[i].name + '</option>';
			}
			html += '</select>  </td>  </tr>';
			html += '<tr>  <td class="module_properties_label">  <label>Subsite</label>  </td>  <td class="module_properties_select">  <select method="setSubsite" class="link_subsite">';
			for(var i = 0 ; i < subsites.length ; i++)
			{
				html += '<option value="' + subsites[i].id + '"';
				html += '>' + subsites[i].name + '</option>';
			}
			html += '</select>  </td>  </tr>';
			html += '<tr>  <td class="module_properties_label">  <label>URL</label>  </td>  <td class="module_properties_input">  <input method="setUrl" type="text" class="link_url" value=""></td>  </tr>';
			html += '</table>';
			
			
			parent.html(html);
			togglePropertyListeners(true);
		},
		
		activate: function(focus_on_input)
		{
			if(this.state == 'editor')
				$(this.textarea).focus();
			else
				this.setActive();
		},
		
		setActive: function()
		{
			if(active_module != null)
				active_module.blur(this.id);
			
			active_module = modules[this.id];
			
			
			this.showProperty(module_properties);	
			this.showOptions();
			//this.changeState('editor');
		},
		
		blur: function(new_active)
		{
			if(new_active != this.id)
			{
				//this.changeState('preview');
				this.hideOptions();
				
			}

		},
		
		showOptions: function()
		{
			if($(this.selector).find('.module_options').length == 0)
			{
				var html = '<div class="module_options">';
				html += module_move_option();
				html += module_remove_option();	
				html += '</div>';
				$(this.selector).append(html);
				toggleOptionListeners(true);
			}
		},
		
		hideOptions: function()
		{
			$(this.selector).find('.module_options').remove();
		},
		
		getSaveData: function()
		{
			
			var ret = 
			{				
				'id': this.db_id,
				'type': this.type,
				'ordering': this.ordering,
				'top_pos': $(this.selector).attr('top_pos'),
				'left_pos': $(this.selector).attr('left_pos'),
				'parent_id': parent_id,
				'parent_type': parent_type,
				'target': this.target,
				'text': this.text,
				'url': this.url,
			};
			
			return ret;
		},
	
		load: function(properties)
		{
			this.setText(properties.text);
			$('#link_text').val(properties.text.replace('<br />', '\r\n'));
			this.setTarget(properties.target);
			$('.link_target').val(properties.target);
			this.setUrl(properties.url);
			$('.link_url').val(properties.url);
			this.setTop(properties.top_pos);
			this.setLeft(properties.left_pos);
			
		},		
		
		//MODULE SPECIFIC
		setText: function(content)
		{	
			if(content)
			{
				content = content.replace(/(?:\r\n|\r|\n)/g, '<br />');
			}
			$('.module_button[module_id="' + this.id + '"]').html(content);
			this.text = content;
			
		},
		
		setTop: function(topP)
		{
			
			this.top_pos = topP;
			$(this.selector).css({ top: topP+'px'});
			$(this.selector).attr('top_pos', topP);
		},
		
		setLeft: function(leftP)
		{
			
			
			$(this.selector).css({ left: leftP+'px'});
			$(this.selector).attr('left_pos', leftP);
		},
		
		setTarget: function(target)
		{
			
			this.target = target;
			
		},
		
		setUrl: function(url)
		{
			this.url = url;
		},
		
		setSubsite: function(subsite)
		{
			if(subsite != 0)
			{
				this.url = rootUrl+'site/'+subsite;
			}
			else
			{
				this.url = $('.link_url').val();
			}
		},
		
		changeState: function(new_state)
		{
			if(new_state == 'preview')
			{
				$(this.selector).find('.module_preview').fadeOut(0);
				$(this.selector).find('.module_html').fadeIn(0);
				$(this.preview).fadeIn(0);
				$(this.textarea).fadeOut(0);
				//$(this.selector).css({'padding-left': 15, 'padding-right': 15});
				this.hideStyling();
			}
			else
			{
				$(this.selector).find('.module_html').fadeOut(0);
				$(this.selector).find('.module_preview').fadeIn(0);
				$(this.textarea).fadeIn(0);
				$(this.preview).fadeOut(0);
				$(this.textarea).keyup();
				//$(this.selector).css({'padding-left': 14, 'padding-right': 14});
				//this.showStyling();
			}
			
			this.state = new_state;
		},
		
		
		applyTag: function(starttag, endtag)
		{
			var ta = $(this.textarea);
			
			var sel = ta.getSelection();
			var text = ta.val();
			if(sel.start == sel.end)
			{
				var before = text.substring(0, sel.start);
				var after = text.substring(sel.start, text.length);
				
				var newtext = before + starttag + endtag + after;
			}
			else
			{
				var before = text.substring(0, sel.start);
				var after = text.substring(sel.end, text.length);
				var selection = text.substring(sel.start, sel.start + sel.length);
				var newtext = before + starttag + selection + endtag + after;
			}

			ta.val(newtext);
			$(this.textarea).keyup();
		},
		
		
		
		
		
		
		
	}
	
	return new_elem;
}