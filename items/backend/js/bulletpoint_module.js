

function new_bulletpoint_module(id)
{
	var new_elem = 
	{
		id: id,	
		ordering: 0,
		db_id: null,
		selector: null,
		textarea: null,
		preview: null,
		type: 'bulletpoint',
		text: '',
		state: null,
		
		margin_top: 0,
		margin_bottom: 0,
		font_color: 1,
		font_size: 15,
		text_align: 0,
		
		// MODULE BASIC
		init: function(parent, drop_top)
		{
			var html = '<div top_pos="'+drop_top+'" left_pos="0" class="module module_bulletpoint" module_id="' + this.id + '" style="top:'+drop_top+'px"><div class="module_text_clear" width=500 height=50></div><textarea spellcheck="false" class="module_text_html" width=500 height=50></textarea></div>';
			parent.append(html);
			this.selector = '.module[module_id="' + this.id + '"]';
			this.textarea = '.module[module_id="' + this.id + '"] .module_text_html';
			this.preview = '.module[module_id="' + this.id + '"] .module_text_clear';
			this.state = 'preview';
			this.bindListeners();
		},
		
		bindListeners: function()
		{
			$(this.textarea).keyup(function(e)
			{
				active_module.text = $(this).val();
				var mod_id = $(e.target).parent().attr('module_id');
				$(modules[mod_id].selector).height($(e.target).height());
				$(e.target).height(0);
			    var scrollval = $(e.target)[0].scrollHeight;
			    $(e.target).height(scrollval);
			    $(modules[mod_id].selector).height('auto');
			    if (parseInt(e.target.style.height) > $(window).height() - 30) 
			    {
			    	$(document).scrollTop(parseInt(e.target.style.height));
			    }
			    
			    $(active_module.preview).empty();
			    $(active_module.preview).html(nl2br($(active_module.textarea).val()));
			});
			
			$(this.selector).draggable(
			{
				
				grid: [380,10],
				containment: '.drag_contain',
				stop:function(event,ui) {	       
			        var pos = ui.helper.position();
			        $(this).attr('top_pos', pos.top);
			        $(this).attr('left_pos', pos.left);
			       
			    }
				
			});
			
			
			
			$(this.textarea).focus(function()
			{
				modules[$(this).parent().attr('module_id')].setActive();
			});
			
			$(this.preview).click(function()
			{
				modules[$(this).parent().attr('module_id')].setActive();
			});
		},
		
		unbindListeners: function()
		{
			$(this.textarea).unbind('keyup');
			$(this.textarea).unbind('focus');
		},
		
		showProperty: function(parent)
		{
			parent.fadeIn(0);
			parent.attr('module_id', this.id);
			parent.empty();
			html = '<table>';
			html += '<tr> <td class="module_properties_label"><label>Text template</label></td> <td class="module_properties_template"><select  method="setTemplate">';
			for(var i = 0 ; i < text_templates.length ; i++)
			{
				html += '<option value="' + text_templates[i].id + '"';			
				html += '>' + text_templates[i].name + '</option>';
			}
			html += '</select></td></tr>';
			html += '<tr> <td class="module_properties_label"><label>Margin-Top</label></td> <td class="module_properties_input"><input type="text" method="setMarginTop" value="' + this.margin_top + '"></td> </tr>';
			
			html += '<tr> <td class="module_properties_label"><label>Margin-Bottom</label></td> <td class="module_properties_input"><input type="text" method="setMarginBottom" value="' + this.margin_bottom + '"></td> </tr>';
			
			html += '<tr> <td class="module_properties_label"><label>Font color</label></td> <td class="module_properties_color"><select method="setFontColor">';
			for(var i = 0 ; i < colors.length ; i++)
			{
				html += '<option value=' + i + ' hexcode="' + colors[i].hexcode + '"';
				if(colors[i].id == this.font_color)
					html += 'SELECTED';
				html += '>' + colors[i].name + '</option>';
			}
			html += '</select></td></tr>';
			
			html += '<tr> <td class="module_properties_label"><label>Font size</label></td> <td class="module_properties_input"><input type="text" method="setFontSize" value="' + this.font_size + '"></td> </tr>';
			
			html += '</table>';
			
			parent.html(html);
			
			togglePropertyListeners(true);
		},
		
		activate: function(focus_on_input)
		{
			if(this.state == 'editor')
				$(this.textarea).focus();
			else
				this.setActive();
		},
		
		setActive: function()
		{
			if(active_module != null)
				active_module.blur(this.id);
			
			active_module = modules[this.id];
			
			
			this.showProperty(module_properties);	
			this.showOptions();
			this.changeState('editor');
		},
		
		blur: function(new_active)
		{
			if(new_active != this.id)
			{
				this.changeState('preview');
				this.hideOptions();
				if(this.text.length == 0)
				{
					$(this.selector).find('.module_remove').click();
				}
			}

		},
		
		showOptions: function()
		{
			if($(this.selector).find('.module_options').length == 0)
			{
				var html = '<div class="module_options">';
				html += '<div class="module_option module_html"></div>';
				html += '<div class="module_option module_preview"></div>';
				html += module_move_option();
				html += module_remove_option();
				
				html += '</div>';
				$(this.selector).append(html);
				toggleOptionListeners(true);
				
				$(this.selector).find('.module_preview').click(function()
				{
					active_module.changeState('preview');
				});
				
				$(this.selector).find('.module_html').click(function()
				{
					active_module.changeState('html');
				});
			}
		},
		
		hideOptions: function()
		{
			$(this.selector).find('.module_options').remove();
		},
		
		getSaveData: function()
		{
			var ret = 
			{
				'text': this.text,
				'margin_top': this.margin_top,
				'margin_bottom': this.margin_bottom,
				'id': this.db_id,
				'type': this.type,
				'ordering': this.ordering,
				'parent_id': parent_id,
				'parent_type': parent_type,
				'font_color': this.font_color,
				'font_size': this.font_size,
				'top_pos': $(this.selector).attr('top_pos'),
				'left_pos': $(this.selector).attr('left_pos'),
			};
			
			return ret;
		},
	
		load: function(properties)
		{
			this.setText(properties.content);
			this.setMarginBottom(properties.margin_bottom);
			this.setMarginTop(properties.margin_top);
			this.setFontColor(properties.font_color);
			this.setFontSize(properties.font_size);
			this.setTop(properties.top_pos);
			this.setLeft(properties.left_pos);
		},		
		
		
		// PROPERTY SETTERS/GETTERS
		setMarginTop: function(margin_top)
		{
			this.margin_top = parseInt(margin_top);
			$(this.selector).css({'margin-top': this.margin_top});
		},
		
		setMarginBottom: function(margin_bottom)
		{
			this.margin_bottom = parseInt(margin_bottom);
			$(this.selector).css({'margin-bottom': this.margin_bottom});
		},
		
		
		setTop: function(topP)
		{
			
			this.top_pos = topP;
			$(this.selector).css({ top: topP+'px'});
			$(this.selector).attr('top_pos', topP);
		},
		
		setLeft: function(leftP)
		{
			
			
			$(this.selector).css({ left: leftP+'px'});
			$(this.selector).attr('left_pos', leftP);
		},
		
		//MODULE SPECIFIC
		setText: function(content)
		{
			$(this.textarea).val(content);
			$(this.textarea).keyup();
		},
		
		setFontColor: function(color_id)
		{
			this.font_color = color_id;
			for(var i = 0 ; i < colors.length ; i++)
			{
				if(colors[i].id == color_id)
				{
					$(this.textarea).css({'color': colors[i].hexcode});
					$(this.preview).css({'color': colors[i].hexcode});
				}
					
			}
			
			this.activate();
		},
		
		setFontSize: function(font_size)
		{
			this.font_size = font_size;
			$(this.textarea).css({'font-size': font_size + 'px'});
			$(this.preview).css({'font-size': font_size + 'px'});
			$(this.textarea).keyup();
		},
		
		setTemplate: function(margin_bot,margin_top, color, size)
		{
			this.setMarginBottom(margin_bot);
			this.setMarginTop(margin_top);
			this.setFontColor(color);
			this.setFontSize(size);
		},
		
		changeState: function(new_state)
		{
			if(new_state == 'preview')
			{
				$(this.textarea).val('<ul style="list-style-position:outside;">' + this.text + '</ul>');
				$(this.textarea).keyup();
				$(this.selector).find('.module_preview').fadeOut(0);
				$(this.selector).find('.module_html').fadeIn(0);
				$(this.preview).fadeIn(0);
				$(this.textarea).fadeOut(0);
				$(this.selector).css({'padding-left': 15, 'padding-right': 15});
				this.hideStyling();
			}
			else
			{
				var text = this.text.replace('<ul style="list-style-position:outside;">', '');
				text = text.replace('</ul>', '');
				this.setText(text);
				$(this.selector).find('.module_html').fadeOut(0);
				$(this.selector).find('.module_preview').fadeIn(0);
				$(this.textarea).fadeIn(0);
				$(this.preview).fadeOut(0);
				$(this.textarea).keyup();
				$(this.selector).css({'padding-left': 14, 'padding-right': 14});
				this.showStyling();
			}
			
			this.state = new_state;
		},
		
		showStyling: function()
		{
			var html = "";
			html += '<div class="module_text_styling">';
			html += '<div class="styling_option styling_bold"></div>';
			html += '<div class="styling_option styling_italic"></div>';
			html += '<div class="styling_option styling_underline"></div>';
			html += '<div class="styling_option styling_sup"></div>';
			html += '<div class="styling_option styling_sub"></div>';
			html += '<div class="styling_option styling_link"></div>';
			html += '<div class="styling_option styling_bulletpoint"></div>';
			html += '</div>';
			$(this.selector).append(html);
			
			$(this.selector).find('.styling_bold').click(function()
			{
				active_module.applyTag('<b>', '</b>');
			});
			
			$(this.selector).find('.styling_italic').click(function()
			{
				active_module.applyTag('<i>', '</i>');
			});

			$(this.selector).find('.styling_underline').click(function()
			{
				active_module.applyTag('<u>', '</u>');
			});
			
			$(this.selector).find('.styling_sub').click(function()
			{
				active_module.applyTag('<sub>', '</sub>');
			});
	
			$(this.selector).find('.styling_sup').click(function()
			{
				active_module.applyTag('<sup>', '</sup>');
			});
			
			$(this.selector).find('.styling_link').click(function()
			{
				active_module.showLinkWindow();
			});
			
			$(this.selector).find('.styling_bulletpoint').click(function()
			{
				active_module.applyTag('<li>', '</li>');
			});
		},
		
		hideStyling: function()
		{
			$(this.selector).find('.module_text_styling').remove();
		},
		
		applyTag: function(starttag, endtag)
		{
			var ta = $(this.textarea);
			
			var sel = ta.getSelection();
			var text = ta.val();
			if(sel.start == sel.end)
			{
				var before = text.substring(0, sel.start);
				var after = text.substring(sel.start, text.length);
				
				var newtext = before + starttag + endtag + after;
			}
			else
			{
				var before = text.substring(0, sel.start);
				var after = text.substring(sel.end, text.length);
				var selection = text.substring(sel.start, sel.start + sel.length);
				var newtext = before + starttag + selection + endtag + after;
			}

			ta.val(newtext);
			$(this.textarea).keyup();
		},
		
		showLinkWindow: function()
		{
			var html = "";
			html += '<div class="text_edit_popup">';
			
			html += '<div class="text_edit_popup_headline">Add link</div>';
			
			html += '<table class="text_edit_popup_content">';
			html += '<tr>  <td class="module_properties_label">  <label>Target</label>  </td>  <td class="module_properties_target">  <select class="link_target">';
			for(var i = 0 ; i < targets.length ; i++)
			{
				html += '<option value="' + targets[i].id + '"';			
				html += '>' + targets[i].name + '</option>';
			}
			html += '</select>  </td>  </tr>';
			html += '<tr>  <td class="module_properties_label">  <label>Subsite</label>  </td>  <td class="module_properties_subsite">  <select class="link_subsite">';
			for(var i = 0 ; i < subsites.length ; i++)
			{
				html += '<option value="' + subsites[i].id + '"';
				html += '>' + subsites[i].name + '</option>';
			}
			html += '</select>  </td>  </tr>';
			html += '<tr>  <td class="module_properties_label">  <label>URL</label>  </td>  <td class="module_properties_input">  <input type="text" class="link_url" value=""></td>  </tr>';
			html += '</table>';
			
			html += '<div class="text_edit_popup_buttons">';
			html += '<div class="text_edit_popup_button link_cancel">Cancel</div>';
			html += '<div class="text_edit_popup_button link_add">Add</div>';
			html += '</div>';
			
			html += '</div>';
			$("body").append(html);
			
			$('.link_add').click(function()
			{
				var starttag = '';
				var endtag = '</a>';
				switch($('.link_target').val())
				{
					case '0':
						var target = '_self';
						break;
					case '1':
						var target = '_blank';
						break;
				}
				
				if($('.link_subsite').val() == '0')
					var href = $('.link_url').val();
				else
					var href = rootUrl + "subsite/" + $('.link_subsite').val();
					
				starttag = '<a href="' + href + '" target="' + target + '">';
				
				active_module.applyTag(starttag, endtag);
				$('.text_edit_popup').remove();
			});
			
			$('.link_cancel').click(function()
			{
				$('.text_edit_popup').remove();
			});
			
		},
		
	}
	
	return new_elem;
}