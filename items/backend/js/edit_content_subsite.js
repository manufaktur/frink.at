var modules = new Array();
var id_increment = 0;
var module_properties = null;
var active_module = null;
var save_ordering;

$(document).ready(function()
{	
	module_properties = $('#module_properties');
	createExistingModules();
	igniteDragDrop();
	
	addSaveListener();
	addScrollListeners();
	//initSubsite();
	
	clickListeners();
});


function initSubsite()
{
	if(!$('#article_content').find('.module_text'))
	{
		new_element('text');
	}
	if(!$('#article_content').find('.module_image'))
	{
		new_element('image');
	}
	if(!$('#article_content').find('.module_link'))
	{
		new_element('link');
	}
}

function clickListeners()
{
	$('#module_text').click(function(){
		
		if(!$('#article_content').find('.module_text').length)
		{
			new_element('text');
		}
		else
		{
			modules[$('#article_content').find('.module_text').attr('module_id')].setActive();
		}
	});
	
	$('#module_image').click(function(){
		
		if(!$('#article_content').find('.module_image').length)
		{
			new_element('image');
		}
		else
		{
			modules[$('#article_content').find('.module_image').attr('module_id')].setActive();
		}
	});
	
	$('#module_link').click(function(){
		
		if(!$('#article_content').find('.module_link').length)
		{
			new_element('link');
		}
		else
		{
			modules[$('#article_content').find('.module_link').attr('module_id')].setActive();
		}
		
	});
	
}



function addScrollListeners()
{
	$('#content').scroll(function() 
	{
		if($('#content').scrollTop() >= 110)
		{
			$('#content_data').css({'position': 'fixed', 'left': 271, 'top': 50, 'float': 'none'});
			$('#content_container').css({'margin-left': 322});
		}
		else
		{
			$('#content_data').css({'position': 'relative', 'left': 0, 'top': 0, 'float': 'left'});
			$('#content_container').css({'margin-left': 50});
		}
		
	});
}

function createExistingModules()
{
	for(var i = 0 ; i < existing_modules.length ; i++)
	{
		new_element(existing_modules[i].type);
		active_module.load(existing_modules[i].properties);
	}
}

function igniteDragDrop()
{
/*	$('.module_item').draggable(
	{
		helper: 'clone'
	});*/
	$( "#article_content" ).droppable(
	{
		drop: function( event, ui ) 
		{
			if(ui.draggable.hasClass('droppable'))
				new_element(ui.draggable.attr('type'));
	    }
    });
	
	$( "#article_content" ).sortable(
	{
		connectWith: '#article_content',
		handle: '.module_anchor',
		stop: function(event, ui)
		{
			orderModules();
		}
	});
}

function orderModules()
{
	var i = 0;
	$('.module').each(function()
	{
		modules[$(this).attr('module_id')].ordering = i++;
	});	
}


function addSaveListener()
{
	$('.edit_content_save').click(function()
	{
		var elements = new Array();
		save_ordering = 0;
		for(var i = 0 ; i < modules.length ; i++)
		{
			if(modules[i] !== undefined)
			{
				elements.push(modules[i].getSaveData());
			}
		}

		$.ajax(
		{
	        url: rootUrl + 'backend/save_content_modules/' + parent_type + '/' + parent_id,
	        data: JSON.stringify(elements),
	        contentType: "application/json; charset=utf-8",
	        type: "POST",
	        dataType: "json",
	        success: function(data) 
	        {
				if(data.success)
				{
					alert(data.message);
				}
				else
				{
					alert(data.message);
				}
	        }
	    });
		
	});
}


function nl2br(str, is_xhtml) 
{
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function new_element(module_type)
{
	var new_id = id_increment++;
	
	switch(module_type)
	{
		case 'text':
			var new_module = new_text_module(new_id);
			new_module.init($("#article_content"));
			break;
		
		case 'image':
			var new_module = new_image_module(new_id);
			new_module.init($("#article_content"));
			break;
			
		case 'link':
			var new_module = new_link_module(new_id);
			new_module.init($("#article_content"));
			break;		
	}
	modules[new_id] = new_module;
	modules[new_id].activate();
	orderModules();
}


function togglePropertyListeners(toggle)
{
	if(toggle)
	{
		$('.module_properties_input input').keyup(function(e)
		{
			var mod = modules[module_properties.attr('module_id')];
			mod[$(this).attr('method')]($(this).val());
		});
		
		$('.module_properties_button').click(function()
		{
			var mod = modules[module_properties.attr('module_id')];
			mod[$(this).attr('method')]($(this).val());
		});
		
		$('.module_properties_select select').change(function(e)
		{
			var mod = modules[module_properties.attr('module_id')];
			mod[$(this).attr('method')]($(this).val());
		});
		
		$('.module_properties_color select').change(function(e)
		{
			var mod = modules[module_properties.attr('module_id')];
			mod[$(this).attr('method')](colors[$(this).val()].id);
		});
		
		$('.module_properties_template select').change(function(e)
		{
			var mod = modules[module_properties.attr('module_id')];
			mod[$(this).attr('method')](text_templates[$(this).val()].margin_bottom, text_templates[$(this).val()].margin_top, text_templates[$(this).val()].font_color, text_templates[$(this).val()].font_size);
		});
	}
	else
	{
		$('module_properties_input input').unbind('keyup');
	}
}

function toggleOptionListeners(toggle)
{
	
	if(toggle)
	{
	
		$('.module_remove').click(function(e)
		{
			console.log("2");
			var id = $(this).parent().parent().attr('module_id');
			
			modules[id] = undefined;
			$(this).parent().parent().remove();
			
			orderModules();
			
			e.preventDefault();
		});
	}
	else
	{
		$('.module_remove').unbind('click');
	}
}

function module_move_option()
{
	return '<div class="module_option module_anchor"></div>';
}

function module_remove_option()
{
	return '<div class="module_option module_remove"></div>';
}