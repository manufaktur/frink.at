$(document).ready(function()
{	
	resize();
	addGlobalListeners();

	switch(active_site)
	{
		case 'backgrounds':
			igniteBackgroundSortable();
			toggleBackgroundOrderingListeners(true);
			break;
			
		case 'frontpage_articles':
			igniteFrontpageArticlesSortable();
			toggleFrontpageArticlesListeners(true);
			break;
			
		case '':
		default:
			break;
	}
	
});

function addGlobalListeners()
{
	$(window).resize(function()
	{
		resize();
	});
}

function resize()
{
	var wWidth = $(window).width();
	var wHeight = $(window).height();
	
	$('#menu').css({'width' : wWidth});
	$('#sidebar').css({'height': wHeight - $('#menu').height()});
	$('#content').css({'height': wHeight - $('#menu').height() -40, 'width': wWidth - $('#sidebar').width() -40});
}


/****************************************************************************************************************************************************************************
	BACKGROUND ORDERING
******************************************************************************************************************************************************************************/
function igniteBackgroundSortable()
{
	$('#backgrounds_active ul, #backgrounds_available ul').sortable(
	{
		connectWith: '.backgrounds_sortable',
		receive: function(event, ui)
		{
			if(ui.sender.hasClass('backgrounds_active_container'))
			{
				ui.item.unbind('mouseenter mouseleave');
				ui.item.find('.background_remove').fadeOut(0);
			}
			else
			{
				toggleBackgroundOrderingListeners(false);
				toggleBackgroundOrderingListeners(true);
			}
			checkAvailableBackgrounds();

		},
	});
	$('#backgrounds_active ul, #backgrounds_available ul').disableSelection();
	checkAvailableBackgrounds();
}

function checkAvailableBackgrounds()
{
	if($('.backgrounds_available_container .backgrounds').length > 0)
	{
		$('.no_articles_available').fadeOut(0);
		$('#backgrounds_available ul').sortable( "option", "disabled", false );
	}
	else
	{
		$('.no_articles_available').fadeIn(0);
		$('#backgrounds_available ul').sortable( "option", "disabled", true );
	}
}

function toggleBackgroundOrderingListeners(toggle)
{
	if(toggle)
	{
		$('#backgrounds_active li').hover(function()
		{
			$(this).find('.background_remove').fadeIn(50);
		}, 
		function()
		{
			$(this).find('.background_remove').fadeOut(50);
		});
		
		$('.background_remove').click(function()
		{
			resetBackground($(this).parent());
		});	
		
		$('.backgrounds_save').click(function()
		{
			saveBackgrounds();
		});
	}
	else
	{
		$('#backgrounds_active li').unbind('mouseenter mouseleave');
		$('.background_remove').unbind('click');
		$('.backgrounds_save').unbind('click');
	}
}

function resetBackground(background)
{
	background.unbind('mouseenter mouseleave');
	background.find('.background_remove').fadeOut(0);
	background.appendTo('#backgrounds_available ul');
	checkAvailableBackgrounds();
}

function saveBackgrounds()
{
	var i = 0;
	var backgrounds = [];
	$('#backgrounds_active li').each(function()
	{
		backgrounds.push
		( 
			{
				'background_id': $(this).attr('background_id'),
				'ordering': i
			}
		);
		i++;
	});
	
	$.ajax({
        url: rootUrl + 'backend/save_background_ordering',
        data: JSON.stringify(backgrounds),
        contentType: "application/json; charset=utf-8",
        type:"POST",
        dataType: "json",
        cache: false,
        success: function(data) 
        {
        	if(data.success)
				alert(data.message);
			else
				alert(data.message);
        }
    });			
}



/****************************************************************************************************************************************************************************
	FRONTPAGE ARTICLES
******************************************************************************************************************************************************************************/
function igniteFrontpageArticlesSortable()
{
	$('#articles_active_container ul, #articles_available ul').sortable(
	{
		connectWith: '.articles_sortable',
		remove: function(event, ui)
		{
			checkAvailableArticles();
		}
	});
	$('#articles_active_container ul, #articles_available ul').disableSelection();
	checkAvailableArticles();
}

function checkAvailableArticles()
{
	if($('#articles_available .article_available').length > 0)
	{
		$('.no_articles_available').fadeOut(0);
		$('#articles_available ul').sortable( "option", "disabled", false );
	}
	else
	{
		$('.no_articles_available').fadeIn(0);
		$('#articles_available ul').sortable( "option", "disabled", true );
	}	
}

function toggleFrontpageArticlesListeners(toggle)
{
	if(toggle)
	{
		$('.article_total').hover(function()
		{
			if($(this).parent().parent().attr('id') != 'articles_available')
				$(this).find('.article_remove').fadeIn(0);
		}, 
		function()
		{
			if($(this).parent().parent().attr('id') != 'articles_available')
				$(this).find('.article_remove').fadeOut(0);
		});
		
		$('.article_remove').click(function()
		{
			resetArticle($(this).parent());
		});	
		
		$('.frontpage_articles_save').click(function()
		{
			saveFrontpageArticles();
		});
	}
	else
	{
		$('.article_total').unbind('mouseenter mouseleave');
		$('.article_remove').unbind('click');
		$('.frontpage_articles_save').unbind('click');
	}

}

function resetArticle(article)
{
	article.find('.article_remove').fadeOut(0);
	article.find('.article_available').fadeOut(0);
	article.appendTo('.articles_sortable');
	article.remove();
	checkAvailableArticles();
}


function saveFrontpageArticles()
{
	var i = 0;
	var articles = [];
	$('#articles_active_container li').each(function()
	{
		articles.push
		( 
			{
				'article_id': $(this).attr('article_id'),
				'ordering': i
			}
		);
		i++;
	});
	
	$.ajax({
        url: rootUrl + 'backend/save_frontpage_articles',
        data: JSON.stringify(articles),
        contentType: "application/json; charset=utf-8",
        type:"POST",
        dataType: "json",
        cache: false,
        success: function(data) 
        {
        	if(data.success)
				alert(data.message);
			else
				alert(data.message);
        }
    });		
}


function blur_image(filename, uploadpath, element, imagedata)
{
	setTimeout(function()
	{
		$('#' + element).parent().find('.bc_col_image_preview').attr('src', rootUrl + 'items/backend/img/blur_preview.png');
	}, 200);
			
	$.ajax(
	{
        url: 'http://54.217.247.147/st_anna_blur.php',
        data: { filename: filename, data: imagedata },
		method: 'POST',
		dataType: 'json',
        success: function(data) 
        {
        	
        	$.ajax(
			{
		        url: rootUrl + 'backend/save_blurred_image',
		        data: { fname: data , old_filename: filename, uploadpath: uploadpath},
				method: 'POST',
				dataType: 'json',
		        success: function(data) 
		        {
		        	var d = new Date();
		        	$('#' + element).parent().find('.bc_col_image_preview').attr('src', rootUrl + '/' + uploadpath + '/' + filename + '?' + d.getTime());
		        }
		    });	
        }
    });	
}
