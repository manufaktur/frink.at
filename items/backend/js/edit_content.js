var modules = new Array();
var id_increment = 0;
var module_properties = null;
var active_module = null;
var save_ordering;
var left_container = '#article_content';
var right_container = '#article_content_right';


$(document).ready(function()
{	
	module_properties = $('#module_properties');
	createExistingModules();
	igniteDragDrop();
	
	addSaveListener();
	addScrollListeners();
});


function addScrollListeners()
{
	$('#content').scroll(function() 
	{
		if($('#content').scrollTop() >= 110)
		{
			$('#content_data').css({'position': 'fixed', 'left': 271, 'top': 50, 'float': 'none'});
			$('#content_container').css({'margin-left': 322});
		}
		else
		{
			$('#content_data').css({'position': 'relative', 'left': 0, 'top': 0, 'float': 'left'});
			$('#content_container').css({'margin-left': 50});
		}
		
	});
}

function createExistingModules()
{
	for(var i = 0 ; i < existing_modules.length ; i++)
	{
		new_element(existing_modules[i].type, $( "#article_content" ));
		active_module.load(existing_modules[i].properties);
	}
}

function igniteDragDrop()
{
	$('.module_item').draggable(
	{
		helper: 'clone'
	});
	
	$("#article_content").droppable(
	{
		drop: function( event, ui ) 
		{
			var scrollTop = $('#content').scrollTop();
			var topPos = ui.position.top+scrollTop;

			if(ui.draggable.hasClass('droppable'))
				new_element(ui.draggable.attr('type'), left_container, topPos);
	    }
    });
	
	
	$( "#article_content_right" ).droppable(
	{
		
		drop: function( event, ui ) 
		{
			
			var topPos = ui.position.top+scrollTop;
        		
			if(ui.draggable.hasClass('droppable'))
				new_element(ui.draggable.attr('type'), right_container, topPos);
	    }
    });
    
    
  /*  $('.module').draggable(
	{
		axis: 'y',
		grid: [10,10],
		containment: '.drag_contain',
		stop:function(event,ui) {	       
	        var pos = ui.helper.position();
	        $(this).attr('topPos', pos.top);
	    }
		
	});
	*/
/*	$( "#article_content" ).sortable(
	{
		connectWith: '#article_content_right',
		handle: '.module_anchor',
		stop: function(event, ui)
		{
			orderModules();
		}
	});
	
	$( "#article_content_right" ).sortable(
	{
		connectWith: '#article_content',
		handle: '.module_anchor',
		stop: function(event, ui)
		{
			orderModules();
		}
	}); */
	
	
}

function orderModules()
{
	var i = 0;
	$('.module').each(function()
	{
		modules[$(this).attr('module_id')].ordering = i++;
	});	
}


function addSaveListener()
{
	$('.edit_content_save').click(function()
	{
		var elements = new Array();
		save_ordering = 0;
		for(var i = 0 ; i < modules.length ; i++)
		{
			if(modules[i] !== undefined)
			{
				elements.push(modules[i].getSaveData());
			}
		}

		$.ajax(
		{
	        url: rootUrl + 'backend/save_content_modules/' + parent_type + '/' + parent_id,
	        data: JSON.stringify(elements),
	        contentType: "application/json; charset=utf-8",
	        type: "POST",
	        dataType: "json",
	        success: function(data) 
	        {
				if(data.success)
				{
					alert(data.message);
				}
				else
				{
					alert(data.message);
				}
	        }
	    });
		
	});
}


function nl2br(str, is_xhtml) 
{
    var breakTag = (is_xhtml || typeof is_xhtml === 'undefined') ? '<br />' : '<br>';
    return (str + '').replace(/([^>\r\n]?)(\r\n|\n\r|\r|\n)/g, '$1' + breakTag + '$2');
}

function new_element(module_type, container, topPos)
{
	var new_id = id_increment++;
	
	switch(module_type)
	{
		case 'text':
			var new_module = new_text_module(new_id);
			new_module.init($(container), topPos);
			break;
		
		case 'image':
			var new_module = new_image_module(new_id);
			new_module.init($(container), topPos);
			break;
			
		case 'gallery':
			var new_module = new_gallery_module(new_id);
			new_module.init($("#article_content"), topPos);
			break;
			
		case 'download':
			var new_module = new_download_module(new_id);
			new_module.init($("#article_content"), topPos);
			break;
			
		case 'bulletpoint':
			var new_module = new_bulletpoint_module(new_id);
			new_module.init($("#article_content"), topPos);
			break;
			
		case 'button':
			var new_module = new_button_module(new_id);
			new_module.init($(container), topPos);
			break;	
			
		case 'impact':
			var new_module = new_impact_module(new_id);
			new_module.init($(container), topPos);
			break;
				
		case 'video':
			var new_module = new_video_module(new_id);
			new_module.init($(container), topPos);
			break;
		case 'text_image':
			var new_module = new_text_image_module(new_id);
			new_module.init($(container), topPos);
			break;	
	}

	modules[new_id] = new_module;
	
	modules[new_id].activate();
	orderModules();
}


function togglePropertyListeners(toggle)
{
	if(toggle)
	{
		$('.module_properties_input input').keyup(function(e)
		{
			
			var mod = modules[module_properties.attr('module_id')];
			
			mod[$(this).attr('method')]($(this).val());
		});
		
		$('.module_properties_input textarea').keyup(function(e)
		{
			
			var mod = modules[module_properties.attr('module_id')];
			
			mod[$(this).attr('method')]($(this).val());
		});
		
		$('.module_properties_button').click(function()
		{
			var mod = modules[module_properties.attr('module_id')];
			mod[$(this).attr('method')]($(this).val());
		});
		
		$('.module_properties_select select').change(function(e)
		{
			
			var mod = modules[module_properties.attr('module_id')];
			mod[$(this).attr('method')]($(this).val());
			
		});
		
		$('.module_properties_width select').change(function(e)
		{
			
			var mod = modules[module_properties.attr('module_id')];
			mod[$(this).attr('method')]($(this).val());
			
		});
		
		
		$('.module_properties_color select').change(function(e)
		{
			var mod = modules[module_properties.attr('module_id')];
			mod[$(this).attr('method')](colors[$(this).val()].id);
		});
		
		$('.module_properties_template select').change(function(e)
		{
			var mod = modules[module_properties.attr('module_id')];
			mod[$(this).attr('method')](text_templates[$(this).val()].margin_bottom, text_templates[$(this).val()].margin_top, text_templates[$(this).val()].font_color, text_templates[$(this).val()].font_size);
		});
	}
	else
	{
		$('module_properties_input input').unbind('keyup');
	}
}

function toggleOptionListeners(toggle)
{
	if(toggle)
	{
		$('.module_remove').click(function(e)
		{
			var id = $(this).parent().parent().attr('module_id');
			
			modules[id] = undefined;
			$(this).parent().parent().remove();
			
			orderModules();
			
			e.preventDefault();
		});
	}
	else
	{
		$('.module_remove').unbind('click');
	}
}

function module_move_option()
{
	return '<div class="module_option module_anchor"></div>';
}

function module_remove_option()
{
	return '<div class="module_option module_remove"></div>';
}