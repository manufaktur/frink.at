

function new_text_module(id)
{
	var new_elem = 
	{
		id: id,	
		ordering: 0,
		db_id: null,
		selector: null,
		textarea: null,
		preview: null,
		type: 'text',
		text: '',
		state: null,
		align: 0,
		
		margin_top: 0,
		margin_bottom: 0,
		font_color: 1,
		font_size: 15,
		text_align: 0,
		
		right_side_img: '',
		right_side_img_top: 0,
		right_side_text: '',
		uploadpath: 'items/general/uploads/sidebar_image',
		
		// MODULE BASIC
		init: function(parent)
		{
						
			var html = '<div class="module module_text" module_id="' + this.id + '"><div class="module_text_clear" width=305 height=50></div><textarea spellcheck="false" class="module_text_html" width=500 height=50 placeholder="Insert text here!"></textarea></div>';
			parent.append(html);
			this.selector = '.module[module_id="' + this.id + '"]';
			this.textarea = '.module[module_id="' + this.id + '"] .module_text_html';
			this.preview = '.module[module_id="' + this.id + '"] .module_text_clear';
			this.state = 'preview';
			this.bindListeners();
		},
		
		bindListeners: function()
		{
			$(this.textarea).keyup(function(e)
			{
				active_module.text = $(this).val();
				var mod_id = $(e.target).parent().attr('module_id');
				$(modules[mod_id].selector).height($(e.target).height());
				$(e.target).height(0);
			    var scrollval = $(e.target)[0].scrollHeight;
			    $(e.target).height(scrollval);
			    $(modules[mod_id].selector).height('auto');
			    if (parseInt(e.target.style.height) > $(window).height() - 30) 
			    {
			    	$(document).scrollTop(parseInt(e.target.style.height));
			    }
			    
			    $(active_module.preview).empty();
			    $(active_module.preview).html(nl2br($(active_module.textarea).val()));
			});
			
			$(this.textarea).focus(function()
			{
				modules[$(this).parent().attr('module_id')].setActive();
			});
			
			$(this.preview).click(function()
			{
				modules[$(this).parent().attr('module_id')].setActive();
			});
		},
		
		unbindListeners: function()
		{
			$(this.textarea).unbind('keyup');
			$(this.textarea).unbind('focus');
		},
		
		showProperty: function(parent)
		{
			parent.fadeIn(0);
			parent.attr('module_id', this.id);
			parent.empty();
			html = '<table>';
			html += '<tr> <td class="module_properties_label"><label>Text template</label></td> <td class="module_properties_template"><select  method="setTemplate">';
			for(var i = 0 ; i < text_templates.length ; i++)
			{
				html += '<option value="' + text_templates[i].id + '"';			
				html += '>' + text_templates[i].name + '</option>';
			}
			html += '</select></td></tr>';
			html += '<tr> <td class="module_properties_label"><label>Margin-Top</label></td> <td class="module_properties_input"><input type="text" method="setMarginTop" value="' + this.margin_top + '"></td> </tr>';
			
			html += '<tr> <td class="module_properties_label"><label>Margin-Bottom</label></td> <td class="module_properties_input"><input type="text" method="setMarginBottom" value="' + this.margin_bottom + '"></td> </tr>';
			
			html += '<tr> <td class="module_properties_label"><label>Font size</label></td> <td class="module_properties_input"><input type="text" method="setFontSize" value="' + this.font_size + '"></td> </tr>';
			
			html += '<tr> <td class="module_properties_label"><label>Text align</label></td> <td class="module_properties_select"><select method="setTextAlign">';
			for(var i = 0 ; i < text_align.length ; i++)
			{
				html += '<option value=' + i + ' ';
				if(i == this.align)
					html += 'SELECTED';
				html += '>' + text_align[i] + '</option>';
			}
			html += '</select></td></tr>';
			
	
			
			html += '</table>';
			
			parent.html(html);
			this.uploadListeners();
			togglePropertyListeners(true);
		},
		
		activate: function(focus_on_input)
		{
			if(this.state == 'editor')
				$(this.textarea).focus();
			else
				this.setActive();
		},
		
		setActive: function()
		{
			if(active_module != null)
				active_module.blur(this.id);
			
			active_module = modules[this.id];
			
			
			this.showProperty(module_properties);	
			this.showOptions();
			this.changeState('editor');
		},
		
		blur: function(new_active)
		{
			if(new_active != this.id)
			{
				this.changeState('preview');
				this.hideOptions();
				if(this.text.length == 0)
				{
					$(this.selector).find('.module_remove').click();
				}
			}

		},
		
		showOptions: function()
		{
			if($(this.selector).find('.module_options').length == 0)
			{
				var html = '<div class="module_options">';
				html += '<div class="module_option module_html"></div>';
				html += '<div class="module_option module_preview"></div>';		
				html += module_remove_option();
				
				html += '</div>';
				$(this.selector).append(html);
				toggleOptionListeners(true);
				
				$(this.selector).find('.module_preview').click(function()
				{
					active_module.changeState('preview');
				});
				
				$(this.selector).find('.module_html').click(function()
				{
					active_module.changeState('html');
				});
			}
		},
		
		hideOptions: function()
		{
			$(this.selector).find('.module_options').remove();
		},
		
		getSaveData: function()
		{
			var ret = 
			{
				'text': this.text,
				'margin_top': this.margin_top,
				'margin_bottom': this.margin_bottom,
				'id': this.db_id,
				'type': this.type,
				'ordering': this.ordering,
				'parent_id': parent_id,
				'parent_type': parent_type,
				'font_color': this.font_color,
				'font_size': this.font_size,
				'align': this.align,
				'right_side_img': this.right_side_img,
				'right_side_img_top': parseInt($(this.selector).offset().top - $('#article_content').offset().top -15),
				'right_side_img_text': this.right_side_text,
				'left_pos': 0,
				'top_pos': 20,
			};
			
			return ret;
		},
	
		load: function(properties)
		{
			this.setText(properties.content);
			this.setMarginBottom(properties.margin_bottom);
			this.setMarginTop(properties.margin_top);
			this.setFontColor(properties.font_color);
			this.setFontSize(properties.font_size);
			this.setTextAlign(properties.align);
			this.setSidebarImage(properties.right_side_img);
			this.setSidebarText(properties.right_side_img_text);
		},		
		
		
		// PROPERTY SETTERS/GETTERS
		setMarginTop: function(margin_top)
		{
			this.margin_top = parseInt(margin_top);
			$(this.selector).css({'margin-top': this.margin_top});
		},
		
		setMarginBottom: function(margin_bottom)
		{
			this.margin_bottom = parseInt(margin_bottom);
			$(this.selector).css({'margin-bottom': this.margin_bottom});
		},
		
		//MODULE SPECIFIC
		setText: function(content)
		{
			$(this.textarea).val(content);
			$(this.textarea).keyup();
		},
		
		setFontColor: function(color_id)
		{
			this.font_color = color_id;
			for(var i = 0 ; i < colors.length ; i++)
			{
				if(colors[i].id == color_id)
				{
					$(this.textarea).css({'color': colors[i].hexcode});
					$(this.preview).css({'color': colors[i].hexcode});
				}
					
			}
			
			this.activate();
		},
		
		setTextAlign: function(align)
		{
			this.align = align;
			$(this.textarea).css({'text-align': text_align[this.align]});
			$(this.preview).css({'text-align': text_align[this.align]});
			$(this.textarea).keyup();
		},
		
		setFontSize: function(font_size)
		{
			this.font_size = font_size;
			$(this.textarea).css({'font-size': font_size + 'px'});
			$(this.preview).css({'font-size': font_size + 'px'});
			$(this.textarea).keyup();
		},
		
		setTemplate: function(margin_bot,margin_top, color, size)
		{
			this.setMarginBottom(margin_bot);
			this.setMarginTop(margin_top);
			this.setFontColor(color);
			this.setFontSize(size);
		},
		
		changeState: function(new_state)
		{
			if(new_state == 'preview')
			{
				$(this.selector).find('.module_preview').fadeOut(0);
				$(this.selector).find('.module_html').fadeIn(0);
				$(this.preview).fadeIn(0);
				$(this.textarea).fadeOut(0);
				$(this.selector).css({'padding-left': 15, 'padding-right': 15});
				this.hideStyling();
			}
			else
			{
				$(this.selector).find('.module_html').fadeOut(0);
				$(this.selector).find('.module_preview').fadeIn(0);
				$(this.textarea).fadeIn(0);
				$(this.preview).fadeOut(0);
				$(this.textarea).keyup();
				$(this.selector).css({'padding-left': 14, 'padding-right': 14});
				this.showStyling();
			}
			
			this.state = new_state;
		},
		
		showStyling: function()
		{
			var html = "";
			html += '<div class="module_text_styling">';
			html += '<div class="styling_option styling_bold"></div>';
			html += '<div class="styling_option styling_italic"></div>';
			html += '<div class="styling_option styling_underline"></div>';
			html += '<div class="styling_option styling_sup"></div>';
			html += '<div class="styling_option styling_sub"></div>';
			html += '<div class="styling_option styling_link"></div>';
			html += '</div>';
			$(this.selector).append(html);
			
			$(this.selector).find('.styling_bold').click(function()
			{
				active_module.applyTag('<b>', '</b>');
			});
			
			$(this.selector).find('.styling_italic').click(function()
			{
				active_module.applyTag('<i>', '</i>');
			});

			$(this.selector).find('.styling_underline').click(function()
			{
				active_module.applyTag('<u>', '</u>');
			});
			
			$(this.selector).find('.styling_sub').click(function()
			{
				active_module.applyTag('<sub>', '</sub>');
			});
	
			$(this.selector).find('.styling_sup').click(function()
			{
				active_module.applyTag('<sup>', '</sup>');
			});
			
			$(this.selector).find('.styling_link').click(function()
			{
				active_module.showLinkWindow();
			});
		},
		
		hideStyling: function()
		{
			$(this.selector).find('.module_text_styling').remove();
		},
		
		applyTag: function(starttag, endtag)
		{
			var ta = $(this.textarea);
			
			var sel = ta.getSelection();
			var text = ta.val();
			if(sel.start == sel.end)
			{
				var before = text.substring(0, sel.start);
				var after = text.substring(sel.start, text.length);
				
				var newtext = before + starttag + endtag + after;
			}
			else
			{
				var before = text.substring(0, sel.start);
				var after = text.substring(sel.end, text.length);
				var selection = text.substring(sel.start, sel.start + sel.length);
				var newtext = before + starttag + selection + endtag + after;
			}

			ta.val(newtext);
			$(this.textarea).keyup();
		},
		
		showLinkWindow: function()
		{
			var html = "";
			html += '<div class="text_edit_popup">';
			
			html += '<div class="text_edit_popup_headline">Add link</div>';
			
			html += '<table class="text_edit_popup_content">';
			html += '<tr>  <td class="module_properties_label">  <label>Target</label>  </td>  <td class="module_properties_target">  <select class="link_target">';
			for(var i = 0 ; i < targets.length ; i++)
			{
				html += '<option value="' + targets[i].id + '"';			
				html += '>' + targets[i].name + '</option>';
			}
			html += '</select>  </td>  </tr>';
			html += '<tr>  <td class="module_properties_label">  <label>Subsite</label>  </td>  <td class="module_properties_subsite">  <select class="link_subsite">';
			for(var i = 0 ; i < subsites.length ; i++)
			{
				html += '<option value="' + subsites[i].id + '"';
				html += '>' + subsites[i].name + '</option>';
			}
			html += '</select>  </td>  </tr>';
			html += '<tr>  <td class="module_properties_label">  <label>URL</label>  </td>  <td class="module_properties_input">  <input type="text" class="link_url" value=""></td>  </tr>';
			html += '</table>';
			
			html += '<div class="text_edit_popup_buttons">';
			html += '<div class="text_edit_popup_button link_cancel">Cancel</div>';
			html += '<div class="text_edit_popup_button link_add">Add</div>';
			html += '</div>';
			
			html += '</div>';
			$("body").append(html);
			
			$('.link_add').click(function()
			{
				var starttag = '';
				var endtag = '</a>';
				switch($('.link_target').val())
				{
					case '0':
						var target = '_self';
						break;
					case '1':
						var target = '_blank';
						break;
				}
				
				if($('.link_subsite').val() == '0')
					var href = $('.link_url').val();
				else
					var href = rootUrl + "subsite/" + $('.link_subsite').val();
					
				starttag = '<a href="' + href + '" target="' + target + '">';
				
				active_module.applyTag(starttag, endtag);
				$('.text_edit_popup').remove();
			});
			
			$('.link_cancel').click(function()
			{
				$('.text_edit_popup').remove();
			});
			
		},
		
		upload: function()
		{
			$('.module_properties_button[method="upload"]').parent().find('input').click();
		},
		
		uploadListeners: function()
		{
			$('#module_properties_upload_button').change(function()
			{
				var element = $(this).attr('id');
				var file = document.getElementById(element).files[0];
				var uploadpath = active_module.uploadpath;
				var reader = new FileReader();
				var url;
				reader.readAsDataURL (file);
				reader.onload = function(event)
				{
					var result = event.target.result;
					$.ajax(
					{
						url: rootUrl + 'backend/content_download_fileupload',
						data: { filename: file.name, data: result , uploadpath: uploadpath},
						method: 'POST',
						success: function(data)
						{
							var ret = $.parseJSON(data);
							
							if(ret.success)
							{
								active_module.setSidebarImage(ret.filename);
							}
							else
							{
								alert('Error while uploading');
							}
						}
					});			
				};
			});
		},
		
		setSidebarImage: function(fname)
		{
			this.right_side_img = fname;
			$('.prop_right_image_fname').empty();
			$('.prop_right_image_fname').val(fname);
		},
		
		remove_right_side_image: function()
		{
			this.setSidebarImage('');
			this.setSidebarText('');
		},
		
		setSidebarText: function(text)
		{
			$('.prop_right_image_text').empty();
			$('.prop_right_image_text').val(text);
			this.right_side_text = text;
		}
		
		
	}
	
	return new_elem;
}