

function new_download_module(id)
{
	var new_elem = 
	{
		id: id,	
		ordering: 0,
		
		selector: null,
		description: null,
		
		type: 'download',
		uploadpath: 'items/general/uploads/content_downloads',
		fname: '',
		margin_top: 0,
		margin_bottom: 0,
		description_text: 'Enter your description here!',
		
		
		// MODULE BASIC
		init: function(parent)
		{
			var html = '<div class="module module_image" module_id="' + this.id + '"><div class="module_download_text">' + this.description_text + '</div><div class="module_download_button"></div></div>';
			parent.append(html);
			this.selector = '.module[module_id="' + this.id + '"]';
			this.description = '.module[module_id="' + this.id + '"] .module_download_text';
			this.bindListeners();
		},
		
		bindListeners: function()
		{
			$(this.selector).click(function()
			{
				modules[$(this).attr('module_id')].setActive();
			});
		},
		
		unbindListeners: function()
		{
			$(this.selector).unbind('click');
		},
		
		showProperty: function(parent)
		{
			parent.fadeIn(0);
			parent.attr('module_id', this.id);
			parent.empty();
			html = '<table>';
			html += '<tr> <td class="module_properties_label"><label>Margin-Top</label></td> <td class="module_properties_input"><input type="text" method="setMarginTop" value="' + this.margin_top + '"></td> </tr>';
			html += '<tr> <td class="module_properties_label"><label>Margin-Bottom</label></td> <td class="module_properties_input"><input type="text" method="setMarginBottom" value="' + this.margin_bottom + '"></td> </tr>';
			html += '<tr> <td class="module_properties_label"><label>Text</label></td> <td class="module_properties_input"><input type="text" method="setDescription" value="' + this.description_text + '"></td> </tr>';
			html += '<tr> <td class="module_properties_label"><label>Filename</label></td> <td class="module_properties_input"><input type="text" class="prop_download_fname" DISABLED method="setFilename" value="' + this.fname + '"></td> </tr>';
			html += '<tr> <td colspan="2"> <div class="module_properties_button" method="upload">Upload</div> <input type="file" id="module_properties_upload_button" accept=".pdf"/> </td> </tr>';
			html += '</table>';
			
			parent.html(html);
			this.uploadListeners();
			togglePropertyListeners(true);
		},
		
		activate: function()
		{
			this.setActive();
		},
		
		blur: function(new_active)
		{
			if(new_active != this.id)
			{
				this.hideOptions();
			}
		},

		setActive: function()
		{
			if(active_module != null)
				active_module.blur(this.id);
			
			active_module = modules[this.id];
			this.showProperty(module_properties);
			this.showOptions();
		},
		
		showOptions: function()
		{
			var html = '<div class="module_options">';
			html += module_move_option();
			html += module_remove_option();
			html += '</div>';
			$(this.selector).append(html);
			toggleOptionListeners(true);
		},
		
		hideOptions: function()
		{
			$(this.selector).find('.module_options').remove();
		},
		
		
		// PROPERTY SETTERS/GETTERS
		setMarginTop: function(margin_top)
		{
			this.margin_top = parseInt(margin_top);
			$(this.selector).css({'margin-top': this.margin_top});
		},
		
		setMarginBottom: function(margin_bottom)
		{
			this.margin_bottom = parseInt(margin_bottom);
			$(this.selector).css({'margin-bottom': this.margin_bottom});
		},
		
		setText: function(text)
		{
			this.text = text;
		},
		
		getSaveData: function()
		{
			var ret = 
			{
				'fname': this.fname,
				'margin_top': this.margin_top,
				'margin_bottom': this.margin_bottom,
				'id': this.db_id,
				'type': this.type,
				'ordering': this.ordering,
				'parent_id': parent_id,
				'parent_type': parent_type,
				'text': this.description_text,
			};
			
			return ret;
		},
		
		load: function(properties)
		{		
			this.setDescription(properties.text)
			this.setMarginBottom(properties.margin_bottom);
			this.setMarginTop(properties.margin_top);
			this.setFilename(properties.fname);
		},
		
		
		//MODULE SPECIFIC
		upload: function()
		{
			$('.module_properties_button[method="upload"]').parent().find('input').click();
		},
		
		uploadListeners: function()
		{
			$('#module_properties_upload_button').change(function()
			{
				var element = $(this).attr('id');
				var file = document.getElementById(element).files[0];
				var uploadpath = active_module.uploadpath;
				var reader = new FileReader();
				var url;
				reader.readAsDataURL (file);
				reader.onload = function(event)
				{
					var result = event.target.result;
					$.ajax(
					{
						url: rootUrl + 'backend/content_download_fileupload',
						data: { filename: file.name, data: result , uploadpath: uploadpath},
						method: 'POST',
						success: function(data)
						{
							var ret = $.parseJSON(data);
							
							if(ret.success)
							{
								active_module.setFilename(ret.filename);
							}
							else
							{
								alert('Error while uploading');
							}
						}
					});			
				};
			});
		},
		
		setDescription: function(desc)
		{
			this.description_text = desc;
			$(this.description).empty();
			$(this.description).html(desc);
		},
		
		setFilename: function(fname)
		{
			this.fname = fname;
			$('.prop_download_fname').val(fname);
		},
		
	}
	
	return new_elem;
}