

function new_video_module(id)
{
	var new_elem = 
	{
		id: id,	
		ordering: 0,
		db_id: null,
		selector: null,
		textarea: null,
		preview: null,
		type: 'video',
		text: '',
		state: null,
		align: 0,
		code: '',
		
		// MODULE BASIC
		init: function(parent,drop_top)
		{
			
			var html = '<div top_pos="'+drop_top+'" left_pos="0" module_id=' + this.id + ' class="module module_video has_placeholder" data-text="Insert video here" style="top:'+drop_top+'px" code="" start="0"><div class="module_video_overlay"></div><iframe class="module_video_iframe" src="https://player.vimeo.com/video/"'+this.code+' frameborder="0" allowfullscreen="" wmode="Opaque" style="width: 100%;height: 100%;"></iframe></div>';
			parent.append(html);
			this.selector = '.module[module_id="' + this.id + '"]';
			
			this.bindListeners();

		},
		
		bindListeners: function()
		{
			
			
			$(this.selector).draggable(
			{
				
				grid: [380,10],
				containment: '.drag_contain',
				stop:function(event,ui) {	       
			        var pos = ui.helper.position();
			        $(this).attr('top_pos', pos.top);
			        $(this).attr('left_pos', pos.left);
			       
			    }
				
			});
			
			
			$(this.selector).click(function()
			{
				modules[$(this).attr('module_id')].setActive();
			});
		},
		
		unbindListeners: function()
		{
			$(this.textarea).unbind('keyup');
			$(this.textarea).unbind('focus');
		},
		
		showProperty: function(parent)
		{
			parent.fadeIn(0);
			parent.attr('module_id', this.id);
			parent.empty();
			html = '<table>';
			html += '<tr> <td class="module_properties_label"><label>Vimeo id</label></td> <td class="module_properties_input"><input type="text" id="video_code" method="embedVideo" value="' + this.code + '"></td> </tr>';
			html += '<tr> <td colspan="2"> <div class="module_properties_button" id="save_embed" method="embedVideo">Embed</div></td> </tr>';
			html += '</table>';
			
			parent.html(html);
			togglePropertyListeners(true);
		},
		
		activate: function(focus_on_input)
		{
			if(this.state == 'editor')
				$(this.textarea).focus();
			else
				this.setActive();
		},
		
		setActive: function()
		{
			if(active_module != null)
				active_module.blur(this.id);
			
			active_module = modules[this.id];
			
			
			this.showProperty(module_properties);	
			this.showOptions();
			this.changeState('editor');
		},
		
		blur: function(new_active)
		{
			if(new_active != this.id)
			{
				this.changeState('preview');
				this.hideOptions();
				if(this.text.length == 0)
				{
					$(this.selector).find('.module_remove').click();
				}
			}

		},
		
		showOptions: function()
		{
			if($(this.selector).find('.module_options').length == 0)
			{
				var html = '<div class="module_options">';
				html += module_move_option();
				html += module_remove_option();
				
				html += '</div>';
				$(this.selector).append(html);
				toggleOptionListeners(true);
				
			}
		},
		
		hideOptions: function()
		{
			$(this.selector).find('.module_options').remove();
		},
		
		getSaveData: function()
		{
			var ret = 
			{
				'id': this.db_id,
				'type': this.type,
				'parent_id': parent_id,
				'parent_type': parent_type,
				'code' : this.code,
				'ordering': this.ordering,
				'top_pos': $(this.selector).attr('top_pos'),
				'left_pos': $(this.selector).attr('left_pos'),
			};
			
			return ret;
		},
	
		load: function(properties)
		{
			this.setCode(properties.code);
			this.setTop(properties.top_pos);
			this.setLeft(properties.left_pos);
		},		
		
		
		// PROPERTY SETTERS/GETTERS
		setCode: function(embed_code)
		{
			this.code = embed_code;
			$(this.selector).find('iframe').attr('src', 'https://player.vimeo.com/video/' + embed_code);
		},
		
		
		//MODULE SPECIFIC
		setText: function(content)
		{
			$(this.textarea).val(content);
			$(this.textarea).keyup();
		},
		
		setTop: function(topP)
		{
			
			this.top_pos = topP;
			$(this.selector).css({ top: topP+'px'});
			$(this.selector).attr('top_pos', topP);
		},
		
		setLeft: function(leftP)
		{
			
			
			$(this.selector).css({ left: leftP+'px'});
			$(this.selector).attr('left_pos', leftP);
		},
		
		embedVideo: function()
		{
			
			var embed_code = $('#video_code').val();
			this.code = embed_code;		
				
			$(this.selector).find('iframe').attr('src', 'https://player.vimeo.com/video/' + embed_code);
			
		},
		
		setTextAlign: function(align)
		{
			this.align = align;
			$(this.textarea).css({'text-align': text_align[this.align]});
			$(this.preview).css({'text-align': text_align[this.align]});
			$(this.textarea).keyup();
		},
		
		setFontSize: function(font_size)
		{
			this.font_size = font_size;
			$(this.textarea).css({'font-size': font_size + 'px'});
			$(this.preview).css({'font-size': font_size + 'px'});
			$(this.textarea).keyup();
		},
		
		setTemplate: function(margin_bot,margin_top, color, size)
		{
			this.setMarginBottom(margin_bot);
			this.setMarginTop(margin_top);
			this.setFontColor(color);
			this.setFontSize(size);
		},
		
		changeState: function(new_state)
		{
			if(new_state == 'preview')
			{
				$(this.selector).find('.module_preview').fadeOut(0);
				$(this.selector).find('.module_html').fadeIn(0);
				$(this.preview).fadeIn(0);
				$(this.textarea).fadeOut(0);
				$(this.selector).css({'padding-left': 15, 'padding-right': 15});
				
			}
			else
			{
				$(this.selector).find('.module_html').fadeOut(0);
				$(this.selector).find('.module_preview').fadeIn(0);
				$(this.textarea).fadeIn(0);
				$(this.preview).fadeOut(0);
				$(this.textarea).keyup();
				$(this.selector).css({'padding-left': 14, 'padding-right': 14});
				
			}
			
			this.state = new_state;
		},
		
		showStyling: function()
		{
			var html = "";
			html += '<div class="module_text_styling">';
			html += '<div class="styling_option styling_bold"></div>';
			html += '<div class="styling_option styling_italic"></div>';
			html += '<div class="styling_option styling_underline"></div>';
			html += '<div class="styling_option styling_sup"></div>';
			html += '<div class="styling_option styling_sub"></div>';
			html += '<div class="styling_option styling_link"></div>';
			html += '</div>';
			$(this.selector).append(html);
			
			$(this.selector).find('.styling_bold').click(function()
			{
				active_module.applyTag('<b>', '</b>');
			});
			
			$(this.selector).find('.styling_italic').click(function()
			{
				active_module.applyTag('<i>', '</i>');
			});

			$(this.selector).find('.styling_underline').click(function()
			{
				active_module.applyTag('<u>', '</u>');
			});
			
			$(this.selector).find('.styling_sub').click(function()
			{
				active_module.applyTag('<sub>', '</sub>');
			});
	
			$(this.selector).find('.styling_sup').click(function()
			{
				active_module.applyTag('<sup>', '</sup>');
			});
			
			$(this.selector).find('.styling_link').click(function()
			{
				active_module.showLinkWindow();
			});
		},
		
		
		
				
		upload: function()
		{
			$('.module_properties_button[method="upload"]').parent().find('input').click();
		},
		
		
		
		
	}
	
	return new_elem;
}