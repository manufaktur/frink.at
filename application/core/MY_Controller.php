<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class MY_Controller extends CI_Controller 
{
    	
    function __construct()
    {
        parent::__construct();
        
        $this->connectDB();
    }  

    public function connectDB()
    {
        if(strpos(site_url(), 'localhost') === false)
        {
            if(strpos(site_url(), 'fileserver') === false)
            {
                $this->load->database('productive');
            }
            else 
                $this->load->database('testing');
        }
        else
            $this->load->database('development');
    }    
}
