<?php

class Frontend_model extends CI_Model  
{
    
    function getConfig($id)
    {
        $this->db->where('id', $id);
        return $this->db->get('config');
    }
    
	function getBackgrounds()
	{
		$this->db->where('ordering !=', -1);
		$this->db->order_by('ordering', 'asc');
		return $this->db->get('background');
	}
	
	function getMetatags()
	{
		$this->db->order_by('name', 'ASC');
	    return $this->db->get('tags');
	}
	
	function getTagsBySubsiteID($id)
	{
	    
	    $this->db->from('subsite_tags');
	    $this->db->join('subsite', 'subsite_tags.subsite_id=subsite.id');
	    $this->db->join('tags', 'subsite_tags.tag_id=tags.id');
	    $this->db->where('subsite_tags.subsite_id', $id);
	    
	    return $this->db->get();
	}
	
	
	function getTagsByArticleID($id)
	{
	    
	    $this->db->from('news_article_metatag');
	    $this->db->join('news_article', 'news_article_metatag.news_article_id=news_article.id');
	    $this->db->join('tags', 'news_article_metatag.news_metatag_id=tags.id');
	    $this->db->where('news_article_metatag.news_article_id', $id);
	    
	    return $this->db->get();
	}
	
	function getMetatagByName($name)
	{
	    $this->db->where('name', $name);
	    return $this->db->get('tags');
	}
	
	
	
	function getMainmenuItems()
	{
	    return $this->db->get('mainmenu_item');
	}
	
	function getNewsArticlesSearch()
	{
	    return $this->db->get('news_article');
	}
	
	function getSubmenuItemsByMainID($main_id)
	{
	    $this->db->where('mainmenu_id', $main_id);
	    $this->db->where('active', 1);
	    $this->db->order_by('ordering', 'asc');
	    $this->db->order_by('name_de', 'asc');
	    return $this->db->get('submenu_item');
	}
	
	function getNewsArticles()
	{
	    $this->db->where('ordering !=', -1);
	    $this->db->where('active', 1);
	    $this->db->order_by('ordering');
	    return $this->db->get('news_article');
	}
	
	
	function getArticlesMagazine()
	{
		$this->db->where('magazine_article', 1);
	    $this->db->where('show_on_magazine', 1);
	    $this->db->where('featured_article', 0);
	    $this->db->where('active', 1);
	    $this->db->order_by('ordering', 'ASC');
	    return $this->db->get('news_article');
	}
	
	
	function getArticlesMagazineImpact()
	{
		$this->db->where('magazine_article', 1);
	    $this->db->where('show_on_magazine', 1);
	    $this->db->where('active', 1);
	    $this->db->order_by('created_date', 'DESC');
	    return $this->db->get('news_article');
	}
	
	function getFeaturedArticlesMagazine()
	{
	    $this->db->where('show_on_magazine', 1);
	    $this->db->where('active', 1);
	    $this->db->where('featured_article', 1);
	    $this->db->order_by('created_date', 'DESC');
	    return $this->db->get('news_article');
	}
	
	
	function getFeaturedHuman()
	{
	    $this->db->where('magazine_article', 1);
	    $this->db->where('active', 1);
	    $this->db->where('featured_article', 1);
	    $this->db->where('magazine_article_type', 1);
	    $this->db->where('show_on_magazine', 1);
	    $this->db->order_by('created_date', 'DESC');
	    return $this->db->get('news_article', 1);
	}
	
	function getFeaturedBusiness()
	{
	    $this->db->where('magazine_article', 1);
	    $this->db->where('active', 1);
	    $this->db->where('featured_article', 1);
	    $this->db->where('magazine_article_type', 2);
	    $this->db->where('show_on_magazine', 1);
	    $this->db->order_by('created_date', 'DESC');
	    return $this->db->get('news_article', 1);
	}
	
	function getFeaturedTechnology()
	{
	    $this->db->where('magazine_article', 1);
	    $this->db->where('active', 1);
	    $this->db->where('featured_article', 1);
	    $this->db->where('magazine_article_type', 3);
	    $this->db->where('show_on_magazine', 1);
	    $this->db->order_by('created_date', 'DESC');
	    return $this->db->get('news_article', 1);
	}
	
	
	function getCountries()
	{
	    $this->db->order_by('priority', 'asc');
	    $this->db->order_by('name_de', 'asc');
	    return $this->db->get('country');
	}
	
	function getNewsArticlesByMetatagID($id)
	{
	    
	    $this->db->from('news_article');
	    $this->db->join('news_article_metatag', 'news_article_metatag.news_article_id=news_article.id');
	    $this->db->where('news_article_metatag.news_metatag_id', $id);
	    $this->db->where('news_article.active', 1);
	    $this->db->order_by('news_article.created_date', 'desc');
	    return $this->db->get();
	}
	
	function getNewsArticlesByMainArticleID($id)
	{
	    
	    $this->db->from('news_article');
	    $this->db->join('related_articles', 'related_articles.related_article_id=news_article.id');
	    $this->db->where('related_articles.main_article_id', $id);
	    $this->db->where('news_article.active', 1);
	    $this->db->order_by('news_article.ordering', 'asc');
	    return $this->db->get();
	}
	
	
	
	function getNewsArticleByID($id)
	{
	    $this->db->where('id', $id);
	    return $this->db->get('news_article');
	}
	
	function getNewsArticlesBySubsite($id)
	{
	    
	    $this->db->from('news_article');
	    $this->db->join('news_subsite_relation', 'news_subsite_relation.news_article_id=news_article.id');
	    $this->db->where('news_subsite_relation.subsite_id', $id);
	    $this->db->where('news_article.active', 1);
	    $this->db->order_by('news_article.ordering', 'asc');
	    return $this->db->get();
	}
	
	function getNewsArticleByPrettyURL($prettyurl)
	{
	    $this->db->select('news_article.*');
	    $this->db->where('prettyurl', $prettyurl);
	    return $this->db->get('news_article');
	}
	
	function getContentByModule($parent_id, $parent_type, $table)
	{
	    $this->db->where('parent_type', $parent_type);
	    $this->db->where('parent_id', $parent_id);
	    return $this->db->get($table);
	}
	
	// BACKEND + FRONTEND
	function getColors()
	{
	    return $this->db->get('color');
	}

	function getSubsiteByID($id)
	{
	    $this->db->where('id', $id);
	    return $this->db->get('subsite');
	}
	
	function getMainByName($name)
	{
	    $this->db->where('name', $name);
	    return $this->db->get('mainmenu_item');
	}
	
	function getMainByID($id)
	{
	    $this->db->where('id', $id);
	    return $this->db->get('mainmenu_item');
	}
	
	function getMainOfSubsite($id)
	{
	    $this->db->where('subsite_id', $id);
	    return $this->db->get('subsite_connection');
	}
	
	
	function getSubsitesOfMain($id)
	{
	    $this->db->where('main_id', $id);
	    return $this->db->get('subsite_connection');
	}
	
	
	function getMostRead()
	{
	    $this->db->where('most_read', 1);
	    return $this->db->get('news_article');
	}
	
	
	
	function insertNewsletterEmail($email)
	{
	    $this->db->insert('newsletter_email', array('email' => $email));
	}
	
	
	public function getGalleryModuleImages($module_id)
	{
	
	    $this->db->where('gallery_module_id', $module_id);
	    return $this->db->get('module_gallery_item');
	}
	
	public function searchSubsiteByName($like)
	{
		$this->db->like('name', $like); 
		return $this->db->get('subsite');
	}
	public function searchArticleByName($like)
	{
		$this->db->like('headline', $like); 
		$this->db->or_like('teaser_text', $like);
		$this->db->where('id !=' . UNIQUE_TOYSTORE_NEWS_ARTICLE_ID);
		return $this->db->get('news_article');
	}
	public function searchMetatagByName($like)
	{
		$this->db->like('name_de', $like); 
		$this->db->or_like('name_en', $like);
		return $this->db->get('news_metatag');
	}
	
	public function searchTextModuleByName($like)
	{
		$this->db->like('content', $like); 
		return $this->db->get('module_text');
	}
	
	public function searchBulletpointModuleByName($like)
	{
	    $this->db->like('content', $like);
	    return $this->db->get('module_bulletpoint');
	}
	
	
	function getSlider($column)
	{
	    $this->db->where('column_id', $column);
	    $this->db->order_by('order', 'asc');
	    $result = $this->db->get('slider_items');
	    return ( $result->num_rows() > 0 ) ? $result->result_array() : array();
	}
	
	function getSliderTimers()
	{
	    $this->db->where('id', 1);
	    $result = $this->db->get('slider_timers');
	    return $result->row();
	}
	
}
?>