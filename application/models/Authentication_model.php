<?php

class Authentication_model extends CI_Model

{

    function getPW($username)
    
    {
        $this->db->where('username', $username);
        
        $this->db->select('pword');
        
        return $this->db->get('user');
    }

    function getUserdataByUsername($username)
    
    {
        $this->db->where('username', $username);
        
        return $this->db->get('user');
    }

    function getUserdataByID($id)
    
    {
        $this->db->where('id', $id);
        
        return $this->db->get('user');
    }
}

?>