<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Backend extends MY_Controller 
{
	protected $user;
	protected $base_url;
	protected $module_tables = array();

	
    function __construct()
    {
        parent::__construct();
		
        date_default_timezone_set('Europe/Vienna');
        
		if(!$this->logged_in())
			redirect('authentication/showLogin');
		
		$this->load->model('Authentication_model');
		$this->user = $this->Authentication_model->getUserdataByID($this->session->userdata('user_id'))->row();
		$this->load->library('Besc_crud');
		$this->load->model('Backend_model', 'b_model');
		$this->load->helper('besc_helper');
		$this->config->load('st_anna');
		$this->module_tables = $this->config->item('st_anna_module_types');
		
    }  

	public function index()
	{
		$this->page('backend/home', array());
	}

	public function support()
	{
		$this->page('backend/support', array());
	}	
	
	
	public function mainmenu_item()
	{
		$bc = new besc_crud();
		$bc->table('mainmenu_item');
		$bc->primary_key('id');
		$bc->title('Mainmenu');
		$bc->list_columns(array('name'));
		
		
		$bc->columns(array
	    (
	        'name' => array
	        (  
	            'db_name' => 'name',
				'type' => 'text',
				'display_as' => 'Name'
	        ),
	        
	        
	        'article_metatag_relation' => array 
	        (
                'relation_id' => 'article_metatag',
                'type' => 'm_n_relation',
                'table_mn' => 'subsite_connection',
                'table_mn_pk' => 'id',
                'table_mn_col_m' => 'main_id',
                'table_mn_col_n' => 'subsite_id',
                'table_m' => 'mainmenu_item',
                'table_n' => 'subsite',
                'table_n_pk' => 'id',
                'table_n_value' => 'name',
                'display_as' => 'Subsites'
            ),
		));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);		
	}
	
	
	
	public function news_metatag()
	{
		$bc = new besc_crud();
		$bc->table('tags');
		$bc->primary_key('id');
		$bc->title('Tags');
		$bc->list_columns(array('display_name'));
		
		
		$bc->columns(array
	    (
	        'name' => array
	        (  
	            'db_name' => 'name',
				'type' => 'text',
				'display_as' => 'Tag'
	        ),
	        
	         'display_name' => array
	        (  
	            'db_name' => 'display_name',
				'type' => 'text',
				'display_as' => 'Display Name'
	        ),
	        
	        
		));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);		
	}
	
	public function news_article()
	{
		$bc = new besc_crud();
		$bc->table('news_article');
		$bc->primary_key('id');
		$bc->title('News article');
		$bc->where('id NOT IN (' . UNIQUE_TOYSTORE_NEWS_ARTICLE_ID . ', ' . UNIQUE_DONATE_NEWS_ARTICLE_ID . ')');
		
		$bc->custom_buttons(array(array('name' => 'Edit news article',
                            		    'icon' => site_url('items/backend/img/icon_edit_article.png'),
                            		    'add_pk' => true,
                            		    'url' => 'edit_article')));
		
		$bc->list_columns(array('headline', 'article_metatag_relation', 'teaser_img'));
		
		$users = $this->b_model->getUsers();
		$userlist = array();
		foreach($users->result() as $user)
		{
		    $userlist[] = array
		    (
		        'key' => $user->id,
		        'value' => $user->firstname . ' ' . $user->lastname,
		        'header' => 'Edit subsite',
		        
		    );
		}
		
		$subsites = array();
		foreach($this->b_model->getSubsites()->result() as $sites)
		{
		    $subsites[] = array
		    (
		        'key' => $sites->id,
		        'value' => $sites->name
		    );
		}
		
		foreach($this->config->item('st_anna_unique_pages') as $unique)
		{
		    $subsites[] = array
		    (
		        'key' => $unique['id'],
		        'value' => $unique['name']
		    );
		}
		
		$type_selector = array();
		
		
		
		
		$type_selector[] = array
	    (
	        'key' => 1,
	        'value' => 'HUMAN IMPACT'
	    );
	    
	    $type_selector[] = array
	    (
	        'key' => 2,
	        'value' => 'BUSINESS IMPACT'
	    );
		
		$type_selector[] = array
	    (
	        'key' => 3,
	        'value' => 'TECHNOLOGY IMPACT'
	    );
		
		
		$selector = array();
		
	    $selector[] = array
	    (
	        'key' => 0,
	        'value' => 'NO'
	    );
	    
	    $selector[] = array
	    (
	        'key' => 1,
	        'value' => 'YES'
	    );
		
		$bc->columns(array
	    (
            'headline' => array
	        (
                'db_name' => 'headline',
                'type' => 'multiline',
                'display_as' => 'Headline',
	            'height' => 35
            ),
			
			'sub_headline' => array
	        (
                'db_name' => 'sub_headline',
                'type' => 'multiline',
                'display_as' => 'Sub Headline',
	            'height' => 35
            ),
			
		    'prettyurl' => array
	        (
                'db_name' => 'prettyurl',
                'type' => 'text',
                'display_as' => 'Short url'
            ),	
            
            'section_id' => array
	        (
                'db_name' => 'section_id',
                'type' => 'text',
                'display_as' => 'Section ID',
                'col_info' => 'Add the ID of the section if the page should auto jump there when opened',
            ),	    
						
			
			'article_metatag_relation' => array 
	        (
                'relation_id' => 'article_metatag',
                'type' => 'm_n_relation',
                'table_mn' => 'news_article_metatag',
                'table_mn_pk' => 'id',
                'table_mn_col_m' => 'news_article_id',
                'table_mn_col_n' => 'news_metatag_id',
                'table_m' => 'headline',
                'table_n' => 'tags',
                'table_n_pk' => 'id',
                'table_n_value' => 'display_name',
                'display_as' => 'Tags'
            ),
	    
			'related_articles' => array 
	        (
                'relation_id' => 'related_articles',
                'type' => 'm_n_relation',
                'table_mn' => 'related_articles',
                'table_mn_pk' => 'id',
                'table_mn_col_m' => 'main_article_id',
                'table_mn_col_n' => 'related_article_id',
                'table_m' => 'headline',
                'table_n' => 'news_article',
                'table_n_pk' => 'id',
                'table_n_value' => 'headline',
                'display_as' => 'Related articles'
            ),
		
		
		
            'teaser_img' => array
	        (  
                'db_name' => 'teaser_img',
                'type' => 'image',
                'display_as' => 'Teaser image',
                'col_info' => 'Files: .png, .jpg, .jpeg --- Resolution: 105 x 115 px',
                'accept' => '.png,.jpg,.jpeg',
                'uploadpath' => 'items/general/uploads/article_teaser'
            ),
	        
	        'teaser_text' => array
	        (
	            'db_name' => 'teaser_text',
	            'type' => 'multiline',
	            'col_info' => 'Maximum ~125 characters',
	            'display_as' => 'Teaser text',
	            'height' => 40,
	        ),
	        
	        'magazine_article' => array
            (
                'db_name' => 'magazine_article',
                'type' => 'select',
                'display_as' => 'Magazine Article',
                'options' => $selector,
            ), 
            
	        'magazine_article_type' => array
            (
                'db_name' => 'magazine_article_type',
                'type' => 'select',
                'display_as' => 'Magazine Article Type',
                'options' => $type_selector,
            ), 
	        
	        'show_on_magazine' => array
            (
                'db_name' => 'show_on_magazine',
                'type' => 'select',
                'display_as' => 'Show on magazine page',
                'options' => $selector,
            ), 
            
            
            'most_read' => array
            (
                'db_name' => 'most_read',
                'type' => 'select',
                'display_as' => 'Show on most read page',
                'options' => $selector,
            ),
            
			'ordering' => array
            (
                'db_name' => 'ordering',
                'type' => 'text',
                'display_as' => 'Order',
                 'value' => 0,
               
            ), 
	        
	        
	       'featured_article' => array
            (
                'db_name' => 'featured_article',
                'type' => 'select',
                'display_as' => 'Featured article',
                'options' => $selector,
            ), 
	        
	        'featured_magazine_image' => array
	        (  
                'db_name' => 'featured_magazine_image',
                'type' => 'image',
                'display_as' => 'Featured Teaser image',
                'col_info' => 'Files: .png, .jpg, .jpeg --- Resolution: 250 x 144 px',
                'accept' => '.png,.jpg,.jpeg',
                'uploadpath' => 'items/general/uploads/article_teaser'
            ),
	        
	        'featured_teaser_text' => array
	        (
	            'db_name' => 'featured_magazine_text',
	            'type' => 'multiline',
	            'col_info' => 'Maximum ~200 characters',
	            'display_as' => 'Featured Teaser text',
	            'height' => 40,
	        ),
	        
	        'article_subsite_relation' => array 
	        (
                'relation_id' => 'news_subsite_relation',
                'type' => 'm_n_relation',
                'table_mn' => 'news_subsite_relation',
                'table_mn_pk' => 'id',
                'table_mn_col_m' => 'news_article_id',
                'table_mn_col_n' => 'subsite_id',
                'table_m' => 'name',
                'table_n' => 'subsite',
                'table_n_pk' => 'id',
                'table_n_value' => 'name',
                'display_as' => 'Subsites'
            ),

	       
	    ));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);		
	}
	
	
	public function slider_timers()
	{
		$bc = new besc_crud();
		$bc->table('slider_timers');
		$bc->primary_key('id');
		$bc->title('Slider Timers');
		$bc->list_columns(array('left_slider', 'center_slider', 'right_slider'));
		
		$bc->unset_add();
		$bc->unset_delete();
		$bc->columns(array
	    (
	        'left_slider' => array
	        (  
	            'db_name' => 'left_slider',
				'type' => 'text',
				'display_as' => 'Slider timer',
				'col_info' => 'Timer in seconds, set it to 0 for no auto scrolling',
	        ),
	        
	        
	        /*'center_slider' => array
	        (  
	            'db_name' => 'center_slider',
				'type' => 'text',
				'display_as' => 'Center Slider timer',
				'col_info' => 'Timer in seconds, set it to 0 for no auto scrolling',
	        ),
	        
	        'right_slider' => array
	        (  
	            'db_name' => 'right_slider',
				'type' => 'text',
				'display_as' => 'Right Slider timer',
				'col_info' => 'Timer in seconds, set it to 0 for no auto scrolling',
	        ),*/
		));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);		
	}
	
	
	
	
	
	public function edit_article($parent_id)
    {
        $this->edit_content_article($parent_id, MODULE_PARENT_TYPE_NEWS);
    }    
	
	
	public function slider_items()
	{
		$bc = new besc_crud();
		$bc->table('slider_items');
		$bc->primary_key('id');
		$bc->title('Slider item');
		$bc->where('id NOT IN (' . UNIQUE_TOYSTORE_NEWS_ARTICLE_ID . ', ' . UNIQUE_DONATE_NEWS_ARTICLE_ID . ')');
		
		$bc->custom_buttons(array());
		
		$bc->list_columns(array('name', 'column_id', 'fname', 'text', 'order'));
		
		
		$articles = array();
		$articles[] = array
		    (
		        'key' => '0',
		        'value' => 'External Site'
		    );
		foreach($this->b_model->getNewsArticles()->result() as $article)
		{
		    $articles[] = array
		    (
		        'key' => 'article/'.$article->prettyurl,
		        'value' => $article->headline
		    );
		}
		
		$type_selector = array();		
		$type_selector[] = array
	    (
	        'key' => 1,
	        'value' => 'LEFT COLUMN'
	    );
	    
	    $type_selector[] = array
	    (
	        'key' => 2,
	        'value' => 'CENTER COLUMN'
	    );
		
		$type_selector[] = array
	    (
	        'key' => 3,
	        'value' => 'RIGHT COLUMN'
	    );
		
		
		$selector = array();
		
	    $selector[] = array
	    (
	        'key' => 0,
	        'value' => 'NO'
	    );
	    
	    $selector[] = array
	    (
	        'key' => 1,
	        'value' => 'YES'
	    );
		
		$bc->columns(array
	    (
            'name' => array
	        (
                'db_name' => 'name',
                'type' => 'text',
                'display_as' => 'Name'
            ),
			
			'column_id' => array
	        (
                'db_name' => 'column_id',
                'type' => 'select',
                'display_as' => 'Column',
                'options' => $type_selector,
            ),
			
		    'text' => array
	        (
                'db_name' => 'text',
                'type' => 'text',
                'display_as' => 'Text over the image',
                'col_info' => 'This should only be a few words',
            ),		    
			
			
			'fname' => array
	        (  
                'db_name' => 'fname',
                'type' => 'image',
                'display_as' => 'Image',
                'col_info' => 'Files: .png, .jpg, .jpeg --- Resolution: 250 x 370 px',
                'accept' => '.png,.jpg,.jpeg',
                'uploadpath' => 'items/general/uploads/slider_images'
            ),
			
			
			'order' => array
            (
                'db_name' => 'order',
                'type' => 'text',
                'display_as' => 'Order',
                 'value' => 0,
               
            ),	
			
			'pretty_url' => array
            (
                'db_name' => 'pretty_url',
                'type' => 'select',
                'display_as' => 'Link to article',
                'options' => $articles,
            ), 
            
             'external_link' => array
	        (
                'db_name' => 'external_link',
                'type' => 'text',
                'display_as' => 'External Link',
                'col_info' => 'Set the Link to article to "External Site" for this to work',
            ),	
	        	       
	    ));
		
		$data['crud_data'] = $bc->execute();
		$this->page('backend/crud', $data);		
	}

	
	
	
	
	
	
	
	
	
	/***********************************************************************************
	 * FRONTPAGE ARTICLES
	 **********************************************************************************/	
    public function frontpage_articles()
    {
        $data['site'] = 'frontpage_articles';
        $data['articles_available'] = $this->b_model->getFrontpageArticlesAvailable();
        $data['articles_active'] = $this->b_model->getFrontpageArticlesActive();
        
        $this->page('backend/frontpage_articles', $data);
    }
    
    public function save_frontpage_articles()
    {
        $content = json_decode(file_get_contents('php://input'));
        $col = array();
         
        $success = true;
        $message = "Frontpage articles successfully changed.";
         
        if($this->b_model->resetFrontpageArticles())
        {
            foreach($content as $article)
            {
                $article_id = $article->article_id;
                $ordering = $article->ordering;
                 
                $articles[] = array('id' => $article_id,
                    'ordering' => $ordering);
            }
             
            if(!$this->b_model->saveFrontpageArticles($articles, 'id'))
            {
                $success = false;
                $message = "Error while saving frontpage articles.";
            }
        }
        else
        {
            $success = false;
            $message = "Error while resetting frontpage articles.";
        }
         
        echo json_encode(array( 'success' => $success,
                                'message' => $message));        
    }
	
	/***********************************************************************************
	 * DISPLAY FUNCTIONS
	 **********************************************************************************/	
	
    public function page($content_view, $content_data)
    {
    	$data = array();
		$data['username'] = $this->user->username;
		$data['additional_css'] = isset($content_data['additional_css']) ? $content_data['additional_css'] : array();
		$data['additional_js'] = isset($content_data['additional_js']) ? $content_data['additional_js'] : array();
        $data['site'] = (isset($content_data['site']) ? $content_data['site'] : '');
		
		$this->load->view('backend/head', $data);
		$this->load->view('backend/menu', $data);
        $this->load->view($content_view, $content_data);
		$this->load->view('backend/footer', $data);
    }	
	
    
    
    /***********************************************************************************
     * CONTENT EDIT
     **********************************************************************************/    
    
    public function content_imageupload()
    {
        $filename = $_POST['filename'];
        $upload_path = $_POST['uploadpath'];
    
        if(substr($upload_path, -1) != '/')
            $upload_path .= '/';
    
        $rnd = rand_string(12);
        $data = explode(',', $_POST['data']);
    
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
    
        $serverFile = time() . "_" . $rnd . "." . $ext;
        $fp = fopen(getcwd() . '/' . $upload_path . $serverFile, 'w');
         
        fwrite($fp, base64_decode($data[1]));
        fclose($fp);
    
        echo json_encode
        (
            array
            (
                'success' => true,
                'filename' => $serverFile
            )
        );
    }
    
    public function content_galleryimageupload()
    {
        $filename = $_POST['filename'];
        $upload_path = $_POST['uploadpath'];
    
        if(substr($upload_path, -1) != '/')
            $upload_path .= '/';
    
        $rnd = rand_string(12);
        $data = explode(',', $_POST['data']);
    
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
    
        $serverFile = time() . "_" . $rnd . "." . $ext;
        $fp = fopen(getcwd() . '/' . $upload_path . $serverFile, 'w');
         
        fwrite($fp, base64_decode($data[1]));
        fclose($fp);
    
        echo json_encode
        (
            array
            (
                'success' => true,
                'filename' => $serverFile
            )
        );
    }
    
    public function content_download_fileupload()
    {
        $filename = $_POST['filename'];
        $upload_path = $_POST['uploadpath'];
    
        if(substr($upload_path, -1) != '/')
            $upload_path .= '/';
    
        $rnd = rand_string(12);
        $data = explode(',', $_POST['data']);
    
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
    
        $fp = fopen(getcwd() . '/' . $upload_path . $filename, 'w');
         
        fwrite($fp, base64_decode($data[1]));
        fclose($fp);
    
        echo json_encode
        (
            array
            (
                'success' => true,
                'filename' => $filename
            )
        );
    }    
    
    public function save_content_modules($parent_type, $parent_id)
    {
        $content = json_decode(file_get_contents('php://input'));
        $col = array();
         
        $success = true;
        $message = "Content saved";
         
        if($this->deleteContent($parent_id, $parent_type))
        {
            foreach($content as $module)
            {
                switch($module->type)
                {
                    case 'image':
                        $mod = $this->save_module_image($module, $parent_id, $parent_type);
                        $table = $this->module_tables['image'];
                        break;
                    case 'text':
                        $mod = $this->save_module_text($module, $parent_id, $parent_type);
                        $table = $this->module_tables['text'];
                        break;
                    case 'gallery':
                        $mod = $this->save_module_gallery($module, $parent_id, $parent_type);
                        $table = $this->module_tables['gallery'];
                        break;
                    case 'download':
                        $mod = $this->save_module_download($module, $parent_id, $parent_type);
                        $table = $this->module_tables['download'];
                        break;
                    case 'bulletpoint':
                        $mod = $this->save_module_bulletpoint($module, $parent_id, $parent_type);
                        $table = $this->module_tables['bulletpoint'];
                        break;
                    case 'link':
                        $mod = $this->save_module_link($module, $parent_id, $parent_type);
                        $table = $this->module_tables['link'];
                        break;
                    case 'button':
                        $mod = $this->save_module_button($module, $parent_id, $parent_type);
                        $table = $this->module_tables['button'];
                        break;                      
                    case 'impact':
                        $mod = $this->save_module_impact($module, $parent_id, $parent_type);
                        $table = $this->module_tables['impact'];
                        break;
                    case 'video':
                        $mod = $this->save_module_video($module, $parent_id, $parent_type);
                        $table = $this->module_tables['video'];
                        break;
                    case 'text_image':
                        $mod = $this->save_module_text_image($module, $parent_id, $parent_type);
                        $table = $this->module_tables['text_image'];
                        break;
                }
                
                if($module->type == "gallery")
                {
                	$last = $this->b_model->saveContentPerModuleGallery($mod, $table);
                	
	                if($last != false)
	                {
	                	
	                	foreach($module->images as $image)
	                	{
		                	$data = array('gallery_module_id' => $last,
		       							'fname' => $image->fname,
		       							'text' => $image->text);
		       				$this->b_model->saveGalleryImage($data);
	                	}
  
	                } 
                }
                else
                {
	               if(!$this->b_model->saveContentPerModule($mod, $table))
	                {
	                    $success = false;
	                    $message = "Error while saving module";
	                } 
                }
                
                
            }
        }
        else
        {
            $success = false;
            $message = "Error while resetting modules.";
        }
         
        echo json_encode
        (
            array
            (
                'success' => $success,
                'message' => $message
            )
        );
    }
    
    protected function save_module_gallery($module, $parent_id, $parent_type)
    {
        return array
        (
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'ordering' => $module->ordering,
            'margin_top' => $module->margin_top,
            'margin_bottom' => $module->margin_bottom,
            'top_pos' => $module->top_pos,
            'left_pos' => $module->left_pos,
           
        );
    }
    
    protected function save_module_image($module, $parent_id, $parent_type)
    {
        return array
        (
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'ordering' => $module->ordering,
            'fname' => $module->fname,
            'text' => $module->text,
            'margin_top' => $module->margin_top,
            'margin_bottom' => $module->margin_bottom,
            'width' => $module->width,
            'top_pos' => $module->top_pos,
            'left_pos' => $module->left_pos,
        );
    }
    
    protected function save_module_text($module, $parent_id, $parent_type)
    {
        return array
        (
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'ordering' => $module->ordering,
            'content' => $module->text,
            'margin_top' => $module->margin_top,
            'margin_bottom' => $module->margin_bottom,
            'font_color' => $module->font_color,
            'font_size' => $module->font_size,
            'align' => $module->align,
            'right_side_img' => $module->right_side_img,
            'right_side_img_top' => $module->right_side_img_top,
            'right_side_img_text' => $module->right_side_img_text,
            'top_pos' => $module->top_pos,
            'left_pos' => $module->left_pos,
        );
    } 
    
    
    protected function save_module_text_image($module, $parent_id, $parent_type)
    {
        return array
        (
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'ordering' => $module->ordering,
            'content' => $module->text,
            'margin_top' => $module->margin_top,
            'margin_bottom' => $module->margin_bottom,
            'font_color' => $module->font_color,
            'font_size' => $module->font_size,
            'align' => $module->align,
            'fname' => $module->fname,
            'top_pos' => $module->top_pos,
            'left_pos' => $module->left_pos,
        );
    }    
    
    protected function save_module_download($module, $parent_id, $parent_type)
    {
        return array
        (
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'ordering' => $module->ordering,
            'fname' => $module->fname,
            'margin_top' => $module->margin_top,
            'margin_bottom' => $module->margin_bottom,
            'text' => $module->text,
        );
    }

    protected function save_module_bulletpoint($module, $parent_id, $parent_type)
    {
        return array
        (
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'ordering' => $module->ordering,
            'content' => $module->text,
            'margin_top' => $module->margin_top,
            'margin_bottom' => $module->margin_bottom,
            'font_color' => $module->font_color,
            'font_size' => $module->font_size,
            'top_pos' => $module->top_pos,
            'left_pos' => $module->left_pos,
        );
    }  
    
     protected function save_module_link($module, $parent_id, $parent_type)
    {
        return array
        (
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,         
            'content' => $module->text,         
        );
    }   
    
     protected function save_module_button($module, $parent_id, $parent_type)
     {
        return array
        (
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,         
            'ordering' => $module->ordering,  
            'top_pos' => $module->top_pos,
            'left_pos' => $module->left_pos,
            'target' => $module->target,
            'text' => $module->text,
            'url' => $module->url,
                
        );
    }  
    
     protected function save_module_impact($module, $parent_id, $parent_type)
     {
     	
        return array
        (
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,         
            'left_content' => $module->left_content,  
            'middle_content' => $module->middle_content,
            'right_content' => $module->right_content,  
            'left_title' => $module->left_title,   
            'middle_title' => $module->middle_title,
            'right_title' => $module->right_title,
            'top_pos' => $module->top_pos,
            'left_pos' => $module->left_pos,
            'left_image' => $module->left_image,  
            'middle_image' => $module->middle_image, 
            'right_image' => $module->right_image, 
            'left_color' => $module->left_color,
			'left_align' => $module->left_align,
			'middle_color' => $module->middle_color,
			'middle_align' => $module->middle_align,
			'right_color' => $module->right_color,
			'right_align' => $module->right_align,
        );
    }  
    
    
    
    protected function save_module_video($module, $parent_id, $parent_type)
     {
     	
        return array
        (
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,         
            'code' => $module->code,  
            'ordering' => $module->ordering, 
            'top_pos' => $module->top_pos,
            'left_pos' => $module->left_pos,            
        );
    }  
    
    protected function deleteContent($parent_id, $parent_type)
    {
        $success = true;
        foreach($this->module_tables as $table)
        {
            if(!$this->b_model->deleteContentPerModule($parent_id, $parent_type, $table))
                $success = false;
        }
        
        return $success;
    }
    
    
    /***********************************************************************************
     * MENUS
     **********************************************************************************/
    public function mainmenu()
    {
        $bc = new besc_crud();
        $bc->table('mainmenu_item');
        $bc->primary_key('id');
        $bc->title('Main menu');
        $bc->unset_add();
        $bc->unset_delete();
        
        $subsites = array();
        $subsites[] = array(
            'key' => 0,
            'value' => 'no subsite',  
        );
        
        foreach($this->b_model->getSubsites()->result() as $sites)
        {
            $subsites[] = array
            (
                'key' => $sites->id,
                'value' => $sites->name
            );
        }
        foreach($this->config->item('st_anna_unique_pages') as $unique)
        {
            $subsites[] = array
            (
                'key' => $unique['id'],
                'value' => $unique['name']
            );
        }
        
        $bc->columns(array
        (
            'name' => array
            (
                'db_name' => 'name',
                'type' => 'text',
                'display_as' => 'Name',
            ),
    
            'image' => array
            (
                'db_name' => 'image',
                'type' => 'image',
                'display_as' => 'Menu image',
                'col_info' => 'Files: .png, .jpg, .jpeg',
                'accept' => '.png,.jpg,.jpeg',
                'uploadpath' => 'items/general/uploads/menu_images'
            ),
            
            'subsite_id' => array
            (
                'db_name' => 'subsite_id',
                'type' => 'select',
                'display_as' => 'Subsite link',
                'options' => $subsites,
            ),
    
        ));
         
        $data['crud_data'] = $bc->execute();
        $this->page('backend/crud', $data);
    } 
    
    public function submenu()
    {
        $bc = new besc_crud();
        $bc->table('submenu_item');
        $bc->primary_key('id');
        $bc->title('Sub menu');
        
        $mainmenu = array();
        foreach($this->b_model->getMainMenuItems()->result() as $items)
        {
            $mainmenu[] = array
            (
                'key' => $items->id,
                'value' => $items->name,    
            );
        }
        
        $subsites = array();
        foreach($this->b_model->getSubsites()->result() as $sites)
        {
            $subsites[] = array
            (
                'key' => $sites->id,
                'value' => $sites->name
            );
        }
        
        foreach($this->config->item('st_anna_unique_pages') as $unique)
        {
            $subsites[] = array
            (
                'key' => $unique['id'],
                'value' => $unique['name']
            );
        }
        
        $bc->columns(array
        (
            'name_de' => array
            (
                'db_name' => 'name_de',
                'type' => 'text',
                'display_as' => 'Name',
            ),
    
            'mainmenu_id' => array
            (
                'db_name' => 'mainmenu_id',
                'type' => 'select',
                'display_as' => 'Parent menu',
                'options' => $mainmenu,
            ),
            
            'target' => array
            (
                'db_name' => 'target',
                'type' => 'select',
                'display_as' => 'Subsite',
                'options' => $subsites,
            ),            
        ));
         
        $data['crud_data'] = $bc->execute();
        $this->page('backend/crud', $data);
    } 

    
    /***********************************************************************************
     * MENUS
     **********************************************************************************/
    public function subsites()
    {
        $bc = new besc_crud();
        $bc->table('subsite');
        $bc->primary_key('id');
        $bc->title('Subsite');
        $bc->unset_delete();
        $bc->list_columns(array('name', 'prettyurl'));
        
        $bc->custom_buttons(array(array('name' => 'Edit subsite',
            'icon' => site_url('items/backend/img/icon_edit_article.png'),
            'add_pk' => true,
            'url' => 'edit_subsite')));
        
        
        $subsites = array();
        foreach($this->b_model->getSubsites()->result() as $sites)
        {
            $subsites[] = array
            (
                'key' => $sites->id,
                'value' => $sites->name
            );
        }
        
        foreach($this->config->item('st_anna_unique_pages') as $unique)
        {
            $subsites[] = array
            (
                'key' => $unique['id'],
                'value' => $unique['name']
            );
        }        
    
        $bc->columns(array
            (
                'name' => array
                (
                    'db_name' => 'name',
                    'type' => 'text',
                    'display_as' => 'Name',
                ),
    
                'prettyurl' => array
                (
                    'db_name' => 'prettyurl',
                    'type' => 'text',
                    'display_as' => 'Short url',
                ),
                
                'sub_header' => array
                (
                    'db_name' => 'sub_header',
                    'type' => 'text',
                    'display_as' => 'Sub header',
                ),
                
                'subsite_metatag_relation' => array 
		        (
	                'relation_id' => 'subsite_tag',
	                'type' => 'm_n_relation',
	                'table_mn' => 'subsite_tags',
	                'table_mn_pk' => 'id',
	                'table_mn_col_m' => 'subsite_id',
	                'table_mn_col_n' => 'tag_id',
	                'table_m' => 'headline',
	                'table_n' => 'tags',
	                'table_n_pk' => 'id',
	                'table_n_value' => 'display_name',
	                'display_as' => 'Tags'
	            ),
                
                
            /*    'right_side_image' => array
                (
                    'db_name' => 'right_side_image',
                    'type' => 'image',
                    'display_as' => 'Right side image',
                    'col_info' => 'Files: .png, .jpg, .jpeg<br/>Width: 245px',
                    'accept' => '.png,.jpg,.jpeg',
                    'uploadpath' => 'items/general/uploads/sidebar_image',
                ),
                
                'right_side_link' => array
                (
                    'db_name' => 'right_side_link',
                    'type' => 'select',
                    'display_as' => 'Image link to',
                    'options' => $subsites,
                ),     */           
    
            ));
         
        $data['crud_data'] = $bc->execute();
        $this->page('backend/crud', $data);
    }
    
    public function edit_subsite($parent_id)
    {
    	if($parent_id == 5)
    	{
	       /* MAGAZINE */
	       $this->edit_content_magazine($parent_id, MODULE_PARENT_TYPE_SUBSITE);
    	}
    	else
    	{
	    	$this->edit_content_subsite($parent_id, MODULE_PARENT_TYPE_SUBSITE);
    	}
    	
       
    }    
	
	protected function edit_content_magazine($parent_id, $parent_type)
    {
        $modules = array();
        foreach($this->module_tables as $key => $value)
        {
            foreach($this->b_model->getContentPerModule($parent_id, $parent_type, $value)->result_array() as $module)
            {
                if($key == "gallery")
                {
                    $images = $this->b_model->getImagesPerGalleryModule($module['id'])->result();
                    if($images)
                    {
                        $image_holder = array();
                        foreach($images as $image)
                        {
                            $image_holder[] = array
                            (
                                'fname' => $image->fname,
                                'text' => $image->text,
                            );
                        }
                         
                        $module['images'] = $image_holder;
                        $modules[$module['ordering']] = array
                        (
                            'type' => $key,
                            'properties' => $module,
                        );
                    }
                }
                else
                {
                    $modules[$module['ordering']] = array
                    (
                        'type' => $key,
                        'properties' => $module,
                    );
                }
            }
        }
        ksort($modules);
        
        $data = array
        (
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'modules' => json_encode($modules),
            'colors' => $this->b_model->getColors(),
            'subsites' => $this->b_model->getSubsites(),
            'text_templates' => $this->b_model->getTextTemplates(),
            'header' => $parent_type == MODULE_PARENT_TYPE_NEWS ? 'Edit news article' : 'Edit subsite',
            'name' => $parent_type == MODULE_PARENT_TYPE_NEWS ? $this->b_model->getNewsArticleByID($parent_id)->row()->headline : $this->b_model->getSubsiteByID($parent_id)->row()->name,
        );
        $this->page('backend/edit_content_subsite', $data);        
    }
	
    
    protected function edit_content_subsite($parent_id, $parent_type)
    {
        $modules = array();
        foreach($this->module_tables as $key => $value)
        {
            foreach($this->b_model->getContentPerModule($parent_id, $parent_type, $value)->result_array() as $module)
            {
                if($key == "gallery")
                {
                    $images = $this->b_model->getImagesPerGalleryModule($module['id'])->result();
                    if($images)
                    {
                        $image_holder = array();
                        foreach($images as $image)
                        {
                            $image_holder[] = array
                            (
                                'fname' => $image->fname,
                                'text' => $image->text,
                            );
                        }
                         
                        $module['images'] = $image_holder;
                        $modules[$module['ordering']] = array
                        (
                            'type' => $key,
                            'properties' => $module,
                        );
                    }
                }
                else
                {
                    $modules[$module['ordering']] = array
                    (
                        'type' => $key,
                        'properties' => $module,
                    );
                }
            }
        }
        ksort($modules);
        
        $data = array
        (
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'modules' => json_encode($modules),
            'colors' => $this->b_model->getColors(),
            'subsites' => $this->b_model->getSubsites(),
            'text_templates' => $this->b_model->getTextTemplates(),
            'header' => $parent_type == MODULE_PARENT_TYPE_NEWS ? 'Edit news article' : 'Edit subsite',
            'name' => $parent_type == MODULE_PARENT_TYPE_NEWS ? $this->b_model->getNewsArticleByID($parent_id)->row()->headline : $this->b_model->getSubsiteByID($parent_id)->row()->name,
        );
        $this->page('backend/edit_content_subsite', $data);        
    }
    
    
     protected function edit_content_article($parent_id, $parent_type)
    {
        $modules = array();
        foreach($this->module_tables as $key => $value)
        {
            foreach($this->b_model->getContentPerModule($parent_id, $parent_type, $value)->result_array() as $module)
            {
                if($key == "gallery")
                {
                    $images = $this->b_model->getImagesPerGalleryModule($module['id'])->result();
                    if($images)
                    {
                        $image_holder = array();
                        foreach($images as $image)
                        {
                            $image_holder[] = array
                            (
                                'fname' => $image->fname,
                                'text' => $image->text,
                            );
                        }
                         
                        $module['images'] = $image_holder;
                        $modules[$module['ordering']] = array
                        (
                            'type' => $key,
                            'properties' => $module,
                        );
                    }
                }
                else
                {
                    $modules[$module['ordering']] = array
                    (
                        'type' => $key,
                        'properties' => $module,
                    );
                }
            }
        }
        ksort($modules);
        
        $data = array
        (
            'parent_id' => $parent_id,
            'parent_type' => $parent_type,
            'modules' => json_encode($modules),
            'colors' => $this->b_model->getColors(),
            'subsites' => $this->b_model->getSubsites(),
            'text_templates' => $this->b_model->getTextTemplates(),
            'header' => $parent_type == MODULE_PARENT_TYPE_NEWS ? 'Edit news article' : 'Edit subsite',
            'name' => $parent_type == MODULE_PARENT_TYPE_NEWS ? $this->b_model->getNewsArticleByID($parent_id)->row()->headline : $this->b_model->getSubsiteByID($parent_id)->row()->name,
        );
        $this->page('backend/edit_content_article', $data);        
    }
    /***********************************************************************************
     * SETTINGS
     **********************************************************************************/
    public function colors()
    {
        $bc = new besc_crud();
        $bc->table('color');
        $bc->primary_key('id');
        $bc->title('Color');
        $bc->unset_delete();
		
        $bc->columns(array
            (
                'name' => array
                (
                    'db_name' => 'name',
                    'type' => 'text',
                    'display_as' => 'Name',
                ),
    
                'hexcode' => array
                (
                    'db_name' => 'hexcode',
                    'type' => 'text',
                    'display_as' => 'Hex Colorcode',
                ),
    
            ));
         
        $data['crud_data'] = $bc->execute();
        $this->page('backend/crud', $data);
    }
    
    public function text_templates()
    {
        $bc = new besc_crud();
        $bc->table('text_templates');
        $bc->primary_key('id');
        $bc->title('Text Templates');
        $bc->unset_delete();
		
		
		$colors = array();
        foreach($this->b_model->getColors()->result() as $color)
        {
            $colors[] = array
            (
                'key' => $color->id,
                'value' => $color->name,
            );
        }
		
		
        $bc->columns(array
            (
                'name' => array
                (
                    'db_name' => 'name',
                    'type' => 'text',
                    'display_as' => 'Name',
                ),
    
                'margin_top' => array
                (
                    'db_name' => 'margin_top',
                    'type' => 'text',
                    'display_as' => 'Margin Top',
                ),
                
                 'margin_bottom' => array
                (
                    'db_name' => 'margin_bottom',
                    'type' => 'text',
                    'display_as' => 'Margin Bottom',
                ),
                
                 'font_size' => array
                (
                    'db_name' => 'font_size',
                    'type' => 'text',
                    'display_as' => 'Font Size',
                ),
                
                 'font_color' => array
                (
                    'db_name' => 'font_color',
                    'type' => 'select',
                    'display_as' => 'Font Color',
                    'options' => $colors,
                ),
    
            ));
         
        $data['crud_data'] = $bc->execute();
        $this->page('backend/crud', $data);
    }

    
    
    
    public function settings()
    {
        $bc = new besc_crud();
        $bc->table('config');
        $bc->primary_key('id');
        $bc->title('Settings');
        $bc->unset_delete();
        $bc->unset_add();
        
        $subsites = array();
        foreach($this->b_model->getSubsites()->result() as $sites)
        {
            $subsites[] = array
            (
                'key' => $sites->id,
                'value' => $sites->name,
            );
        }
    
        $bc->columns(array
            (
                'background_change_interval' => array
                (
                    'db_name' => 'background_change_interval',
                    'type' => 'text',
                    'col_info' => 'Change interval of the backgrounds in milliseconds (1000 = 1 second)',
                    'display_as' => 'Background interval',
                ),
    
                'impress_subsite' => array
                (
                    'db_name' => 'impress_subsite',
                    'type' => 'select',
                    'display_as' => 'Impress subsite',
                    'options' => $subsites,
                ),
                
                'contact_subsite' => array
                (
                    'db_name' => 'contact_subsite',
                    'type' => 'select',
                    'display_as' => 'Contact subsite',
                    'options' => $subsites,
                ),                
    
            ));
         
        $data['crud_data'] = $bc->execute();
        $this->page('backend/crud', $data);
    }

    public function save_blurred_image()
    {
        $fname_blur = $_POST['fname'];
        $fname_unblur = $_POST['old_filename'];
        $uploadpath = $_POST['uploadpath'];
        
        $img = file_get_contents('http://54.217.247.147/st_anna_blur/' . $fname_blur);
        file_put_contents(getcwd() . '/' . $uploadpath . $fname_unblur, $img);
        
        echo json_encode('success');
    }
    
    public function backgrounds()
    {
        $bc = new besc_crud();
        $bc->table('background');
        $bc->primary_key('id');
        $bc->title('Backgrounds');
        $bc->custom_actions(array
            (
                array
                (
                    'name' => 'Background ordering',
                    'icon' => site_url('items/backend/img/sorting_icon.png'),
                    'add_pk' => false,
                    'url' => 'background_ordering'
                )
            ));
        $bc->list_columns(array('name', 'text_content', 'bg_img', 'bg_img_blurred', 'img', 'news_article_id'));
        
        $articles = array();
        $articles[] = array(
            'key' => 0,
            'value' => 'no news article',  
        );
        foreach($this->b_model->getNewsArticles()->result() as $article)
        {
            $articles[] = array(
                'key' => $article->id,
                'value' => $article->headline,
            );
        }
         
        $bc->columns(array
            (
                'name' => array
                (
                    'db_name' => 'name',
                    'type' => 'text',
                    'display_as' => 'Name',
                ),
                 
                'text_content' => array
                (
                    'db_name' => 'text_content',
                    'type' => 'multiline',
                    'display_as' => 'Text content',
                    'height' => 90
                ),
                 
                'text_orientation_x' => array
                (
                    'db_name' => 'text_orientation_x',
                    'type' => 'select',
                    'display_as' => 'Horizontal orientation',
                    'options' => array
                    (
                        array
                        (
                            'key' => TEXT_ORIENTATION_LEFT,
                            'value' => 'left'
                        ),
                        array
                        (
                            'key' => TEXT_ORIENTATION_CENTER,
                            'value' => 'center'
                        ),
                        array
                        (
                            'key' => TEXT_ORIENTATION_RIGHT,
                            'value' => 'right'
                        )
                    ),
                ),
                 
                'text_orientation_y' => array
                (
                    'db_name' => 'text_orientation_y',
                    'type' => 'select',
                    'display_as' => 'Vertical orientation',
                    'options' => array
                    (
                        array
                        (
                            'key' => TEXT_ORIENTATION_TOP,
                            'value' => 'top'
                        ),
                        array
                        (
                            'key' => TEXT_ORIENTATION_MIDDLE,
                            'value' => 'middle'
                        ),
                        array
                        (
                            'key' => TEXT_ORIENTATION_BOTTOM,
                            'value' => 'bottom'
                        )
                    ),
                ),
    
                'video_embedd' => array
                (
                    'db_name' => 'video_embedd',
                    'type' => 'text',
                    'col_info' => 'only the video code, e.g. "https://www.youtube.com/watch?v=JfbFzVDy-w0" => "JfbFzVDy-w0"',
                    'display_as' => 'Headline video'
                ),
                
                'bg_img_blurred' => array
                (
                    'db_name' => 'bg_img_blurred',
                    'type' => 'image',
                    'display_as' => 'Blurred Background image',
                    'col_info' => 'Files: .png, .jpg, .jpeg<br>If selected take priority over Background image<br>This may take up to 20 seconds',
                    'accept' => '.png,.jpg,.jpeg',
                    'uploadpath' => 'items/general/uploads/backgrounds_blurred',
                    'js_callback_after_upload' => 'blur_image',
                ),
    
                'bg_img' => array
                (
                    'db_name' => 'bg_img',
                    'type' => 'image',
                    'display_as' => 'Background image',
                    'col_info' => 'Files: .png, .jpg, .jpeg',
                    'accept' => '.png,.jpg,.jpeg',
                    'uploadpath' => 'items/general/uploads/backgrounds',
                ),
    
                'img' => array
                (
                    'db_name' => 'img',
                    'type' => 'image',
                    'display_as' => 'Headline image',
                    'col_info' => 'Files: .png, .jpg, .jpeg',
                    'accept' => '.png,.jpg,.jpeg',
                    'uploadpath' => 'items/general/uploads/backgrounds_small'
                ),
                
                'news_article_id' => array
                (
                    'db_name' => 'news_article_id',
                    'type' => 'select',
                    'col_info' => 'Select the news article the text should link to',
                    'display_as' => 'News article',
                    'options' => $articles,
                ),
    
            ));
         
        $data['crud_data'] = $bc->execute();
        $this->page('backend/crud', $data);
    }
    
    public function background_ordering()
    {
        $data['backgrounds_active'] = $this->b_model->getBackgroundsActive();
        $data['backgrounds_available'] = $this->b_model->getBackgroundsAvailable();
        $data['site'] = 'backgrounds';
        $this->page('backend/background_ordering', $data);
    }
    
    public function save_background_ordering()
    {
        $content = json_decode(file_get_contents('php://input'));
        $col = array();
         
        $success = true;
        $message = "Backgrounds successfully changed.";
         
        if($this->b_model->resetBackgrounds())
        {
            foreach($content as $background)
            {
                $background_id = $background->background_id;
                $ordering = $background->ordering;
                 
                $backgrounds[] = array('id' => $background_id,
                    'ordering' => $ordering,
                    'modified_by' => $this->user->id,
                    'modified_date' => date('Y-m-d H:i:s'));
            }
             
            //echo print_r($backgrounds);
    
            if(!$this->b_model->saveBackgroundOrdering($backgrounds, 'id'))
            {
                $success = false;
                $message = "Error while saving backgrounds.";
            }
        }
        else
        {
            $success = false;
            $message = "Error while resetting Backgrounds.";
        }
         
        echo json_encode(array('success' => $success,
            'message' => $message));
    }    
    
    public function newsletter_emails()
    {
        $bc = new besc_crud();
        $bc->table('newsletter_email');
        $bc->primary_key('id');
        $bc->title('Newsletter E-Mail');
        $bc->unset_add();
        $bc->unset_edit();
    
        $bc->columns(array
            (
                'email' => array
                (
                    'db_name' => 'email',
                    'type' => 'text',
                    'display_as' => 'E-Mail',
                ),
            ));
         
        $data['crud_data'] = $bc->execute();
        $this->page('backend/crud', $data);
    }
    
	/************************************************************************************
	 * AUTH
	 ************************************************************************************/
	public function logged_in()
	{
		return (bool) $this->session->userdata('user_id');
	}	
	
}