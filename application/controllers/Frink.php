<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Frink extends MY_Controller 
{
    protected $besc_config = null;
    
    function __construct()
    {
        parent::__construct();
        
        date_default_timezone_set('Europe/Vienna');
        
        $this->config->load('st_anna');
        $this->lang->load('frontend', 'german');
        $this->load->model('Frontend_model', 'fm');
        
        $this->besc_config = $this->fm->getConfig($this->config->item('st_anna_config_id'))->row();
    }  


	/**********************************************************************************************************************************************************************************
	 * GENERAL
	 *********************************************************************************************************************************************************************************/
	
    public function index()
    {
        $data['news_articles'] = $this->news_articles();
		$data['slider_left'] = $this->fm->getSlider(1);
		$data['slider_center'] = $this->fm->getSlider(2);
		$data['slider_right'] = $this->fm->getSlider(3);
		
		foreach($data['slider_left'] as $key => $slide)
		{
		    $data['slider_left'][$key]['link'] = $slide['pretty_url'] == '0' ? $slide['external_link'] : site_url().$slide['pretty_url'];
		}
		foreach($data['slider_center'] as $key => $slide)
		{
		    $data['slider_center'][$key]['link'] = $slide['pretty_url'] == '0' ? $slide['external_link'] : site_url().$slide['pretty_url'];
		}
		foreach($data['slider_right'] as $key => $slide)
		{
		    $data['slider_right'][$key]['link'] = $slide['pretty_url'] == '0' ? $slide['external_link'] : site_url().$slide['pretty_url'];
		}
		
		
		$data['timers'] = $this->fm->getSliderTimers();
		
        $this->default_view('frontend/home', $data);
    }
    
    
    public function default_view($view, $viewdata)
    {
        $data = $this->header_info();
   
        $this->load->view('frontend/head', $data);
        $this->load->view('frontend/header', $data);
        $this->load->view($view, $viewdata);
        $this->load->view('frontend/footer', $data);
    }    
    
	protected function header_info()
	{
	    if(!$this->agent->is_mobile() && !$this->agent->is_mobile('ipad'))
	        $data['is_mobile'] = false;
	    else
	        $data['is_mobile'] = true;
	     
	    
	    $data['besc_config'] = $this->besc_config;
	  //  $data['submenus'] = $this->submenus();
	    
	    return $data;
	}
	
	protected function submenus()
	{
	    $submenus = array();
	    
	    $mainmenu_items = $this->fm->getMainmenuItems();
	    foreach($mainmenu_items->result() as $item)
	    {
	        switch($item->id)
	        {
	            case MAINMENU_HOME:
	            case MAINMENU_CONTACT:
	                break;
	            case MAINMENU_NEWS:
	                $metatags = array();
	                foreach($this->fm->getMetatags()->result() as $tag)
	                {
	                    $articles = $this->fm->getNewsArticlesByMetatagID($tag->id);
	                    if(count($this->filter_news_articles_by_date($articles)) > 0)
	                        $metatags[] = $tag;
	                }
	                
	                $submenus[$item->id]['items'] = $metatags;
	                $submenus[$item->id]['is_filter'] = true;
	                $submenus[$item->id]['count'] = count($metatags);
	                break;
	            case MAINMENU_ABOUT:
                case MAINMENU_RESEARCH:
	            case MAINMENU_DONATE:
	                $submenus[$item->id]['items'] = $this->fm->getSubmenuItemsByMainID($item->id)->result();
	                $submenus[$item->id]['is_filter'] = false;
	                $submenus[$item->id]['count'] = $this->fm->getSubmenuItemsByMainID($item->id)->num_rows();
	                break;
	        }
	        if($item->id != MAINMENU_HOME && $item->id != MAINMENU_CONTACT)
	        {
	            $submenus[$item->id]['img'] = $item->image;
	            $submenus[$item->id]['id'] = $item->id;
	            $submenus[$item->id]['subsite_id'] = $item->subsite_id;
	        }
	    }
	    
        return $submenus;	    
	}
	
	
		
	/**********************************************************************************************************************************************************************************
	 * GLOBAL CONTENT
	 *********************************************************************************************************************************************************************************/
	
	public function getColors()
	{
	    $colors = array();
	    foreach($this->fm->getColors()->result() as $color)
	    {
	        $colors[$color->id] = array
	        (
	            'id' => $color->id,
	            'name' => $color->name,
	            'hexcode' => $color->hexcode,
	        );
	    }
	     
	    return $colors;
	}
	
	public function getModules($parent_id, $parent_type)
	{
	    $moduletypes = $this->config->item('st_anna_module_types');
	    $modules = array();
	    $colors = $this->getColors();
	    foreach($moduletypes as $type => $table)
	    {
	        foreach($this->fm->getContentByModule($parent_id, $parent_type, $table)->result() as $module)
	        {
	            switch($type)
	            {
	                case 'text':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/text', array('module' => $module, 'colors' => $colors), true);
	                    break;
                    case 'image':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/image', array('module' => $module), true);
	                    break;
	                case 'gallery':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/gallery', array('module' => $module, 'images' => $this->fm->getGalleryModuleImages($module->id)->result()), true);
	                    break;
	                case 'download':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/download', array('module' => $module), true);
	                    break;
	                case 'bulletpoint':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/bulletpoint', array('module' => $module, 'colors' => $colors), true);
	                    break;
	                case 'link':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/links', array('module' => $module), true);
	                    break;
	                case 'button':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/button', array('module' => $module), true);
	                    break;
	                case 'impact':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/impact', array('module' => $module), true);
	                    break;
	                case 'video':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/video', array('module' => $module), true);
	                    break;
	                 case 'text_image':
	                    $modules[$module->ordering] = $this->load->view('frontend/modules/text_image', array('module' => $module, 'colors' => $colors), true);
	                    break;
	            }
	        }
	    }
	    ksort($modules);
	     
	    return $modules;
	}
	
	protected function sidebar($img, $link, $parent_type, $parent_id)
	{
	    $data['img'] = $img;
	    $data['link'] = $link;
	    $data['text_images'] = array();
	    
	    $moduletypes = $this->config->item('st_anna_module_types');
	    foreach($this->fm->getContentByModule($parent_id, $parent_type, $moduletypes['text'])->result() as $module)
	    {
	        if($module->right_side_img != '')
	        {
	            $data['text_images'][] = array(
	                'fname' => $module->right_side_img, 
	                'top' => $module->right_side_img_top,
	                'text' => $module->right_side_img_text,
	            );
	        }
	    }
	    
	    return $this->load->view('frontend/sidebar', $data, true);
	}
	
	
	/**********************************************************************************************************************************************************************************
	 * SUBSITES
	 *********************************************************************************************************************************************************************************/
	
	public function subsite($id)
	{
	    
        $mainId = $this->fm->getMainOfSubsite($id)->row()->main_id;
       $subsite_name =  $this->fm->getSubsiteByID($id)->row()->pretty_url;
        $main = $this->fm->getMainByID($mainId)->row();
        
        redirect('site/'.$main->name.'/'.$subsite_name);	
		
       // $this->default_view('frontend/subsite', $data);
	   
	}	
	
	
	public function site($name, $subname = null)
	{
	    
	    if(is_numeric($name))
	    {
		   $mainId = $this->fm->getMainOfSubsite($name)->row()->main_id;
         $subsite_name =  $this->fm->getSubsiteByID($id)->row()->pretty_url;
		   $main = $this->fm->getMainByID($mainId)->row();
		   
		   redirect('site/'.$main->name.'/'.$subsite_name);	
	    }
	    
	    if($name != 'Insights' && $name != 'most-read')
	    {
		   $main = $this->fm->getMainByName($name)->row();
		   $subsites = $this->fm->getSubsitesOfMain($main->id)->result();
		   $subsite_container = array();
        
	        foreach($subsites as $subsite)
	        {
	        	$subsite_elem = $this->fm->getSubsiteByID($subsite->subsite_id)->row();
		        $subsite_item = array('modules' => $this->getModules($subsite_elem->id, MODULE_PARENT_TYPE_SUBSITE),
		        					  'news' => $this->fm->getNewsArticlesBySubsite($subsite_elem->id)->result(),
		        					  'tags' => $this->fm->getTagsBySubsiteID($subsite_elem->id)->result(),
		        					  'name' => $subsite_elem->name,
		        					  'prettyurl' => $subsite_elem->prettyurl,
		        					  'subheader' => $this->fm->getSubsiteByID($subsite->subsite_id)->row()->sub_header);
		        					  
		        $subsite_container[] = $subsite_item;
	        }
        	$data['section'] = $subname;
			$data['site_name'] = $name;
			$data['subsites'] = $subsite_container;
		
			$this->default_view('frontend/subsite_template', $data); 
	    }
	    else if($name == 'most-read')
	    {
	    	// MOST READ SECTION
	    	
	    	
	    	
			$most_read_id = 12;
		    $subsite_container = array();
                
        	$subsite_elem = $this->fm->getSubsiteByID($most_read_id)->row();
	        $subsite_item = array('modules' => $this->getModules($subsite_elem->id, MODULE_PARENT_TYPE_SUBSITE),
	        					  'news' => $this->fm->getMostRead()->result(),
	        					  'tags' => $this->fm->getTagsBySubsiteID($subsite_elem->id)->result(),
	        					  'name' => $subsite_elem->name,
	        					  'prettyurl' => $subsite_elem->prettyurl,
	        					  'subheader' => $subsite_elem->sub_header);
	        					  
	        $subsite_container[] = $subsite_item;
	        
        	$data['section'] = $subname;
			$data['site_name'] = $name;
			$data['subsites'] = $subsite_container;
		
			$this->default_view('frontend/subsite_mostread', $data);

	    }
	    else
	    {
	    	$ids = array();
	    	
	    	$data['human_articles'] = array();
	    	$data['business_articles'] = array();
	    	$data['technology_articles'] = array();
	    	
			
			
			
	    	
	    	
	    	$human_item =  $this->fm->getFeaturedHuman()->row();
	    	
	    	if($human_item)
	    	{
		    	$human_item->tags = $this->fm->getTagsByArticleID($human_item->id)->result();
	    	}
	    		    	
	    	$data['human_featured'] = $human_item;
	    	
	    	$business_item =  $this->fm->getFeaturedBusiness()->row();
	    	
	    	if($business_item)
	    	{
		    	$business_item->tags = $this->fm->getTagsByArticleID($business_item->id)->result();
	    	}
	    	    	
	    	$data['business_featured'] = $business_item;
	    	
	    	$technology_item =  $this->fm->getFeaturedTechnology()->row();
	    	
	    	if($technology_item)
	    	{
		    	$technology_item->tags = $this->fm->getTagsByArticleID($technology_item->id)->result();
	    	}
	    	
			$data['technology_featured'] = $technology_item;
	    	

	    	$all_articles = $this->fm->getArticlesMagazine()->result();
	    	
	    	foreach($all_articles as $article)
	    	{
		    	
			    	
			    	$article_item = $article;
			    	$article_item->tags =  $this->fm->getTagsByArticleID($article->id)->result();
			    	
			    	
			    switch($article->magazine_article_type)
			    {
				    case 1: {	
					    		$data['human_articles'][] = $article_item;	
				    		};break;
				    
				    case 2: {
					    		$data['business_articles'][] = $article_item;
				    		};break;
				    
				    case 3: {
					    		$data['technology_articles'][] = $article_item;
				    		};break;
			    }
			    	
		    	
	    	}
	    	
	    	
			$data['site_name'] = $name;
			
			$data['sub_header'] = $this->fm->getSubsiteByID(5)->row()->sub_header;
			
			
			
	        $this->default_view('frontend/subsite_magazine', $data);

	    }
       	   
	}
	
	/**********************************************************************************************************************************************************************************
	 * NEWS ARTICLES 
	 *********************************************************************************************************************************************************************************/
	
	public function news($prettyurl)
	{
	    $prettyurl = urldecode($prettyurl);
	    $data['news'] = $this->fm->getNewsArticleByPrettyURL($prettyurl);
	   
	    if($data['news']->num_rows() == 1)
	    {
	    	$data['related'] = $this->fm->getNewsArticlesByMainArticleID($data['news']->row()->id)->result();
	        $data['news'] = $data['news']->row();
	        $data['modules'] = $this->getModules($data['news']->id, MODULE_PARENT_TYPE_NEWS);
	        $data['tags'] = $this->fm->getTagsByArticleID($data['news']->id)->result();
	        
	         
	        $this->default_view('frontend/news', $data);
	    }
	    else
	    {
	        show_error('article not found', 404);
	    }
	}	
	
	protected function news_articles()
	{
	    $data['news_articles'] = $this->filter_news_articles_by_date($this->fm->getNewsArticles());
	    return $data['news_articles'];
	}
	
	public function news_filtered($tag)
	{
	    $tag = urldecode($tag);
	    $metatag = $this->fm->getMetatagByName($tag);
	    if($metatag->num_rows() == 1)
	    {
	        $data['articles'] = $this->fm->getNewsArticlesByMetatagID($metatag->row()->id);
	        $data['articles'] = $this->filter_news_articles_by_date($data['articles']);
		
	        $this->default_view('frontend/news_filtered', $data);
	    }
	    else 
	    {
	        $this->load->view('errors/html/error_general', array('heading' => 'error', 'message' => 'metatag not found'));
	    }
	}
	
	protected function filter_news_articles_by_date($articles)
	{
	    $filtered_news = array();
	    $today = strtotime(date('Y-m-d'));
	    foreach($articles->result_array() as $article)
	    {
	        $start_pass = true;
	    /*    if($article['timed_start'] != "" && $article['timed_start'] != 1 && $article['timed_start'] != "0000-00-00 00:00:00" && $article['timed_start'] != null && $article['timed_start'] != '1970-01-01 01:00:00')
	        {
	            $startdate = strtotime($article['timed_start']);
	            if($today < $startdate)
	                $start_pass = false;
	        }
	        */
	        $end_pass = true;
	       /* if($article['timed_end'] != "" && $article['timed_end'] != 1 && $article['timed_end'] != "0000-00-00 00:00:00" && $article['timed_end'] != null && $article['timed_start'] != '1970-01-01 01:00:00')
	        {
	            $enddate = strtotime($article['timed_end']);
	            if($today < $enddate)
	                $end_pass = false;
	        }
	        	 */       
	        if($start_pass && $end_pass)
	            $filtered_news[] = $article;
	    }
	    
	    return $filtered_news;
	}
	
	/**********************************************************************************************************************************************************************************
	 * DONATIONS
	 *********************************************************************************************************************************************************************************/
	
	public function donate($donation_type, $donation_amount)
	{
	    $data['donation_type'] = $donation_type;
	    $data['donation_amount'] = $donation_amount;
	    $data['countries'] = $this->fm->getCountries();
	    $this->default_view('frontend/donate', $data);
	}	
	
	public function toyshop()
	{
	    $data['countries'] = $this->fm->getCountries();
	    $this->default_view('frontend/toyshop', $data);
	}	
	
	/**********************************************************************************************************************************************************************************
	 * NEWSLETTER
	 *********************************************************************************************************************************************************************************/
	public function save_newsletter_email()
	{
	    $email = $_POST['email'];
	    $this->fm->insertNewsletterEmail($email);
	}
	
	/**********************************************************************************************************************************************************************************
	 * SEARCH
	 *********************************************************************************************************************************************************************************/
	public function search($tag = null)
	{
		$data['tags'] = $this->fm->getMetatags()->result();
		$articles = $this->fm->getNewsArticlesSearch()->result();
		
		$data['haveTag'] = "false";
		
		$data['articles'] = array();
		
		if($tag !== null)
		{
			$data['haveTag'] = $tag;
		}
		
		
		foreach($articles as $article)
		{
			$article_tags = $this->fm->getTagsByArticleID($article->id)->result();
			$article_item = array('id' => $article->id,
								  'headline' => $article->headline,
								  'prettyurl' => $article->prettyurl,
								  'section_id' => $article->section_id,
								  'img' => $article->teaser_img,
								  'text' => $article->teaser_text,
								  'tags' => $article_tags);
			
			$data['articles'][] = $article_item;
		}
		
        $this->default_view('frontend/search', $data);
	}
	
	
	/**********************************************************************************************************************************************************************************
	 * IMPACTS
	 *********************************************************************************************************************************************************************************/
	public function impact($impact)
	{
	
		$all_articles = $this->fm->getArticlesMagazineImpact()->result();
	    
	    $data['articles'] = array();
	    
	    	
    	foreach($all_articles as $article)
    	{
	    		    	
	    	$article_item = $article;
	    	$article_item->tags =  $this->fm->getTagsByArticleID($article->id)->result();
		    	
		    
		    switch($article->magazine_article_type)
		    {
			    case 1: {	
				    		$data['human_articles'][] = $article_item;	
			    		};break;
			    
			    case 2: {
				    		$data['business_articles'][] = $article_item;
			    		};break;
			    
			    case 3: {
				    		$data['technology_articles'][] = $article_item;
			    		};break;
		    }
		    	
	    	
    	}
		
		
		switch($impact)
		{
			case "human": {						
							$data['articles'] = $data['human_articles'];
							$data['title'] = "HUMAN IMPACT";
						};break;
						
						
			case "business": {						
							$data['articles'] = $data['business_articles'];
							$data['title'] = "BUSINESS IMPACT";
						};break;
						
						
			case "technology": {						
							$data['articles'] = $data['technology_articles'];
							$data['title'] = "TECHNOLOGY IMPACT";
						};break;
			default: {
				redirect('');
			}			
		}
		
	
        $this->default_view('frontend/impact', $data);
	}
	
	
}








