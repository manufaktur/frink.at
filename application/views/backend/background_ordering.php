

<div id="content">

    <div class="content_h1">Background sorting</div>
    <div class="content_h2">Use drag and drop to select which backgrounds should be shown and in which order.</div>

	<div id="backgrounds_available">
        <div class="content_h3">Available</div>
        <ul class="backgrounds_sortable backgrounds_available_container">
            <?php foreach($backgrounds_available->result() as $background):?>
                <li class="backgrounds" background_id="<?= $background->id?>">
                    <div class="background_remove"></div>
                    <?= $background->name?>
                </li>
            <?php endforeach;?>
            <li class="no_articles_available">
                All articles selected
            </li>
        </ul>
	</div>
	
	<div id="backgrounds_active">
        <div class="content_h3">Active</div>
        <ul class="backgrounds_sortable backgrounds_active_container">
            <?php foreach($backgrounds_active->result() as $background):?>
                <li class="backgrounds" background_id="<?= $background->id?>">
                    <div class="background_remove"></div>
                    <?= $background->name?>
                </li>
            <?php endforeach;?>
        </ul>
	</div>
	
	<br clear="both" />
	
	<div class="custom_actions">
        <ul>
            <li class="backgrounds_save">Save</li>
            <li><a href="<?= site_url('backend/backgrounds')?>">Cancel</a></li>
        </ul>
	</div>

</div>