

<link rel="stylesheet" type="text/css" href="<?=site_url("items/backend/css/edit_content.css"); ?>">
<link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/modules.css"); ?>">
<link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/fonts.css"); ?>">

<script type="text/javascript" src="<?=site_url("items/backend/js/imagesloaded.pkgd.min.js"); ?>"></script>
<script type="text/javascript" src="<?=site_url("items/backend/js/edit_content.js"); ?>"></script>
<script type="text/javascript" src="<?=site_url("items/backend/js/text_module.js"); ?>"></script>
<script type="text/javascript" src="<?=site_url("items/backend/js/image_module.js"); ?>"></script>
<script type="text/javascript" src="<?=site_url("items/backend/js/button_module.js"); ?>"></script>
<script type="text/javascript" src="<?=site_url("items/backend/js/impact_module.js"); ?>"></script>
<script type="text/javascript" src="<?=site_url("items/backend/js/gallery_module.js"); ?>"></script>
<script type="text/javascript" src="<?=site_url("items/backend/js/download_module.js"); ?>"></script>
<script type="text/javascript" src="<?=site_url("items/backend/js/bulletpoint_module.js"); ?>"></script>
<script type="text/javascript" src="<?=site_url("items/backend/js/video_module.js"); ?>"></script>
<script type="text/javascript" src="<?=site_url("items/backend/js/text_image_module.js"); ?>"></script>


<script>
    var parent_id = "<?= $parent_id?>";
    var parent_type = "<?= $parent_type?>";
    var existing_modules = <?= $modules?>;
    var colors = new Array();
    var subsites = new Array();
    var targets = new Array();
    var text_templates = new Array();
    var gallery_items = new Array();
    var text_align = new Array();
    text_align[<?= TEXT_ORIENTATION_LEFT ?>] = 'Left';
    text_align[<?= TEXT_ORIENTATION_CENTER ?>] = 'Center';
    text_align[<?= TEXT_ORIENTATION_RIGHT ?>] = 'Right';
    text_align[<?= TEXT_ORIENTATION_JUSTIFY ?>] = 'Justify';
    text_templates.push({id:0, margin_top:0, margin_bottom:0, font_size:15, font_color:0, name:"Select a template..."});
    subsites.push({id:0, prettyurl:"", name:"Add my own url"});
    targets.push({id:0, value:"_self", name:"Same Window"});
    targets.push({id:1, value:"_blank", name:"New Window"});
    <?php foreach($colors->result() as $color):?>colors.push({'id': <?= $color->id?>, 'hexcode': '<?= $color->hexcode?>', 'name': '<?= $color->name?>'}); <?php endforeach;?>
    <?php foreach($subsites->result() as $subsite):?>subsites.push({'id': <?= $subsite->id?>, 'prettyurl': '<?= $subsite->prettyurl?>', 'name': '<?= $subsite->name?>'}); <?php endforeach;?>
    <?php foreach($text_templates->result() as $template):?>text_templates.push({'id': <?= $template->id?>, 'margin_top': '<?= $template->margin_top?>', 'margin_bottom': '<?= $template->margin_bottom?>', 'font_size': '<?= $template->font_size?>', 'font_color': '<?= $template->font_color?>' , 'name': '<?= $template->name?>'}); <?php endforeach;?>
</script>

<div id="content">

    <div class="content_h1"><?= $header?></div>
    <div class="content_name"><?= $name?></div>
    <div class="content_h2"></div>
    
    <div id="content_edit">
        <div id="content_data">
            <div class="content_h3">Toolbox</div>
            <div id="module_list">
                <div id="module_text" type="text" class="module_item droppable">TEXT</div>
                <div id="module_image" type="image" class="module_item droppable">IMAGE</div>
                <div id="module_impact" type="impact" class="module_item droppable">IMPACT</div>
                <div id="module_button" type="button" class="module_item droppable">BUTTON</div>
                <div id="module_bulletpoint" type="bulletpoint" class="module_item droppable">BULLETPOINTS</div>
                <div id="module_gallery" type="gallery" class="module_item droppable">IMAGE GALLERY</div> 
                <div id="module_video" type="video" class="module_item droppable">VIMEO EMBED</div>
                <div id="module_text_image" type="text_image" class="module_item droppable">IMAGE + TEXT</div>
              <!--  <div id="module_download" type="download" class="module_item droppable">PDF DOWNLOAD</div> 
                 -->
            </div>
            
            <div class="content_h3">Properties</div>
            <div id="module_properties">
            
            </div>
            
            <div class="custom_actions">
            <ul>
                <li class="edit_content_save">Save</li>
                <li><a href="<?= site_url('backend/subsites')?>">Cancel</a></li>
            </ul>
	</div>
        </div>
        
        <div id="content_container">
            <div class="content_h3">Content</div>
            <div id="article_content">
        </div>
        
        </div>
    </div>
    
    <div id="hidden_elements">
 	
	</div>
    
</div>