

<div id="content">

    <div class="content_h1">Frontpage articles</div>
    <div class="content_h2">Use drag and drop to select which articles should be shown and in which order.</div>

	<div id="articles_available">
        <div class="content_h3">Available</div>
        <ul class="articles_sortable">
            <?php foreach($articles_available->result() as $article):?>
                <li article_id="<?= $article->id?>" class="article_total">
                    <div class="article_remove"></div>
                    <div class="article_available"><?= $article->headline?></div>
                    <div class="article_active">
                        <div class="article_active_headline"><?= $article->headline?></div>
                        <img class="article_active_image" src="<?= site_url('items/general/uploads/article_teaser/' . $article->teaser_img)?>" />
                    </div>
                </li>
            <?php endforeach;?>
                
            <li class="no_articles_available">
                All articles selected
            </li>
        </ul>
	</div>
	
	<div id="articles_active">
        <div class="content_h3">Active</div>
        <div id="articles_active_container">
            <ul class="articles_sortable">
                <?php foreach($articles_active->result() as $article):?>
                    <li article_id="<?= $article->id?>" class="article_total">
                        <div class="article_remove"></div>
                        <div class="article_available"><?= $article->headline?></div>
                        <div class="article_active">
                            <div class="article_active_headline"><?= $article->headline?></div>
                            <img class="article_active_image" src="<?= site_url('items/general/uploads/article_teaser/' . $article->teaser_img)?>" />
                        </div>
                    </li>
                <?php endforeach;?>
            </ul>
        </div>
        
	</div>
	
	<br clear="both" />
	
	<div class="custom_actions">
        <ul>
            <li class="frontpage_articles_save">Save</li>
            <li><a href="<?= site_url('backend/backgrounds')?>">Cancel</a></li>
        </ul>
	</div>

</div>