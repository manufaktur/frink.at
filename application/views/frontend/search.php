<script>
	
$(document).ready(function()
{	
	var tag = "<?= $haveTag?>";
	
	if(tag != "false")
	{
		igniteIsotopeTeaser(tag);
	}
	else
	{
		igniteIsotopeTeaser(false);
	}
});
</script>
		<link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/modules.css"); ?>">
		
		<div class="site_info_holder">
			<a style="color:#a9a9a9;text-decoration:none;" href="<?= site_url();?>">HOME</a> 
			<span style="font-size:20px;"> » </span>
			<a style="color:#a9a9a9;text-decoration:none;text-transform:uppercase;" href="<?= site_url();?>">SEARCH</a> 
		</div>
		<div id="tag_holder">
				Tags:
				<? foreach($tags as $tag):?>
					<span class="search_tag_item" tag="<?= $tag->name;?>"><?= $tag->display_name;?></span>&nbsp;
				<? endforeach;?>
			</div>
		<div id="isotopeHolder">
			<? foreach($articles as $article):?>
					 <? $link = $article['prettyurl'];
						
					 if($article['section_id'] != "" && $article['section_id'] !== NULL)
					 {
						 $link = $article['prettyurl']."#".$article['section_id'];
					 }
						
						
					?>
					<div class="teaser_item <? foreach($article['tags'] as $tag){ echo $tag->name." ";}?>" >
						<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$link)?>">
							<div class="article_teaser_img_holder">
								<img class="article_teaser_img" src="<?= site_url('items/general/uploads/article_teaser/'.$article['img'])?>"/>
							</div>
						</a>
						
						<div class="article_teaser_headline"><?= $article['headline'];?></div>					
						<div class="article_teaser_text"><?= $article['text'];?></div>
						<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$link)?>">
							<div class="article_teaser_more">> read more <</div>
						</a>
					</div>
				
			<? endforeach;?>
		</div>
		
		
		
		
		