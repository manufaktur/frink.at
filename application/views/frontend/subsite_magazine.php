
		<link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/modules.css"); ?>">
		
		<div class="site_info_holder">
			<a style="color:#a9a9a9;text-decoration:none;" href="<?= site_url();?>">HOME</a> 
			<span style="font-size:20px;"> » </span>
			<a style="color:#a9a9a9;text-decoration:none;text-transform:uppercase;" href="<?= site_url();?>"><?= $site_name;?></a> 
		</div>
		<div id="article_title">FRINK MAGAZINE</div>
		<div id="article_sub_title" style="margin-bottom:5px;"><?= $sub_header;?></div>
		<div id="featured_holder">
		
		
		
			
	<? if($business_featured):?>
			<div class="featured_item" >
				<div class="magazine_item_title">BUSINESS FACTOR</div>
				<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$business_featured->prettyurl)?>">
					<div class="featured_teaser_img_holder">
						<img class="article_teaser_img" src="<?= site_url('items/general/uploads/article_teaser/'.$business_featured->featured_magazine_image)?>"/>
					</div>
				</a>
				<div class="featured_teaser_headline"><?= nl2br($business_featured->headline);?></div>					
				<div class="featured_teaser_text"><?= $business_featured->featured_magazine_text;?></div>
				<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$business_featured->prettyurl)?>">
					<div class="featured_teaser_more">> read more <</div>
				</a>
				
				<div class="featured_tag_holder">
					Tags:
					<? foreach($business_featured->tags as $tag):?>
						<a href="<?= site_url('search/'.$tag->name);?>"><span class="tag_item"><?= $tag->display_name;?></span></a>&nbsp;
					<? endforeach;?>
				</div>				
			</div>
	<? endif;?>			
	
	<? if($human_featured):?>
			<div class="featured_item" >
				<div class="magazine_item_title">HUMAN FACTOR</div>
				<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$human_featured->prettyurl)?>">
					<div class="featured_teaser_img_holder">
						<img class="article_teaser_img" src="<?= site_url('items/general/uploads/article_teaser/'.$human_featured->featured_magazine_image)?>"/>
					</div>
				</a>
				<div class="featured_teaser_headline"><?= nl2br($human_featured->headline);?></div>					
				<div class="featured_teaser_text"><?= $human_featured->featured_magazine_text;?></div>
				<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$human_featured->prettyurl)?>">
					<div class="featured_teaser_more">> read more <</div>
				</a>
				
				<div class="featured_tag_holder">
					Tags:
					<? foreach($human_featured->tags as $tag):?>
						<a href="<?= site_url('search/'.$tag->name);?>"><span class="tag_item"><?= $tag->display_name;?></span></a>&nbsp;
					<? endforeach;?>
				</div>				
			</div>
		<? endif;?>	
		
	<? if($technology_featured):?>		
			<div class="featured_item" >
				<div class="magazine_item_title">TECHNOLOGY FACTOR</div>
				<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$technology_featured->prettyurl)?>">
					<div class="featured_teaser_img_holder">
						<img class="article_teaser_img" src="<?= site_url('items/general/uploads/article_teaser/'.$technology_featured->featured_magazine_image)?>"/>
					</div>
				</a>
				<div class="featured_teaser_headline"><?= nl2br($technology_featured->headline);?></div>					
				<div class="featured_teaser_text"><?= $technology_featured->featured_magazine_text;?></div>
				<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$technology_featured->prettyurl)?>">
					<div class="featured_teaser_more">> read more <</div>
				</a>
				
				<div class="featured_tag_holder">
					Tags:
					<? foreach($technology_featured->tags as $tag):?>
						<a href="<?= site_url('search/'.$tag->name);?>"><span class="tag_item"><?= $tag->display_name;?></span></a>&nbsp;
					<? endforeach;?>
				</div>				
			</div>
	<? endif;?>		
		</div>
		
		<div id="teaser_holder">
			
			
			<div class="teaser_column">
				<? foreach($business_articles as $article):?>
				
					<div class="teaser_item" >
						<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$article->prettyurl)?>">
							<div class="article_teaser_img_holder">
								<img class="article_teaser_img" src="<?= site_url('items/general/uploads/article_teaser/'.$article->teaser_img)?>"/>
							</div>
						</a>
						<div class="article_teaser_headline"><?= nl2br($article->headline);?></div>					
						<div class="article_teaser_text"><?= $article->teaser_text;?></div>
						<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$article->prettyurl)?>">
							<div class="article_teaser_more">> read more <</div>
						</a>
					</div>
				
				<? endforeach;?>
				<a style="text-decoration:none;color:#000000;" href="<?= site_url('impact/business')?>">
					<div class="impact_button">
						SEE ALL ARTICLES FOR<br/> "BUSINESS IMPACT"
					</div>
				</a>
			</div>
			
			<div class="teaser_column">
				<? foreach($human_articles as $article):?>
				
					<div class="teaser_item" >
						<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$article->prettyurl)?>">
							<div class="article_teaser_img_holder">
								<img class="article_teaser_img" src="<?= site_url('items/general/uploads/article_teaser/'.$article->teaser_img)?>"/>
							</div>
						</a>
						<div class="article_teaser_headline"><?= nl2br($article->headline);?></div>					
						<div class="article_teaser_text"><?= $article->teaser_text;?></div>
						<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$article->prettyurl)?>">
							<div class="article_teaser_more">> read more <</div>
						</a>
					</div>
				
				<? endforeach;?>
				<a style="text-decoration:none;color:#000000;" href="<?= site_url('impact/human')?>">
					<div class="impact_button">
						SEE ALL ARTICLES FOR<br/> "HUMAN IMPACT"
					</div>
				</a>
			</div>
			
			<div class="teaser_column">
				<? foreach($technology_articles as $article):?>
				
					<div class="teaser_item" >
						<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$article->prettyurl)?>">
							<div class="article_teaser_img_holder">
								<img class="article_teaser_img" src="<?= site_url('items/general/uploads/article_teaser/'.$article->teaser_img)?>"/>
							</div>
						</a>
						<div class="article_teaser_headline"><?= nl2br($article->headline);?></div>					
						<div class="article_teaser_text"><?= $article->teaser_text;?></div>
						<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$article->prettyurl)?>">
							<div class="article_teaser_more">> read more <</div>
						</a>
					</div>
				
				<? endforeach;?>
				<a style="text-decoration:none;color:#000000;" href="<?= site_url('impact/technology')?>">
					<div class="impact_button">
						SEE ALL ARTICLES FOR<br/> "TECHNOLOGY IMPACT"
					</div>
				</a>
			</div>
		</div>
		 
<script>
	
</script>