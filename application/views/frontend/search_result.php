

        <script>
            var ignite_backgrounds = 0;
        </script>


        <div id="container" class="containerborder containerwidth filtered_articles">
            
             
            <?php foreach($results['article'] as $result):?>
                <div class="article_preview">
                    <div class="article_preview_headline"><a href="<?= site_url('news/' . $result['prettyurl'])?>"><?= $result['headline']?></a></div>
                    <div class="article_preview_dataline">
                        <div class="article_preview_date"><?= date('d.m.Y', strtotime($result['created_date']))?></div>
                    </div>
                    <div class="article_preview_contentline">
                        <a href="<?= site_url('news/' . $result['prettyurl'])?>">
                            <div class="article_preview_image">
                                <img src="<?= site_url('items/general/uploads/article_teaser/' . $result['teaser_img'])?>" />
                            </div>
                        </a>
                        <div class="article_preview_text">
                            <?= $result['teaser_text'] ?>
                            <div class="article_preview_readmore">
                                <a href="<?= site_url('news/' . $result['prettyurl'])?>"><?= $this->lang->line('newsarticle_readmore')?></a>
                            </div>
                        </div> 
                    </div>                        
                </div>
            <?php endforeach;?>
            
            <?php foreach($results['subsite'] as $result):?>
                    <div class="subsite_preview">
                        <div class="article_preview_headline"><a href="<?= site_url('subsite/' . $result['id'])?>"><?= $result['name']?></a></div>
                        <div class="article_preview_contentline">
                            <div class="subsite_preview_text">
                                <?= $result['teaser_text'] ?>
                                <div class="article_preview_readmore">
                                    <a href="<?= site_url('subsite/' . $result['id'])?>"><?= $this->lang->line('newsarticle_readmore')?></a>
                                </div>
                            </div> 
                        </div>                        
                    </div>
            <?php endforeach;?>
        </div>