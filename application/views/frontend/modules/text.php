<?
	
	$alignments = array('Left', 'Center', 'Right', 'Justify');
?>

        <div class="module module_text" style="top: <?= $module->top_pos?>px; left: <?= $module->left_pos?>px; color: <?= $colors[$module->font_color]['hexcode']?>; font-size: <?= $module->font_size?>px;text-align:<?= $alignments[$module->align] ?>">
            <?= auto_link(nl2br($module->content), 'email')?>         
        </div>