

        <div class="module module_gallery" style="top:<?= $module->top_pos?>px;left:<?= $module->left_pos?>px;margin-top: <?= $module->margin_top?>px; margin-bottom: <?= $module->margin_bottom?>px;">
            <? foreach($images as $image):?>
            	<div class="gallery_item_holder">
	            	<a href="<?= site_url('items/general/uploads/content_gallery_img/' . $image->fname)?>" data-title="<?= $image->text;?>" data-lightbox="gallery_<?= $module->id;?>">
	            	<!--	<img class="gallery_image" src="<?= site_url('items/general/uploads/content_gallery_img/' . $image->fname)?>" /> -->
	            		<div class="gallery_image" style="background-image: url('<?= site_url('items/general/uploads/content_gallery_img/' . $image->fname)?>');" ></div>
	            	</a>
	            	<div class="gallery_image_text"><?= nl2br($image->text)?></div>
            	</div>
            <? endforeach;?>
            
               
        </div>