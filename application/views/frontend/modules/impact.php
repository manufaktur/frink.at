<?	
	$alignments = array('Left', 'Center', 'Right', 'Justify');
?>

        <div class="module module_impact" style="top: <?= $module->top_pos?>px; left: <?= $module->left_pos?>px;">
           <div class="module_impact_left impact_item" >
           		<? if($module->left_image !== NULL):?>
					<img class="impact_left_image impact_image" src="<?= site_url('items/general/uploads/impact_images').'/'.$module->left_image;?>" />
				<? endif;?>
				<div class="impact_left_title impact_title"><?= $module->left_title?></div>
				<div class="impact_content_left impact_content" style="color: <?= $colors[$module->left_color]['hexcode']?>;text-align:<?= $alignments[$module->left_align] ?>"><?= $module->left_content?></div>
			</div>
			<div class="module_impact_mid impact_item" >
				<? if($module->middle_image !== NULL):?>
					<img class="impact_middle_image impact_image" src="<?= site_url('items/general/uploads/impact_images/').'/'.$module->middle_image;?>" />
				<? endif;?>
				<div class="impact_middle_title impact_title"><?= $module->middle_title?></div>
				<div class="impact_content_middle impact_content" style="color: <?= $colors[$module->middle_color]['hexcode']?>;text-align:<?= $alignments[$module->middle_align] ?>"><?= $module->middle_content?></div>
			</div>
			<div class="module_impact_right impact_item" >
				<? if($module->right_image !== NULL):?>
					<img class="impact_right_image impact_image" src="<?= site_url('items/general/uploads/impact_images/').'/'.$module->right_image;?>" />
				<? endif;?>
				<div class="impact_right_title impact_title"><?= $module->right_title?></div>
				<div class="impact_content_right impact_content" style="color: <?= $colors[$module->right_color]['hexcode']?>;text-align:<?= $alignments[$module->right_align] ?>"><?= $module->right_content?></div>
			</div>       
        </div>