<?
	
	$alignments = array('Left', 'Center', 'Right', 'Justify');
?>

        <div class="module module_text_image" style="top: <?= $module->top_pos?>px; left: <?= $module->left_pos?>px; color: <?= $colors[$module->font_color]['hexcode']?>; font-size: <?= $module->font_size?>px;text-align:<?= $alignments[$module->align] ?>">
        	 <a href="<?= site_url('items/general/uploads/images/' . $module->fname)?>" data-lightbox="gallery_<?= $module->id;?>">
        	 	<img src="<?= site_url('items/general/uploads/images/'.$module->fname)?>" />
        	 </a>
            <?= auto_link(nl2br($module->content), 'email')?>         
        </div>