
			<div id="menu_holder">
        		<div class="menu_item" style="margin-left: 20px;">
        			<a style="color:#000;text-decoration:none;" href="<?= site_url('site/Work')?>"><div class="menu_item_title">Work</div></a>
        			<a style="text-decoration:none;" href="<?= site_url('site/Work/services')?>"><div class="menu_sub_item">Services</div></a>
        			<a style="text-decoration:none;" href="<?= site_url('site/Work/solutions')?>"><div class="menu_sub_item">Solutions</div></a>
        		</div>
        		<div class="menu_item"  style="margin-left: 9px;">
        		<a style="color:#000;text-decoration:none;" href="<?= site_url('site/Expertise')?>">	<div class="menu_item_title">Expertise</div></a>
        			<a style="text-decoration:none;" href="<?= site_url('site/Expertise/our-approach')?>"><div class="menu_sub_item">Our approach</div></a>
        			<a style="text-decoration:none;" href="<?= site_url('site/Expertise/case-studies')?>"><div class="menu_sub_item">Case studies</div></a>
        		</div>
        		<div class="menu_item" style="margin-left: 31px;">
        			<a style="color:#000;text-decoration:none;" href="<?= site_url('site/Insights')?>"><div class="menu_item_title">Insights</div></a>
        			<a style="text-decoration:none;" href="<?= site_url('site/Insights')?>"><div class="menu_sub_item">Frink magazine</div></a>
        			<a style="text-decoration:none;" href="<?= site_url('site/most-read')?>"><div class="menu_sub_item">Most read</div></a>
        			
        		</div>
        		<div class="menu_item" style="margin-left: 33px;">
        			<a style="color:#000;text-decoration:none;" href="<?= site_url('site/People')?>"><div class="menu_item_title">People</div></a>
        			<a style="text-decoration:none;" href="<?= site_url('site/People/our-vision')?>"><div class="menu_sub_item">Our vision</div></a>
        			<a style="text-decoration:none;" href="<?= site_url('site/People/your-career')?>"><div class="menu_sub_item">Your career</div></a>
        		</div>
        		<div class="menu_item" style="margin-left: 28px;">
        			<a style="color:#000;text-decoration:none;" href="<?= site_url('site/About')?>"><div class="menu_item_title">About</div></a>
        			<a style="text-decoration:none;" href="<?= site_url('site/About/our-story')?>"><div class="menu_sub_item">Our story</div></a>
        			<a style="text-decoration:none;" href="<?= site_url('site/About/our-team')?>"><div class="menu_sub_item">Our team</div></a>
        		</div>
        		<div class="menu_item" style="margin-left: 9px;">
        			<a style="color:#000;text-decoration:none;" href="<?= site_url('site/Contact')?>"><div class="menu_item_title">Contact</div></a>
        			<a style="text-decoration:none;" href="<?= site_url('site/Contact/meet-us')?>"><div class="menu_sub_item">Meet us</div></a>
        			<a style="text-decoration:none;" href="<?= site_url('site/Contact/imprint')?>"><div class="menu_sub_item">Imprint</div></a>
        		</div>
        	</div>
        	
            <div id="social_holder">
             	<a style="text-decoration:none;" target="_blank" href="https://www.instagram.com/frink_advanced_services/">
            		<div class="social_item" style="margin-right: 17px;">
            			<img style="width:100%;" src="<?= site_url('items/frontend/img/instagram.png')?>" />
            		</div>
            	</a>
            	
            	 <a style="text-decoration:none;" target="_blank" href="https://twitter.com/Frink_AS">
            		 <div class="social_item">
            			 <img style="width:100%;" src="<?= site_url('items/frontend/img/twitter.png')?>" />
            		 </div>
            	 </a>
            	 
            	 <a style="text-decoration:none;" target="_blank" href="https://www.linkedin.com/company/frink-advanced-services-gmbh">
            	 	<div class="social_item">
            			<img style="width:100%;" src="<?= site_url('items/frontend/img/linkedin.png')?>" />
            		</div>
                </a>
            	 
            	<!-- <div class="social_item" style="margin-left:0px;">
            			 <img style="width:100%;" src="<?= site_url('items/frontend/img/facebook.png')?>" />
            		 </div>
            	 -->
            </div>
			<div id="footer">

			</div>
		</div>
		<script type="text/javascript" src="<?=site_url("items/frontend/js/lightbox.js"); ?>"></script>
	</body>
</html>