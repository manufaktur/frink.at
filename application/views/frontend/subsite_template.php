
		<link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/modules.css"); ?>">
		
		<div class="site_info_holder">
			<a style="color:#a9a9a9;text-decoration:none;" href="<?= site_url();?>">HOME</a> 
			<span style="font-size:20px;"> » </span>
			<a style="color:#a9a9a9;text-decoration:none;text-transform:uppercase;" href="<?= site_url();?>"><?= $site_name;?></a> 
		</div>
		<?	$x = 1; foreach($subsites as $subsite):?>
			
			<div id="article_title" section="<?= $subsite['prettyurl'];?>"><?= $subsite['name'];?></div>
			<div id="article_sub_title" style="margin-bottom:5px;"><?= $subsite['subheader'];?></div>
			<div id="tag_holder">
				Tags:
				<? foreach($subsite['tags'] as $tag):?>
					<a style="color:#888888;" href="<?= site_url('search/'.$tag->name);?>"><span class="tag_item"><?= $tag->display_name;?></span></a>&nbsp;
				<? endforeach;?>
			</div>
			<div class="article">
	            <div class="article_content_subsite">
	                <?php foreach($subsite['modules'] as $module):?>
	                    <?= $module?>
	                <?php endforeach;?>
	            </div>
	            
	            <? if($subsite['name'] == "OUR TEAM"):?>
					<img style="width:100%" src="<?= site_url('items/frontend/img/team.jpeg')?>">
				<? endif;?>
	       </div>  
	       
	       
	       
	       <div class="articles_holder_isotope" id="isotopeHolder<?= $x;?>">
			<? foreach($subsite['news'] as $article):?>
				   <? $link = $article->prettyurl;
						
					 if($article->section_id != "" && $article->section_id !== NULL)
					 {
						 $link = $article->prettyurl."#".$article->section_id;
					 }
						
						
					?>
					<div class="teaser_item" >
						<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$link)?>">
							<div class="article_teaser_img_holder">
								<img class="article_teaser_img" src="<?= site_url('items/general/uploads/article_teaser/'.$article->teaser_img)?>"/>
							</div>
						</a>
						<div class="article_teaser_headline"><?= nl2br($article->headline);?></div>					
						<div class="article_teaser_text"><?= $article->teaser_text;?></div>
						<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$link)?>">
							<div class="article_teaser_more">> read more <</div>
						</a>
					</div>
				
			<? endforeach;?>
		</div>
	       <br/>
	      
            			
		<? $x++; endforeach; ?>
		
<script>
	$(document).ready(function()
	{	
		var sumIsotope = <?= count($subsites)?>;
		igniteIsotopeTeaserSubsite(sumIsotope);
		
		var section = "<?= $section?>";
		
		var element = $('#article_title[section="'+section+'"]');
		scrollToElement(element);
	});
</script>