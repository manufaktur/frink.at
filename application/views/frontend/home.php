<script>
$(document).ready(function()
{	
	initSlick(<?= $timers->left_slider;?>,<?= $timers->center_slider;?>,<?= $timers->right_slider;?>);	
});


</script>

<div id="content">
	<div id="description_text">
		 <span class="semibold">FRINK ADVANCED SERVICES.</span> 
		 <span class="light">Collaboration designed for humans.</span>
	</div>
	
	<div class="top_border"></div>
	<div id="mid_holder" style="border: none; padding: 0px;">
		<?php for($i = 0 ; $i < count($slider_left) ; $i++):?>
    		<div class="slider_container">
                <div class="slider_holder" id="slider_left">
    				<a style="text-decoration:none;border:0px;" <? if($slider_left[$i]['pretty_url'] == '0'){ echo 'target="_blank"'; }?> href="<?= $slider_left[$i]['link'];?>">
    					<div class="home_item">
    						<img class="home_item_image" style="" src="<?= site_url('items/general/uploads/slider_images/'.$slider_left[$i]['fname'])?>" />
    						<div class="home_item_button_bg"></div>
    						<div class="home_item_button">
    							<?= $slider_left[$i]['text'];?>
    						</div>
    					</div>
    				</a>
        		</div>

                <div class="slider_holder" id="slider_center">
    				<a style="text-decoration:none;border:0px;" <? if($slider_center[$i]['pretty_url'] == '0'){ echo 'target="_blank"'; }?> href="<?= $slider_center[$i]['link'];?>">
    					<div class="home_item">
    						<img class="home_item_image" style="" src="<?= site_url('items/general/uploads/slider_images/'.$slider_center[$i]['fname'])?>" />
    						<div class="home_item_button_bg"></div>
    						<div class="home_item_button">
    							<?= $slider_center[$i]['text'];?>
    						</div>
    					</div>
    				</a>
        		</div>

        		<div class="slider_holder" id="slider_right">
    				<a style="text-decoration:none;border:0px;" <? if($slider_right[$i]['pretty_url'] == '0'){ echo 'target="_blank"'; }?> href="<?= $slider_right[$i]['link'];?>">
    					<div class="home_item">
    						<img class="home_item_image" style="" src="<?= site_url('items/general/uploads/slider_images/'.$slider_right[$i]['fname'])?>" />
    						<div class="home_item_button_bg"></div>
    						<div class="home_item_button">
    							<?= $slider_right[$i]['text'];?>
    						</div>
    					</div>
    				</a>
        		</div>
    		</div>
		<?php endfor;?>
	</div>
	<div class="bottom_border"></div>
	
	
</div>

     
		