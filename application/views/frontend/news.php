

        <script>
            var ignite_backgrounds = 0;
            
            
            $(document).ready(function()
			{	
				$('.impact_content').each(function(){
					$this = $(this);
					var newText = $this.text();
					newText = nl2br(newText);
					$this.html(newText);
				});
				
			});
        </script>
		

		<link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/modules.css"); ?>">
		
		
		<div class="site_info_holder">
			<a style="color:#a9a9a9;text-decoration:none;" href="<?= site_url('')?>">HOME</a>
		</div>
		
		<div id="article_title"><?= $news->headline;?></div>
		<div id="article_sub_title"><?= $news->sub_headline;?></div>
		<div id="tag_holder">
			Tags:
			<? foreach($tags as $tag):?>
				<a style="color:#888888;" href="<?= site_url('search/'.$tag->name);?>"><span class="tag_item"><?= $tag->display_name;?></span></a>&nbsp;
			<? endforeach;?>
		</div>
		<div id="article">
        
            <div id="article_content">
                <?php foreach($modules as $module):?>
                    <?= $module?>
                <?php endforeach;?>
                
               
            </div>
            
           <div class="articles_holder_isotope" id="isotopeHolder1">
			<?  foreach($related as $article):?>
					<? $link = $article->prettyurl;
						
					 if($article->section_id != "" && $article->section_id !== NULL)
					 {
						 $link = $article->prettyurl."#".$article->section_id;
					 }
						
						
					?>
					<div class="teaser_item" >
						<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$link)?>">
							<div class="article_teaser_img_holder">
								<img class="article_teaser_img" src="<?= site_url('items/general/uploads/article_teaser/'.$article->teaser_img)?>"/>
							</div>
						</a>
						<div class="article_teaser_headline"><?= nl2br($article->headline);?></div>					
						<div class="article_teaser_text"><?= $article->teaser_text;?></div>
						<a style="text-decoration:underline;color:#494949;" href="<?= site_url('article/'.$link)?>">
							<div class="article_teaser_more">> read more <</div>
						</a>
					</div>
				
			<? endforeach;?>
		</div>
            
		</div>

<script>
	$(document).ready(function()
	{	
		var sumIsotope = 1;
		igniteIsotopeTeaserSubsite(sumIsotope);
	});
</script>		