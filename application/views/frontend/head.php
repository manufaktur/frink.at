<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
	<? if($is_mobile):?>
		<meta name="viewport" content="width=760px, minimum-scale=0.2, maximum-scale=2.5, user-scalable=no">
	<? endif;?>	
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	
    <meta charset="UTF-8">
	
	<meta property="og:title" content="Frink"/>
	<meta property="og:type" content="website"/>
	<meta name="description" content="Frink"/>
	<meta name="google" content="notranslate" />

	<title>Frink Advanced Services</title>
	<link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/fonts.css"); ?>">
	<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/lightbox.css"); ?>">
	<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/desktop.css"); ?>">
	<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/jquery.qtip.css"); ?>">
	<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/slick.css"); ?>">
	<link rel="icon" type="image/ico" href="<?=site_url("items/frontend/img/favicon.ico"); ?>"/>
	<? if($is_mobile):?>
		<link rel="stylesheet" type="text/css" href="<?=site_url("items/frontend/css/mobile.css"); ?>">
	<? endif;?>		

	<script type="text/javascript" src="<?=site_url("items/general/js/jquery-1.11.2.min.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/packery.pkgd.min.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/isotope.pkgd.min.js"); ?>"></script>
	
	<script type="text/javascript" src="<?=site_url("items/frontend/js/jquery.qtip.min.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/placeholders.min.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/slick.min.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/desktop.js"); ?>"></script>
	<!-- <script type="text/javascript" src="<?=site_url("items/frontend/js/layouts/widescreen.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/layouts/smallscreen.js"); ?>"></script>
	<script type="text/javascript" src="<?=site_url("items/frontend/js/layouts/mobiledevice.js"); ?>"></script> -->
		
	<!-- VARIABLES -->
	<script type="text/javascript">
            var rootUrl = "<?= site_url(); ?>";
           
            
	</script>	
	
	<!-- YT -->
	
	<!-- GA -->
    <script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	  ga('create', 'UA-64469260-2', 'auto');
	  ga('send', 'pageview');
	
	</script>
     
     
     
    <!-- FB -->

</head>
<body>
	
