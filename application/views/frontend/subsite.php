
        <script>
            var ignite_backgrounds = 0;
        </script>


		<link rel="stylesheet" type="text/css" href="<?=site_url("items/general/css/modules.css"); ?>">
		
		<div id="article">
        
            <div id="article_content" class="article_content_subsite">
                <?php foreach($modules as $module):?>
                    <?= $module?>
                <?php endforeach;?>
            </div>
            
            <?= $sidebar?>
            
            <div id="articles" style="float:left;margin-left:15px;">
                <?php foreach($news as $article):?>
                    
                        <div class="article">
                            <div class="article_headline"><a href="<?= site_url('news/' . $article->prettyurl)?>"><?= nl2br($article->headline) ?></a></div>
                            <a href="<?= site_url('news/' . $article->prettyurl)?>"><img class="article_teaserimg" src="<?= site_url('items/general/uploads/article_teaser/' . $article->teaser_img)?>"/></a>
                            <div class="article_teasertext"><?= $article->teaser_text?></div>
                            <div class="article_readmore"><a href="<?= site_url('news/' . $article->prettyurl)?>"><?= $this->lang->line('newsarticle_readmore')?></a></div>
                        </div>
                    </a>    
                <?php endforeach;?>
			</div>
		</div>
		
		