



<div class="bc_column <?php if(($num_row) %2 == 0): ?>erow<?php endif;?>">
	<p class="bc_column_header"><?= $display_as?>:</p>
	<?php if(isset($col_info) && $col_info != ""):?>
        <p class="bc_column_info">
            <i><?= $col_info ?></i>
        </p>
    <?php endif; ?>
	<div class="bc_column_input bc_col_date">
		<input type="text" READONLY name="col_<?= $db_name?>" format="<?= $edit_format?>" value="<?php if(isset($value) && $value != "" && $value != 1 && $value != "0000-00-00 00:00:00" && $value != null && $value != '1970-01-01 01:00:00'):?><?=date($list_format, strtotime($value));?><?php endif;?>">
		<img class="bc_col_date_calendar" src="<?= site_url('items/besc_crud/img/calendar_icon.png')?>" />
		<img class="bc_col_date_reset" src="<?= site_url('items/besc_crud/img/delete.png')?>" />
	</div>
</div>