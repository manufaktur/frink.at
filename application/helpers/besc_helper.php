<?php

if (! defined('BASEPATH')) exit('No direct script access allowed');

function rand_string($length)
{
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    $str = "";
    $str = base64_encode(openssl_random_pseudo_bytes($length, $strong));
    $str = substr($str, 0, $length);
    $str = preg_replace("/[^a-zA-Z0-9\s]/", "", $str);
    return $str;
}

function ip_address_to_number($IPaddress)
{
    if ($IPaddress == '') 
    {
        return 0;
    } 
    else 
    {
        $ips = explode('.', $IPaddress);
        return ($ips[3] + $ips[2] * 256 + $ips[1] * 65536 + $ips[0] * 16777216);
    }
}

function check_hash($hash, $stored_hash)
{
    require_once (APPPATH . 'libraries/PasswordHash.php');
    $hasher = new PasswordHash(8, false);
    return $hasher->CheckPassword($hash, $stored_hash);
}

function purify($dirty_html)
{
    require_once (APPPATH . 'libraries/htmlPurifier/HTMLPurifier.standalone.php');
    $config = HTMLPurifier_Config::createDefault();
    $config->set('Core.Encoding', 'UTF-8');
    $config->set('HTML.Doctype', 'HTML 4.01 Transitional');
    $config->set('HTML.TidyLevel', 'light');
    $config->set('HTML.Allowed', 'b,strong,a[href],i,em,ul,ol,li,p,img[alt],img[src],u,h1,h2');
    $purifier = new HTMLPurifier($config);

    $clean_html = $purifier->purify($dirty_html);
    
    return $clean_html;
}

FUNCTION inverseHex($color)
{
    $color = TRIM($color);
    $prependHash = FALSE;
    IF (STRPOS($color, '#') !== FALSE) 
    {
        $prependHash = TRUE;
        $color = STR_REPLACE('#', NULL, $color);
    }
    SWITCH ($len = STRLEN($color)) 
    {
        CASE 3:
            $color = PREG_REPLACE("/(.)(.)(.)/", "\\1\\1\\2\\2\\3\\3", $color);
        CASE 6:
            BREAK;
        DEFAULT:
            TRIGGER_ERROR("Invalid hex length ($len). Must be (3) or (6)", E_USER_ERROR);
    }
    
    IF (! PREG_MATCH('/[a-f0-9]{6}/i', $color)) 
    {
        $color = HTMLENTITIES($color);
        TRIGGER_ERROR("Invalid hex string #$color", E_USER_ERROR);
    }
    
    $r = DECHEX(255 - HEXDEC(SUBSTR($color, 0, 2)));
    $r = (STRLEN($r) > 1) ? $r : '0' . $r;
    $g = DECHEX(255 - HEXDEC(SUBSTR($color, 2, 2)));
    $g = (STRLEN($g) > 1) ? $g : '0' . $g;
    $b = DECHEX(255 - HEXDEC(SUBSTR($color, 4, 2)));
    $b = (STRLEN($b) > 1) ? $b : '0' . $b;
    
    RETURN ($prependHash ? '#' : NULL) . $r . $g . $b;
}