<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$config['st_anna_config_id'] = 1;
$config['st_anna_module_types'] = array('text' => 'module_text', 'image' => 'module_image', 'gallery' => 'module_gallery', 'download' => 'module_download', 'bulletpoint' => 'module_bulletpoint', 'link' => 'module_link', 'button' => 'module_button', 'impact' => 'module_impact', 'video' => 'module_video', 'text_image' => 'module_text_image');
$config['st_anna_unique_pages'] = array
(
    'online_donation' => array
    (
        'name' => 'Online Spenden',
        'id' => UNIQUE_DONATE_PAGE_ID
    ),
    'toystore' => array
    (
        'name' => 'Kuscheltiere bestellen',
        'id' => UNIQUE_TOYSTORE_PAGE_ID
    )
);
